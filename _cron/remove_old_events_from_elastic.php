<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

use Elasticsearch\ClientBuilder;

$client = ClientBuilder::create()->build();


$form_data = array(
	'widget_type' => 'event',
	'search_lang' => 'en',
);

$resp = vg_front_search($form_data);

$current_timestamp = current_time('timestamp');

$current_time = mktime(0,0,0, date('n', $current_timestamp), date('j', $current_timestamp), date('Y', $current_timestamp));

echo '<pre>';
print_r($resp['data']);
echo '</pre>';

foreach ($resp['data'] as $r) {

	$single_dates_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 0 AND `date` > %d AND post_id = %d ORDER BY `date` ASC LIMIT 10', $current_time, $r['post_id']);

	$date_ranges_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 1 AND end_date_range > %d AND post_id = %d ORDER BY start_date_range ASC LIMIT 10', $current_time, $r['post_id']);

	$single_dates_results = $wpdb->get_results($single_dates_sql);

	$date_ranges_results = $wpdb->get_results($date_ranges_sql);

	echo 'doing post id: ' . $r['post_id'] . '<br />';

	echo 'Upcoming Single Dates<pre>';
	print_r($single_dates_results);
	echo '</pre>';

	echo 'Upcoming Date Ranges<pre>';
	print_r($date_ranges_results);
	echo '</pre>';

	$total_upcoming_dates = 0;

	foreach ($single_dates_results as $k=>$row) {
		$total_upcoming_dates++;
	}

	foreach ($date_ranges_results as $k=>$row) {
		$total_upcoming_dates++;
	}

	if ($total_upcoming_dates === 0) {
		/* Remove this event from elastic so that it is no longer searchable */

		echo 'we should delete this event:<br />';

		
		try {

			$params = array();
		    $params['index'] = 'vg';
		    $params['type'] = 'listings';
		    $params['id'] = $r['post_id'];
		    $response = $client->delete($params);

		    $result = array('success' => true, 'action' => 'deleted', 'response' => $response, 'params' => $params);

		} catch (Exception $e) {
	    	//echo 'Caught exception: ',  $e->getMessage(), "\n";
	    	$result = array('success' => false, 'action' => 'deleted', 'error' => $e->getMessage(), 'params' => $params);
		}

		echo 'Delete Response:<pre>';
		print_r($result);
		echo '</pre>';

		echo 'delete argis!<br />';

		delete_arcgis_event($r['post_id']);

		echo 'loop done!';

	}

}



