<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

$tasks = array(
	'listings' => array(
		'post_types' => array('page', 'vg_listings'),
		'file' => 'listing_filters'
	),
	'events' => array(
		'post_types' => array('vg_events'),
		'file' => 'event_filters'
	),
);

foreach ($tasks as $taskID=>$task) {

	$posts = get_posts(array(
		'post_type' => $task['post_types'],
		'numberposts' => -1,
		'suppress_filters' => false,
	));

	$pool_list = array();

	$total_posts_processed = 0;

	foreach ($posts as $post) {

		if ($taskID == 'listings') {

			$terms = wp_get_object_terms($post->ID, 'vg_listings_cats');

		}

		if ($taskID == 'events') {

			$terms = wp_get_object_terms($post->ID, 'vg_events_cats');

		}

		$categories = array();
		foreach ($terms as $term) {
			$categories[] = $term->term_id;
		}

		$terms = wp_get_object_terms($post->ID, 'vg_locality_cats');

		$localities = array();
		foreach ($terms as $term) {
			$localities[] = $term->term_id;
		}

		sort($categories, SORT_NUMERIC);
		sort($localities, SORT_NUMERIC);

		if (empty($categories) || empty($localities)) {
			continue;
		}

		$total_posts_processed++;

		$key = 'pool_key_c-' . implode('-', $categories) . '_l-' . implode('-', $localities);

		if (!isset($pool_list[$key])) {

			$pool_list[$key] = array(
				'l' => $localities,
				'c' => $categories,
				//'count' 	 => 0,
			);

		}

		//$pool_list[$key]['count']++;

	}

	$pool_list = array_values($pool_list);

	echo 'total posts processed: ' . $total_posts_processed . '<br />';

	echo '<pre>';
	print_r($pool_list);
	echo '</pre>';

	$data = json_encode($pool_list);

	$filepath = get_template_directory() . '/cache/' . $task['file'] . '.json';

	file_put_contents($filepath, $data, LOCK_EX);

}

?>