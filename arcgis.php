<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Get started with MapView - Create a 2D map</title>
<style>
  html, body, #viewDiv {
    padding: 0;
    margin: 0;
    height: 100%;
    width: 100%;
  }
</style>
<link rel="stylesheet" href="//js.arcgis.com/4.0/esri/css/main.css">
<script src="//js.arcgis.com/4.0/"></script>
<script>
require([
        "esri/Map",
        "esri/views/MapView",
        "esri/layers/FeatureLayer",
        "dojo/domReady!"
      ],
      function(
        Map, MapView,
        FeatureLayer
      ) {

        var map = new Map({
          basemap: "streets"
        });

        var view = new MapView({
          container: "viewDiv",
          map: map,
          zoom : 13,
          center: [14.242072, 36.044052],
        });

        /********************
         * Add feature layer
         ********************/

        // Carbon storage of trees in Warren Wilson College.
        var featureLayer = new FeatureLayer({
          url: "https://54.194.53.236:6080/arcgis/rest/services/Visit_Gozo/Gozo/FeatureServer"
        });

        map.add(featureLayer);

      });
</script>
</head>
<body>
   	<div id="viewDiv"></div>
</body>
</html>