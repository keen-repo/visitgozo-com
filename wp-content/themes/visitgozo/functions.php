<?php
/**
 * vg functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package vg
 */

if ( ! function_exists( 'vg_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vg_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'page-banner', 2500, 600, true );
	add_image_size( 'subpage-featured', 700, 500, true );
	add_image_size( 'medium-uncropped', 500, 500, false );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'vg' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

}
endif; // vg_setup
add_action( 'after_setup_theme', 'vg_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vg_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vg_content_width', 640 );
}
add_action( 'after_setup_theme', 'vg_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vg_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'vg' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'vg_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vg_scripts() {

	wp_enqueue_style( 'vg-style', get_stylesheet_uri() );
	wp_enqueue_style( 'vg-leaflet', 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css' );
	wp_enqueue_style( 'vg-slick', get_template_directory_uri() . '/css/slick.css' );
	wp_enqueue_style( 'vg-slick-theme', get_template_directory_uri() . '/css/slick-theme.css' );
	wp_enqueue_style( 'vg-remodal', get_template_directory_uri() . '/css/remodal.css' );
	wp_enqueue_style( 'vg-remodal-theme', get_template_directory_uri() . '/css/remodal-default-theme.css' );
	wp_enqueue_style( 'vg-select2', get_template_directory_uri() . '/css/select2.min.css' );
	wp_enqueue_style( 'vg-smartbanner', get_template_directory_uri() . '/css/jquery.smartbanner.css' );
	wp_enqueue_style( 'vg-css', get_template_directory_uri() . '/css/vg.css', array(), false, 'screen' );
	wp_enqueue_style( 'vg-print-css', get_template_directory_uri() . '/css/vg-print.css', array(), false, 'print' );

	wp_enqueue_style( 'vg-local-artists-css', get_template_directory_uri() . '/css/local-artists.css', array(), false, 'screen' );

	wp_enqueue_script( 'vg-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'vg-simple-weather', get_template_directory_uri() . '/js/jquery.simpleWeather.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-slick', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-equalheight', get_template_directory_uri() . '/js/jquery.matchHeight.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-remodal', get_template_directory_uri() . '/js/remodal.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-select2', get_template_directory_uri() . '/js/select2.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-geolocator', get_template_directory_uri() . '/js/geolocator.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-defiant', get_template_directory_uri() . '/js/defiant.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-pagination', get_template_directory_uri() . '/js/pagination.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-history', get_template_directory_uri() . '/js/jquery.history.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-smartbanner', get_template_directory_uri() . '/js/jquery.smartbanner.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'vg-vide', get_template_directory_uri() . '/js/jquery.vide.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'underscore');
	wp_enqueue_script('jquery-ui-autocomplete');
	wp_enqueue_script( 'vg-js', get_template_directory_uri() . '/js/vg.js', array('jquery'), '20130115', true );

	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'vg_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Theme Core Functionality.
 */
require get_template_directory() . '/inc/vg.php';
