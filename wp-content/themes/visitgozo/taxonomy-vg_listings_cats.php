<?php
/* Since we are not using the taxonomy post type page we can redirect to the matching category */
$term_id = get_queried_object()->term_id;
/* get the english version of the term id */
$term_id = icl_object_id($term_id, 'vg_listings_cats', true, 'en');

$connected_page = get_term_meta($term_id, $prefix . 'connected-page', true );

if ($connected_page) {
	/* get the current language page */
	$connected_page = icl_object_id($connected_page, 'page', true);

	$permalink = get_permalink($connected_page);
} else {
	$permalink = home_url();
}

wp_redirect($permalink, 301);

?>