<?php
/**
 * Template Name: My Gozo
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

				<div id="my-gozo-container"></div>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

		<script type="text/javascript">

		jQuery(document).ready(function() {

			jQuery.post( vgAjaxURL, { action: 'vg_get_my_gozo_entries_render' }, function( response ) {
				jQuery('#my-gozo-container').html(response);
			});

		});

		</script>

<?php get_footer(); ?>
