<?php
/**
Template Name: My Account
*/

global $wpdb;

get_header(); ?>

	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php if(is_user_logged_in()){ ?>

					<div class="txt-alignright">
						<?php echo do_shortcode( '[frm-login layout=h]' ); ?>
					</div>
					<div class="section section-my-account section-maps section-padding-bottom">
						<h3 class="ma-title"><?php  _e( 'Saved Maps', 'vg-front' ); ?></h3>
						<div class="message no-items">
							<?php _e( 'You have no saved maps. Explore the Interactive Map', 'vg-front' ); ?>
							<a href="<?php echo get_permalink( icl_object_id( 8351, 'page', false ) ); ?>" class="clickable"><?php _e( 'here', 'vg-front' ); ?></a>.
						</div>
					</div>

					<div class="section section-my-account section-events section-padding-bottom">
						<h3 class="ma-title relative">
							<?php  _e( 'Events', 'vg-front' ); ?>
							<a href="<?php echo get_permalink( icl_object_id( 9599, 'page', false ) ); ?>manage-events" class="button clickable"><?php _e( 'Add Event', 'vg-front' ); ?></a>
						</h3>
						<?php 

							$events = $wpdb->get_results($wpdb->prepare('SELECT * FROM vg_listing_relationships WHERE user_id = %d and type = %s and status != 2', get_current_user_id(), 'event'));

							if(count($events) == 0){ ?>
								<div class="message no-items">
									<?php _e( 'You haven\'t created any events yet. Click the button above to create or click', 'vg-front' ); ?>
									<a href="<?php echo get_permalink( icl_object_id( 9599, 'page', false ) ); ?>manage-events" class="clickable"><?php _e( 'here', 'vg-front' ); ?></a>.
								</div>
							<?php }

							foreach ($events as $event) {

								$entry = FrmEntry::getOne($event->form_entry_id, true);

								$aid = $entry->metas[114];

								$image = wp_get_attachment_image_src($aid, 'medium-uncropped');

								?>
								<a class="item-box" href="<?php echo get_permalink() . 'manage-events/?frm_action=edit&entry=' . $event->form_entry_id; ?>">
									<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<?php echo $image[0]; ?>');"></div>
									<div class="item-title row">
										<span class="col col-80"><?php echo $entry->metas[98]; ?></span>
									</div>
								</a>
								<?php

							}

						?>
					</div>
					<div class="section section-my-account section-listings section-padding-bottom">
						<h3 class="ma-title relative">
							<?php  _e( 'Listings', 'vg-front' ); ?>
							<a href="<?php echo get_permalink( icl_object_id( 9599, 'page', false ) ); ?>manage-listings" class="button clickable"><?php _e( 'Add Listing', 'vg-front' ); ?></a>
						</h3>
						<?php
							$listings = $wpdb->get_results($wpdb->prepare('SELECT * FROM vg_listing_relationships WHERE user_id = %d and type = %s and status != 2', get_current_user_id(), 'listing'));

							if(count($listings) == 0){ ?>
								<div class="message no-items">
									<?php _e( 'You haven\'t created any listings yet. Click the button above to create or click', 'vg-front' ); ?>
									<a href="<?php echo get_permalink( icl_object_id( 9599, 'page', false ) ); ?>manage-listings" class="clickable"><?php _e( 'here', 'vg-front' ); ?></a>.
								</div>
							<?php }

							?>
							<div class="item-boxes listings oflow">
							<?php

							foreach ($listings as $listing) {

								$entry = FrmEntry::getOne($listing->form_entry_id, true);

								$aid = $entry->metas[91];

								$image = wp_get_attachment_image_src($aid, 'medium-uncropped');

								?>
								<a class="item-box" href="<?php echo get_permalink() . 'manage-listings/?frm_action=edit&entry=' . $listing->form_entry_id; ?>">
									<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<?php echo $image[0]; ?>');"></div>
									<div class="item-title row">
										<span class="col col-80"><?php echo $entry->metas[75]; ?></span>
									</div>
								</a>
								<?php

							}

						?>
							</div>
					</div>

				<?php } else { ?>

				<div class="section section-login-register row section-padding-bottom">
					<div class="fl col col-50 login">
						<h3 class="section-title txt-alignleft"><?php _e( 'Sign In', 'vg-front' ); ?></h3>
						<?php echo do_shortcode( '[frm-login layout=h]' ); ?>

						<br class="clear" />

						<a href="<?php echo wp_lostpassword_url( get_permalink() ); ?>"><?php _e('Forgot Password?', 'vg-front'); ?></a>

					</div>
					<div class="fr col col-50 register">
						<h3 class="section-title txt-alignleft"><?php _e( 'Register', 'vg-front' ); ?></h3>
						<?php echo do_shortcode( '[formidable id=11]' ); ?>
					</div>
				</div>


				<?php } ?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
