<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vg
 */

global $prefix, $sitepress;

$pageBanners = rwmb_meta( $prefix.'page-banners', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );
if( count($pageBanners) > 0 && $post->post_type == 'page' ) $hasBanners = 'has-banners'; else $hasBanners = 'no-banners';

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="p:domain_verify" content="745957e224f30fa47d4c7204f87a09da"/>
<meta name="apple-itunes-app" content="app-id=1018169114">
<meta name="google-play-app" content="app-id=com.esri.android.tutorials.visitgozo">
<link rel="icon" type="image/x-icon" href="/favicon.ico" />
<link rel="apple-touch-icon" href="/apple-icon.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<script type="text/javascript">
	var vgCurrentLang = '<?php echo ICL_LANGUAGE_CODE; ?>';
	var vgAjaxURL = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23304770-21', 'auto');
  ga('send', 'pageview');

</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N727XZV');</script>
<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1203288913089766');
fbq('track', 'PageView', {lang : '<?php echo ICL_LANGUAGE_CODE; ?>', url : '<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>'});
</script>
<noscript>
 <img height="1" width="1"
src="https://www.facebook.com/tr?id=1203288913089766&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>

<body <?php body_class($hasBanners); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N727XZV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'vg-front' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="wrap">
			<div class="site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo">
					<img src="<?php bloginfo( 'template_url' ); ?>/images/logo-visitgozo.png" alt="<?php bloginfo( 'name' ); ?>" />
				</a>
			</div><!-- .site-branding -->

			<div class="site-navigation txt-alignright">
				<div class="nav-item clickable item-lang icon-globe relative">

				<?php



				if (is_singular(array('vg_listings', 'vg_events'))) {

					$active_languages = $sitepress->get_active_languages();

					$languages = array();

					$en_post_id = icl_object_id(get_the_ID(), get_post_type(get_the_ID()), true, 'en');

					$en_permalink = str_replace('/' . ICL_LANGUAGE_CODE . '/', '/', get_permalink($en_post_id));

					foreach ($active_languages as $lang) {

						$languages[] = array(
							'active' => ($lang['code'] == ICL_LANGUAGE_CODE) ? true : false,
							'url' => apply_filters( 'wpml_permalink', $en_permalink, $lang['code'] ),
							'code' => $lang['code'],
						);

					}


				} else {

					$languages = icl_get_languages('skip_missing=0');

				}

				?>
					<?php  ?>
					<select class="lang-select" id="lang-select-dropdown" style="width:80px;">
					  <?php if(count($languages) > 0){
					    foreach($languages as $l){
					    	if($l['active'])
					    		$selected = 'selected';
					    		else $selected = ''; ?>
					    	<option value="<?php echo $l['url']; ?>" <?php echo $selected; ?>><?php echo str_replace('-hans', '' ,$l['code']); ?></option>
					    <?php }
					  } ?>
					</select>
				</div>
				<div class="nav-item clickable item-search icon-search">
					<?php _e( 'Search', 'vg-front' ); ?>
				</div>
				<div class="nav-item clickable item-menu icon-menu highlight">
					<?php _e( 'Menu', 'vg-front' ); ?>
				</div>
			</div>
		</div>

		<?php /*<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'vg' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation --> */ ?>

	</header><!-- #masthead -->

	<div class="site-overlay menu-overlay">
		<div class="wrap overlay-content">
			<span class="btn-close icon-cancel-circled clickable"></span>
			<div class="row">
				<div class="col col-20 heading">
					<?php _e( 'Menu', 'vg-front' ); ?>
				</div>
				<div class="col col-50 submenu-items">
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
						<div id="submenu-wrap" class="fl oflow"></div>
						<br class="clear" />
					<div class="login-wrap">
						<ul class="login-links">
							<?php if (is_user_logged_in()) { ?>
								<li><a href="<?php bloginfo('url'); ?>/my-account"><?php _e( 'My Account', 'vg-front' ); ?></a></li>
								<li><?php echo do_shortcode( '[frm-login layout=h]' ); ?></li>
							<?php } else { ?>
								<li><a href="<?php bloginfo('url'); ?>/my-account"><?php _e( 'Sign In', 'vg-front' ); ?></a></li>
								<li><a href="<?php bloginfo('url'); ?>/my-account"><?php _e( 'Register', 'vg-front' ); ?></a></li>
							<?php } ?>
						</ul>
						<p class="note">
							<?php _e( 'Sign in / Register to save Your Trip or add content on visitgozo.com', 'vg-front' ); ?>
						</p>
					</div>
				</div>
				<div class="col col-30 quick-links">
					<?php wp_nav_menu( array( 'menu' => 'Quick Links' ) ); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="site-overlay search-overlay">
		<div class="wrap overlay-content">
			<span class="btn-close icon-cancel-circled clickable"></span>
			<div class="row">
				<div class="col col-20 heading">
					<?php _e( 'Search', 'vg-front' ); ?>
				</div>
			</div>
			<div class="overlay-search-input relative">
				<div class="ui-front">
					<input type="text" name="search-input" value="<?php echo (isset($_GET['search_term'])) ? esc_attr($_GET['search_term']) : ''; ?>" placeholder="<?php _e( 'Search', 'vg-front' ); ?>" />
					<span class="icon icon-search"></span>
				</div>
				<div class="message section-padding-top none no-search-results"><?php _e('Sorry no results were found.', 'vg-front'); ?></div>
			</div>
		</div>
	</div>

	<?php if( is_front_page() ){ ?>
		<div class="home-intro oflow" style="background-image: url('https://embed-ssl.wistia.com/deliveries/2e400dca3e9af722e16d1509f10a0171b970c2e1.jpg?image_crop_resized=1920x1012');">
			<style>
				#vg-full-video {
					display: none;
				}
				.remodal .wistia_responsive_padding {
					display: none;
				}
			</style>
			<div class="yt-embed fullwidth-fullheight">
				<!-- <script src="//fast.wistia.com/embed/medias/pq5rctbhzq.jsonp" async></script>
				<script src="//fast.wistia.com/assets/external/E-v1.js" async></script>
				<div id="vg-home-video" class="wistia_responsive_padding" style="padding:52.71% 0 0 0;position:relative;">
					<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
						<div class="wistia_embed wistia_async_pq5rctbhzq videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
					</div>
				</div> -->
			    <iframe id="vg-full-video" width="1901" height="1012" src="https://www.youtube.com/embed/XRgOvegVrOw?controls=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<!--div id="yt-video" class="fullwidth-fullheight"></div-->
			</div>
			<div class="home-video fullwidth-fullheight">
				<script src="https://fast.wistia.com/embed/medias/ej2b9xcjbj.jsonp" async></script>
				<div class="wistia_responsive_padding" style="padding:52.71% 0 0 0;position:relative;">
					<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
						<div class="wistia_embed wistia_async_ej2b9xcjbj seo=false videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
					</div>
				</div>
				<div class="video-overlay"></div>
			</div>
			<div class="home-video-still fullwidth-fullheight bg-cover none" style="background-image: url('https://embed-ssl.wistia.com/deliveries/2e400dca3e9af722e16d1509f10a0171b970c2e1.jpg?image_crop_resized=1920x1012');"></div>
			<div class="intro-search-wrap valign-middle">
				<div class="intro-search-container">
					<?php /*
					<div class="competition">
						<div class="competition-logo">
							<img src="<?php bloginfo('template_url'); ?>/images/logo-givemegozo.png" alt="Give Me Gozo" />
						</div>
						<div class="competition-text">
							<div class="heading">Win a GO PRO HERO 5!</div>
							<div class="subheading"><a href="https://givemegozo.hscampaigns.com/?utm_source=VisitGozo%20Website&utm_medium=Banner&utm_campaign=%23GiveMeGozo" target="_blank">Click here</a> to enter the #GiveMeGozo Video Competition.</div>
						</div>
					</div> */ ?>
					<div class="intro-search-content txt-aligncenter">
						<div class="btn-close icon-cancel-circled clickable"></div>
						<h2><?php _e( 'Visiting Gozo?', 'vg-front' ); ?></h2>
						<div class="tagline"><?php _e( ' All the info you need, just a click away...', 'vg-front' ); ?></div>
						<div class="search-input-wrap relative">
							<input type="text" class="search-input" name="psearchinput" placeholder="<?php _e( 'Example: farmhouses, beaches, walks, events, etc...', 'vg-front' ); ?>" />
							<span class="clickable btn-search icon-search"></span>
						</div>
						<div class="hot-searches row">
							<span class="clickable col col-25"><?php _e( 'Beaches', 'vg-front' ); ?></span>
							<span class="clickable col col-25"><?php _e( 'Farmhouses', 'vg-front' ); ?></span>
							<span class="clickable col col-25"><?php _e( 'Marsalforn', 'vg-front' ); ?></span>
							<span class="clickable col col-25"><?php _e( 'Archaeological Sites', 'vg-front' ); ?></span>
						</div>
					</div>
					<!-- <span id="vg-play-video" class="clickable btn-play-video">Play Video<span class="icon-play-circled"></span></span> -->
					<span class="clickable btn-play-video video-inline"><?php _e( 'Play Video', 'vg-front' ); ?><span class="icon-play-circled"></span></span>
					<span class="clickable btn-play-video video-modal" data-remodal-target="home-video"><?php _e( 'Play Video', 'vg-front' ); ?><span class="icon-play-circled"></span></span>
				</div>
			</div>
			<div class="big-play-btn none fullwidth-fullheight">
				<span class="clickable btn-play-video video-modal valign-middle" data-remodal-target="home-video"><span class="icon-play-circled"></span><br/><?php _e( 'Play Video', 'vg-front' ); ?></span>
			</div>
			<div class="remodal" data-remodal-id="home-video">
				<iframe id="vg-full-video-modal" width="600" height="480" src="https://www.youtube.com/embed/XRgOvegVrOw?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				<script src="https://fast.wistia.com/embed/medias/ej2b9xcjbj.jsonp" async></script>
				<!-- <script src="//fast.wistia.com/embed/medias/ej2b9xcjbj.jsonp" async></script> -->
				<script src="//fast.wistia.com/assets/external/E-v1.js" async></script>
				<div class="wistia_responsive_padding" style="padding:52.71% 0 0 0;position:relative;">
					<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
						<div class="wistia_embed wistia_async_ej2b9xcjbj videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
					</div>
				</div>

			</div>
			<script>
				window._wq = window._wq || [];
				_wq.push({ id: "ej2b9xcjbj", onReady: function(video) {
				  	var playButton = jQuery('.btn-play-video.video-inline');
				  	var closeButton = jQuery('.intro-search-content .btn-close');
					var videoThumb = jQuery('.home-video');

					playButton.click(function(){
						videoThumb.fadeOut('fast');
						jQuery('.home-intro').addClass('yt-active');

						video.play();
					});

					closeButton.click(function(){
						videoThumb.fadeOut('fast');
						jQuery('.home-intro').addClass('yt-active');

						video.play();
					});

					video.bind("end", function(t) {
						videoThumb.fadeIn('fast');
						jQuery('.home-intro').removeClass('yt-active');
					});

				}});
			</script>
		</div>
	<?php } else {
		if ( $hasBanners != 'no-banners' ){ ?>
			<div class="page-banners">
				<?php foreach( $pageBanners as $banner ){
					$bgImage = wp_get_attachment_image_src( $banner['ID'], 'page-banner' ); ?>
					<div class="slide bg-cover" style="background-image: url('<?php echo $bgImage[0]; ?>');"></div>
				<?php } ?>
			</div>
		<?php }
	?>
	<?php } ?>

	<?php if( is_front_page() || $hasBanners != 'no-banners' ){ ?>
		<div class="social-links-bar">
			<div class="wrap row">
				<div class="social col col-40">
					<a href="https://www.facebook.com/VisitGozo" target="_blank" rel="nofollow" class="fl icon-facebook"></a>
					<a href="https://twitter.com/VisitGozo" target="_blank" rel="nofollow" class="fl icon-twitter"></a>
					<a href="https://www.instagram.com/insta_visitgozo/" target="_blank" rel="nofollow" class="fl icon-instagram"></a>
					<!--<a href="https://plus.google.com/+VisitGozoOnline" target="_blank" rel="nofollow" class="fl icon-gplus"></a>-->
					<a href="https://www.youtube.com/user/VisitGozoOnline" target="_blank" rel="nofollow" class="fl icon-youtube-play"></a>
					<a href="https://www.pinterest.com/visitgozo0636/" target="_blank" rel="nofollow" class="fl icon-pinterest"></a>
				</div>
				<div class="links-bar col col-60 txt-alignright">
					<a class="fr item ferry-queue" href="<?php echo get_permalink(icl_object_id( 8347, 'page', false )); ?>"><p class="icon-videocam" ><?php echo get_the_title( icl_object_id( 8347, 'page', false ) ); ?></p></a>
					<a class="fr item panoramic-views" href="<?php echo get_permalink(icl_object_id( 26797, 'page', false )); ?>"><p class="icon-panoramic" ><?php echo get_the_title( icl_object_id( 26797, 'page', false ) ); ?></p></a>
					<a class="fr item pool-trips" href="<?php echo get_permalink(icl_object_id( 8349, 'page', false )); ?>"><p class="icon-chat" ><?php echo get_the_title( icl_object_id( 8349, 'page', false ) ); ?></p></a>
					<!--a class="fr item interactive-map" href="<?php echo get_permalink(icl_object_id( 8351, 'page', false )); ?>"><p class="icon-map" ><?php echo get_the_title( icl_object_id( 8351, 'page', false ) ); ?></p></a-->
					<div class="fr item weather">
						<?php
							setlocale(LC_TIME, vg_wpml_get_code( ICL_LANGUAGE_CODE ));
							$day = strftime("%a");
						?>
						<p><?php echo $day; ?> <span id="weather-temp"></span></p>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if(is_front_page() && $_REQUEST['justadummyval'] == 'ionlywannahidethis'){ ?>
		<div class="competition competition-small">
			<div class="competition-logo">
				<img src="<?php bloginfo('template_url'); ?>/images/logo-givemegozo.png" alt="Give Me Gozo" />
			</div>
			<div class="competition-text">
				<div class="heading">Win a GO PRO HERO 5!</div>
				<div class="subheading"><a href="https://givemegozo.hscampaigns.com/?utm_source=VisitGozo%20Website&utm_medium=Banner&utm_campaign=%23GiveMeGozo" target="_blank">Click here</a> to enter the #GiveMeGozo Video Competition.</div>
			</div>
		</div>
	<?php } ?>

	<?php
		if ( function_exists('yoast_breadcrumb') && !is_front_page() ) { ?>
			<div class="wrap">
				<?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>
			</div>
		<?php }
	?>
	<script>
		jQuery(document).ready(function($) {
			//$('#vg-play-video').on('click', function(ev) {
			$('.btn-play-video.video-inline').on('click', function(ev) {
				$("#vg-full-video")[0].src += "&autoplay=1";
				$("#vg-full-video").fadeIn();
				$("#vg-full-video-modal").fadeIn();
				//$("#wistia_preview_video").remove();
				$(".home-video").remove();
				$(".remodal .wistia_responsive_padding").remove();
				$('.intro-search-wrap').fadeOut();
				ev.preventDefault();
			});
		});
	</script>
	<div id="content" class="site-content">
