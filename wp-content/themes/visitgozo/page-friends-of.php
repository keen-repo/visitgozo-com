<?php
/**
 * Template Name: Friends Of
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

				<div class="friends-of-links">
					<?php
						$links = rwmb_meta( $prefix.'friends-links' );

						foreach( $links as $link ){ ?>
							<div class="link row">
								<div class="col col-30">
									<?php
										$image = $link['logo'][0];
										if(isset($image)) {
											echo wp_get_attachment_image( $image, 'medium' );
										}
									?>
								</div>
								<div class="col col-70">
									<?php $item = $link['name']; if(!empty($item)){ ?>
										<h2><?php echo $link['name']; ?></h2>
									<?php } ?>
									<?php $item = $link['notes']; if(!empty($item)){ ?>
										<br/><?php echo $link['notes']; ?>
									<?php } ?><?php $item = $link['url']; if(!empty($item)){ ?>
										<br/><a href="<?php echo $link['url']; ?>" target="_blank"><?php echo $link['url']; ?></a>
									<?php } ?>
								</div>
							</div>
						<?php }
					?>
				</div>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
