<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vg
 */

?>

	<?php

		/* ALSO CHECK OUT SECTION */
		vg_render_also_check_out( get_the_ID() );

	?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row wrap">
			<div class="col col-50 ta-in-footer">
				<div class="ta-poweredby oflow fl">
					<div class="fl ta-poweredby-txt">
						<?php _e( 'Rating & Reviews', 'vg-front' ); ?><br/>
						<?php _e( 'Powered By', 'vg-front' ); ?>
					</div>
					<div class="fl">
						<a href="http://www.tripadvisor.com/" target="_blank" rel="nofollow">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/logo-ta.png" alt="<?php _e( 'Rating & Reviews', 'vg' ); ?> <?php _e( 'Powered By', 'vg-front' ); ?> TripAdvisor" />
						</a>
					</div>
				</div>
				<div class="clear">
					<a href="https://www.tripadvisor.co.uk/Tourism-g190313-Island_of_Gozo-Vacations.html" target="_blank" rel="nofollow"><?php _e( 'what other travelers say about Gozo on TripAdvisor', 'vg-front' ); ?></a>.
				</div>
			</div>
			<div class="col col-50 txt-logo txt-alignright">
				<a href="http://www.gozo.gov.mt/" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_url'); ?>/images/logo-ministry.png" alt="Ministry for Gozo" /></a><br/>
				<?php _e( 'visitgozo.com is the official site of the Ministry for Gozo' ); ?>
			</div>
		</div>
		<div class="footer-links">
			<div class="wrap">
				<div class="footer-menu oflow">
					<div class="social-menu oflow">
						<div class="title"><?php _e( 'Connect with us', 'vg-front' ); ?></div>
						<a href="https://www.facebook.com/VisitGozo" target="_blank" rel="nofollow" class="fl icon-facebook">Facebook</a>
						<a href="https://twitter.com/VisitGozo" target="_blank" rel="nofollow" class="fl icon-twitter">Twitter</a>
						<a href="https://www.instagram.com/insta_visitgozo/" target="_blank" rel="nofollow" class="fl icon-instagram">Instagram</a>
						<!--<a href="https://plus.google.com/+VisitGozoOnline" target="_blank" rel="nofollow" class="fl icon-gplus"></a>-->
						<a href="https://www.youtube.com/user/VisitGozoOnline" target="_blank" rel="nofollow" class="fl icon-youtube-play">YouTube</a>
						<a href="https://www.pinterest.com/visitgozo0636/" target="_blank" rel="nofollow" class="fl icon-pinterest">Pinterest</a>

						<div class="app-links">
							<a href='https://play.google.com/store/apps/details?id=com.esri.android.tutorials.visitgozo&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' class="fl" target="_blank"><img alt='Get it on Google Play' src='<?php echo get_template_directory_uri(); ?>/images/android-play-store.png' /></a>
							<a href='https://itunes.apple.com/mt/app/visit-gozo/id1018169114?mt=8' class="fl" target="_blank"><img alt='Get it on iOS App Store' src='<?php echo get_template_directory_uri(); ?>/images/ios-app-store.png' /></a>
						</div>

					</div>

					<?php echo wp_nav_menu( array( 'menu' => 'Main Menu', 'depth' => 2, 'container_class' => 'foot-main-menu' ) ); ?>
				</div>
				<div class="footer-partners oflow">
					<span class="title"><?php _e( 'Our Partners', 'vg-front' ); ?></span>
					<a href="http://www.eco-gozo.com/" target="_blank" rel="nofollow"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-ecogozo.png" alt="Eco Gozo" /></a>
					<a href="http://www.islandofgozo.org/" target="_blank" rel="nofollow"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-islandofgozo.png" alt="Island of Gozo" /></a>
					<a href="http://www.visitmalta.com/" target="_blank" rel="nofollow"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-visitmalta.png" alt="Visit Malta" /></a>
					<a href="http://www.mta.com.mt/" target="_blank" rel="nofollow"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-mta.png" alt="MTA" /></a>
					<a href="http://www.gov.mt/" target="_blank" rel="nofollow"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-govmt.png" alt="<?php _e( 'Government of Malta', 'vg-front' ); ?>" /></a>
					<a href="http://www.responsibletravel.com/Gozo-Travel-Guide/" target="_blank" rel="nofollow"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-responsibletravel.png" alt="responsibletravel.com" /></a>
					<a href="http://www.tourism.gov.mt/" target="_blank" rel="nofollow"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo-ministrytourism.png" alt="<?php _e( 'Ministry for Tourism', 'vg-front' ); ?>" /></a>
				</div>
				<div class="footer-copyright row">
					<div class="col col-50">
						Copyright &copy; <?php echo date( 'Y' ); ?> VisitGozo.com. |
						<a href="<?php echo get_permalink(icl_object_id( 7215, 'page', false )); ?>"><?php echo get_the_title( icl_object_id( 7215, 'page', false ) ); ?></a> |
						<a href="<?php echo get_permalink(icl_object_id( 7217, 'page', false )); ?>"><?php echo get_the_title( icl_object_id( 7217, 'page', false ) ); ?></a>
					</div>
					<div class="col col-50 txt-alignright">
						<a href="<?php echo get_permalink(icl_object_id( 7261, 'page', false )); ?>"><?php echo get_the_title( icl_object_id( 7261, 'page', false ) ); ?></a> |
						<a href="<?php echo get_permalink(icl_object_id( 7263, 'page', false )); ?>"><?php echo get_the_title( icl_object_id( 7263, 'page', false ) ); ?></a> |
						<a href="<?php echo get_permalink(icl_object_id( 7265, 'page', false )); ?>"><?php echo get_the_title( icl_object_id( 7265, 'page', false ) ); ?></a>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->


<script type="text/html" id="tmpl_auto_complete_search">
	<a class="" href="<%= permalink %>">
		<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<%= thumbnail[0] %>');"></div>
		<div class="item-title row">
			<span class="col col-80"><%= title %></span>
			<span class="col col-20 actions"><span data-listing-id="<%= post_id %>" class="icon-heart my-gozo-trigger <% if (in_my_gozo) { %>added<% } %>"></span></span>
		</div>
	</a>
</script>

<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:301423,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery.smartbanner({
			title : 'Visit Gozo',
			layer : true,
		});
	});
</script>

<?php wp_footer(); ?>

</body>
</html>
