<?php
/**
 * The template for displaying pool trip page.
 * Template Name: Pool Trips
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>

			<div class="section section-pool-trips section-padding-bottom row">
				<?php $trips = get_posts(array(
					'post_type' => 'vg_pool_trips',
					'numberposts' => -1,
					'meta_key' => $prefix.'date',
					'orderby' => 'meta_value',
					'order' => 'ASC',
					'meta_value' => time(),
					'meta_compare' => '>'
				));

				if( count($trips) > 0 ){

					foreach( $trips as $trip){ ?>
						<div class="col col-50 oflow trip relative">
							<div class="fl col-40 txt-aligncenter">
								<span class="icon"></span>
								<span class="clickable btn-contact" data-remodal-target="pool-trip-request" data-e="<?php echo base64_encode(rwmb_meta( $prefix.'email', array(), $trip->ID )); ?>"><?php _e( 'Contact', 'vg-front' ); ?></span>
							</div>
							<div class="fr col-50">
								<div class="row">
									<div class="col col-50 label"><?php _e( 'Travel from', 'vg-front' ); ?>:</div>
									<div class="col col-50"><?php echo rwmb_meta( $prefix.'travel_from', array(), $trip->ID ) ?></div>
								</div>
								<div class="row">
									<div class="col col-50 label"><?php _e( 'Travel to', 'vg-front' ); ?>:</div>
									<div class="col col-50"><?php echo rwmb_meta( $prefix.'travel_to', array(), $trip->ID ) ?></div>
								</div>
								<div class="row">
									<div class="col col-50 label"><?php _e( 'Date', 'vg-front' ); ?>:</div>
									<div class="col col-50"><?php echo date( 'd-m-Y', rwmb_meta( $prefix.'date', array(), $trip->ID ) ) ?></div>
								</div>
								<div class="row">
									<div class="col col-50 label"><?php _e( 'Time', 'vg-front' ); ?>:</div>
									<div class="col col-50"><?php echo rwmb_meta( $prefix.'time', array(), $trip->ID ) ?></div>
								</div>
								<div class="row">
									<div class="col col-50 label"><?php _e( 'Persons', 'vg-front' ); ?>:</div>
									<div class="col col-50"><?php echo rwmb_meta( $prefix.'persons', array(), $trip->ID ) ?></div>
								</div>
								<div class="row">
									<div class="col col-50 label"><?php _e( 'Transport', 'vg-front' ); ?>:</div>
									<div class="col col-50"><?php echo rwmb_meta( $prefix.'transport', array(), $trip->ID ) ?></div>
								</div>
							</div>
						</div>
					<?php } 

				} else { ?>

				<div class="message"><?php _e( 'Sorry, no Pool Trip requests have been found. You can add a request from the form below.' ); ?></div>

				<?php } ?>
			</div>

			<div class="section section-disclaimer section-padding-bottom">
				<h3><?php _e( 'Disclaimer', 'vg-front' ); ?></h3>
				<small><?php echo rwmb_meta( $prefix.'page-intro-text' ); ?></small>
			</div>

			<div class="section section-submit-trip section-padding-bottom">
				<?php echo do_shortcode( '[formidable id=13]' ); ?>
			</div>
			<div class="remodal pool-trip-request" data-remodal-id="pool-trip-request">
				<button data-remodal-action="close" class="remodal-close"></button>
				<h2><?php _e( 'Pool Trip Request', 'vg-front' ); ?></h2>
				<?php echo do_shortcode( '[formidable id=5]' ); ?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
