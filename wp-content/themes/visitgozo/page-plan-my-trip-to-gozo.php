<?php
/**
 * Template Name: Plan My Trip
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">
			<?php

			$filters = array(
				'seasons' => array(
					'label' 	=> __('Seasons', 'vg'),
					'type' 		=> 'radio',
					'options' 	=> vg_get_seasons()
				),
				'accompanied' => array(
					'label' 	=> __('Accompanied', 'vg'),
					'type' 		=> 'radio',
					'options' 	=> vg_get_accompanied()
				),
				'interests' => array(
					'label' 	=> __('Interests', 'vg'),
					'type' 		=> 'checkbox',
					'options' 	=> vg_get_interests()
				),
			);

			?>
			<div class="section section-listings-filter">
				<div class="listings-filters row">
					<?php

					foreach ($filters as $filter_key=>$filter) {

					?>
						<div class="col col-33">
							<h3 class="section-title"><?php echo $filter['label']; ?></h3>
							<ul class="filter">
								<?php
								foreach ($filter['options'] as $k=>$v) {

									if ($filter['type'] == 'radio') {
										$field_name = $filter_key;
									}

									if ($filter['type'] == 'checkbox') {
										$field_name = $filter_key . '[]';
									}

									?>

									<li>
										<label>
											<input type="<?php echo $filter['type']; ?>" data-tag="<?php echo $filter_key; ?>" name="<?php echo $field_name; ?>" value="<?php echo $k; ?>" /><?php echo $v; ?>
										</label>
									</li>
									<?php
								} 
								?>
							</ul>
						</div>
					<?php
					}
					?>
				</div>
			</div>
			<?php


			$posts = get_posts(array(
				'post_type' => 'page', 
				'numberposts' => -1,
				//'suppress_filters' => false,
				'meta_query' => array(
					array(
						'key' => '_vg_plan-my-trip',
						'value' => 'yes',
					)
				)
			));

			$parent_page_ids = array(
				68 => __('What to do', 'vg-front'),
				212 => __('Where to go', 'vg-front'), 
				190 => __('Where to stay', 'vg-front'), 
			);

			$trip_planner_data = array();

			foreach ($posts as $post) {

				$ancestors = get_ancestors( get_the_ID(), 'page' );

				foreach ($ancestors as $pageID) {

					if (isset($parent_page_ids[$pageID])) {

						$postID = icl_object_id($post->ID, 'page', false);

						$aid = get_post_thumbnail_id($post->ID);

						$image = wp_get_attachment_image_src($aid, 'medium-uncropped');

						$trip_planner_data[] = array(
							'category' 		=> $pageID,
							'post_id' 		=> $postID,
							'title' 		=> get_the_title($postID),
							'url' 			=> get_permalink($postID),
							'image' 		=> $image[0],
							'seasons' 		=> get_post_meta($post->ID, '_vg_plan-my-trip-season'),
							'accompanied' 	=> get_post_meta($post->ID, '_vg_plan-my-trip-accompanied'),
							'interests' 	=> get_post_meta($post->ID, '_vg_plan-my-trip-interests'),
						);


					}


				}


			}

			foreach ($parent_page_ids as $pageID=>$title) {
				?>
				<div class="trip-planner-wrap" id="trip-planner-<?php echo $pageID; ?>">
					<h2 class="section-title"><?php echo $title; ?></h2>
					<div class="message section-padding-top-bottom none"><?php _e('Sorry no results found.', 'vg'); ?></div>
					<ul class="item-boxes listings oflow"></ul>
				</div>
				<?php

			}

			$trip_planner_data 	= json_encode($trip_planner_data);

			?>
			<script type="text/template" id="vg_trip_planner_item_template">
		    	<a class="item-box" href="<%= item.url %>">
					<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<%= item.image %>');"></div>
					<div class="item-title row">
						<span class="col col-80"><%= item.title %></span>
					</div>
				</a>
			</script>
			<script type="text/javascript">

				jQuery(document).ready(function() {

					var trip_planner_data = <?php echo $trip_planner_data; ?>;

					var dataSnapshot = null;

					Defiant.getSnapshot(trip_planner_data, function(snapshot) {
					  	// executed when the snapshot is created
					 	dataSnapshot = snapshot;

					 	filterTripPlannerItems();
					 	
					});

					jQuery('.filter input').change(function() {

						filterTripPlannerItems();

					});

					var filterTripPlannerItems = (function() {

						var search_args = {};

						jQuery('.filter input:checked').each(function() {

							var tag = jQuery(this).data('tag');

							if (!search_args.hasOwnProperty(tag)) {
								search_args[tag] = [];
							}

							search_args[tag].push(jQuery(this).val());

						});

						var wrapper_args = '';

						for (var filter_key in search_args) {

							var args = [];

							for (var k in search_args[filter_key]) {
								args.push(filter_key + '="' + search_args[filter_key][k] + '"');

							}

							wrapper_args += '[' + args.join(' or ') + ']';

						}

						wrapper_args += '[category != 190]';

						var query = '//*' + wrapper_args;

						var matchingItems = JSON.search(dataSnapshot, query);  

						/* forced Items */
						var query = '//*[category="190"]';

						var matchingProperties = JSON.search(dataSnapshot, query);  

						for (var i in matchingProperties)
							matchingItems.push(matchingProperties[i]);

						jQuery('.trip-planner-wrap .item-boxes').empty();

						var counts = {};

						_.each(matchingItems, function(item) {

							if (!counts.hasOwnProperty(item.category)) {
								counts[item.category] = 0;
							}

							counts[item.category]++;

							var template = _.template(jQuery('#vg_trip_planner_item_template').html());
					        var html = template({
					            item : item
					        });

					        jQuery('#trip-planner-' + item.category + ' .item-boxes').append(html);


						});

						jQuery('.trip-planner-wrap').each(function() {

							var catID = jQuery(this).attr('id').split('-').pop();

							if (counts.hasOwnProperty(catID)) {
								jQuery(this).find('.message').addClass('none');
							} else {
								jQuery(this).find('.message').removeClass('none');
							}

						});


					});


				});


			</script>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>