jQuery(document).ready(function() {

	if (jQuery('#vg_auto_submit').length) {

		jQuery('#vg_auto_submit').submit();

	}

	// add top section padding
	jQuery('.home #page').css('padding-top',jQuery('.site-header').outerHeight());

	// play homepage video
	if(jQuery('.home-video video').length){
		jQuery('.home-video video').get(0).play();
	}

	// init lang dropdown
	jQuery('.site-navigation select').select2({
		minimumResultsForSearch: Infinity,
	});

	jQuery('#lang-select-dropdown').change(function(){
		var url = jQuery(this).val();
		window.location.href = url;
	});

	// handle menu display
	jQuery('.site-navigation .item-menu').click(function(){

		ga('send', 'event', 'Menu', 'triggered', 'Click Menu');

		jQuery('body').removeClass('active-search-overlay');
		jQuery('body').toggleClass('active-menu-overlay');
		jQuery('.site-navigation .item-search').removeClass('active');
		jQuery(this).toggleClass('active');
	});

	// handle clicking on search display
	jQuery('.site-navigation .item-search').click(function(){

		ga('send', 'event', 'Search', 'triggered', 'Click Search');

		jQuery('body').removeClass('active-menu-overlay');
		jQuery('body').toggleClass('active-search-overlay');
		jQuery('.site-navigation .item-menu').removeClass('active');
		jQuery(this).toggleClass('active');
		jQuery('.search-overlay .overlay-search-input input').focus().autocomplete('search');
	});

	jQuery('.search-input, .btn-search').click(function(){
		jQuery('.search-input').blur();
		jQuery('.site-navigation .item-search').click();
		jQuery('.search-overlay .overlay-search-input input').val('');
		jQuery('.search-overlay .overlay-search-input input').focus();
	});

	jQuery('.hot-searches .clickable').click(function(){
		var text = jQuery(this).text();
		jQuery('.site-navigation .item-search').click();
		jQuery('.overlay-search-input input').focus().val(text).autocomplete('search', text);
	});

	// handle main menu sub items display
	var rootSubMenus = jQuery('#primary-menu').find('> li > .sub-menu');
	jQuery.each(rootSubMenus, function(){
		var parentId = jQuery(this).parent('li').attr('id');
		jQuery('#submenu-wrap').append('<ul id="submenu-'+parentId+'" class="sub-menu-container" data-parent="'+parentId+'"></ul>');
		jQuery('#submenu-'+parentId).append(jQuery(this).html());
	});

	jQuery('#primary-menu a').click(function(e){

		if(jQuery(this).next().hasClass('sub-menu')){

			e.preventDefault();

			jQuery('#submenu-wrap .sub-menu,#primary-menu > li > a').removeClass('active');
			jQuery(this).addClass('active');
			jQuery('#submenu-wrap .sub-menu-container').removeClass('active');

			var elId = jQuery(this).parent('li').attr('id');
			jQuery('#submenu-'+elId).addClass('active');
			jQuery('#submenu-'+elId).css('left', 0);
		}

	});

	jQuery('#submenu-wrap ul.sub-menu-container a').click(function(e){

		if(jQuery(this).next().hasClass('sub-menu')){

			e.preventDefault();

			var ancestors = jQuery(this).parents('ul.sub-menu').length+1;
			var subMenuContainer = jQuery(this).parents('.sub-menu-container');
			var containerWidth = jQuery('#submenu-wrap').outerWidth();
			var level = jQuery(this).parents('ul.sub-menu').length;

			if(jQuery(this).next().hasClass('sub-menu')){
				jQuery(this).next().addClass('active');
				subMenuContainer.css('left', -Math.abs(ancestors*containerWidth));
			}

			var subMenuHeight = jQuery(jQuery('ul.sub-menu-container .sub-menu.active')[level]).outerHeight();
			subMenuContainer.css('height', subMenuHeight);
			jQuery('#submenu-wrap').css('height', subMenuHeight);	
		}
	});

	// copy main elements as headings in child sub menu
	jQuery.each(jQuery('.sub-menu-container .sub-menu'), function(){
		var firstEl = jQuery(this).prev();
		firstEl.clone().prependTo(jQuery(this));
	});

	// do the same for root menu items
	jQuery.each(jQuery('.sub-menu-container'), function(){
		var parentMenu = jQuery(this).data('parent');
		var aEl = jQuery('#'+parentMenu+' > a');
		aEl.clone().prependTo(jQuery(this));
	});

	jQuery('.sub-menu-container > a, .sub-menu > a').wrap('<li class="sub-menu-head"></li>');

	// add menu back button and header + handlers
	jQuery('#submenu-wrap ul.sub-menu-container, .sub-menu-container .sub-menu').prepend('<li class="menu-item-back"><span class="clickable">< back</span></li>');

	jQuery('.menu-item-back').click(function(){
		var menuWidth = jQuery('.sub-menu-container.active').outerWidth();
		var rootSubMenus = jQuery('#submenu-wrap .sub-menu-container.active');
		var subMenuPos = rootSubMenus.position();
		var level = jQuery(this).parents('ul.sub-menu').length-2;
		var subMenuContainer = jQuery(this).parents('.sub-menu-container');
		var subMenuHeight = jQuery(jQuery('.sub-menu-container.active')[0]).outerHeight();

		if(level >= 0){
			subMenuHeight = jQuery(jQuery('ul.sub-menu-container .sub-menu.active')[level]).outerHeight();
		}

		jQuery(this).parent().removeClass('active');

		rootSubMenus.css('left', subMenuPos.left+menuWidth);

		if(subMenuPos.left+menuWidth > 0){
			jQuery('#primary-menu > li > a.active').removeClass('active');
		}

		subMenuContainer.css('height', subMenuHeight);

	});

	// handle close button in overlays
	jQuery('.menu-overlay .btn-close').click(function(){
		jQuery('body').removeClass('active-menu-overlay');
		jQuery('.nav-item.active').removeClass('active');
	});

	jQuery('.search-overlay .btn-close').click(function(){
		jQuery('body').removeClass('active-search-overlay');
		jQuery('.nav-item.active').removeClass('active');
	});

	// handle setting active menu items
	jQuery('#primary-menu > li.current-menu-ancestor > a, #primary-menu > li.current-menu-item > a').addClass('active');

	if(jQuery('#primary-menu > li.current-menu-item > a').next().hasClass('sub-menu')){
		var elId = jQuery('#primary-menu > li.current-menu-item').attr('id');
		jQuery('#submenu-'+elId+', #submenu-'+elId+' .sub-menu-head > a').addClass('active root');
		jQuery('#submenu-'+elId).css('left', 0);
	}

	var activeEl = jQuery('#submenu-wrap .current-menu-item > a');
	var activeAncestors = activeEl.parents('ul.sub-menu').length;
	var activeSubMenuContainer = activeEl.parents('.sub-menu-container');
	var containerWidth = jQuery('#submenu-wrap').outerWidth();

	jQuery.each(activeEl.parents('ul.sub-menu'), function(){
		jQuery(this).addClass('active');
	});

	activeSubMenuContainer.addClass('active');
	activeSubMenuContainer.css('left', -Math.abs(activeAncestors*containerWidth));
	var nextEl = jQuery('#submenu-wrap .current-menu-item > a').next();

	if(nextEl.hasClass('sub-menu')){
		nextEl.addClass('active');
		nextEl.find('.sub-menu-head > a').addClass('active');

		activeSubMenuContainer.css('left', activeSubMenuContainer.position().left - containerWidth);
	}

	jQuery('#submenu-wrap .current-menu-item > a').addClass('active');
	var parentHeight = jQuery('#submenu-wrap .current-menu-item').parent().outerHeight();
	jQuery('#submenu-wrap .current-menu-item').parents('#submenu-wrap').css('height', parentHeight+80); // +80px for some extra height

	// move login-wrap into main menu section
	jQuery('.login-wrap').appendTo(jQuery('#primary-menu').parent());


	// resize logo on scroll
	window.addEventListener('scroll', function(){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 0,
            logo = jQuery('.logo');
        if (distanceY > shrinkOn) {
            logo.addClass('mini');
        } else {
            if (logo.hasClass('mini')) {
                logo.removeClass('mini');
            }
        }
    });
    
	// get current temp
	jQuery.simpleWeather({
		location: 'Victoria, Gozo, Malta',
		woeid: '',
		unit: 'c',
		success: function(weather) {
			var html = weather.temp+'&deg;'+weather.units.temp;

			jQuery("#weather-temp").html(html);
		},
		error: function() {
			//console.log(error);
		}
	});

	// reposition social menu in footer
	jQuery('.footer-menu .social-menu').wrap('<li id="foot-menu-social"></li>');
	jQuery('#foot-menu-social').prependTo('.footer-menu .menu');

	// init slicks
	jQuery('.page-banners').slick({
		fade: true,
		autoplay: true,
		speed: 1000,
		arrows: false
	});

	jQuery('.checkout-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		arrows: true,
		responsive: [
			{
				breakpoint: 1101,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 601,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true
				}
			},
		]
	});

	jQuery('.page-gallery-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		variableWidth: true,
		// centerMode: true
	});

	jQuery('.leader-slides').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		adaptiveHeight: true
	});

	jQuery('.leader-slides').on('afterChange', function(slick, current){
		var slide = current.currentSlide;
		jQuery('.featured-nav .nav-item').removeClass('active');
		jQuery('.featured-nav .nav-item:eq('+slide+')').addClass('active');

		jQuery('.featured-group').removeClass('active');
		jQuery('.featured-pages .featured-group:eq('+slide+')').addClass('active');
	});

	jQuery('#photo-gallery').slick({
		arrows: false,
		adaptiveHeight: true,
		asNavFor: '#photo-gallery-thumbs'
	});

	jQuery('#photo-gallery-thumbs').slick({
		arrows: true,
		asNavFor: '#photo-gallery',
		focusOnSelect: true,
		slidesToShow: 11,
		centerMode: true,
		responsive: [
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
			},
		]
	});

	jQuery('#video-gallery').slick({
		arrows: false,
		adaptiveHeight: true,
		asNavFor: '#video-gallery-thumbs'
	});

	jQuery('#video-gallery-thumbs').slick({
		arrows: true,
		asNavFor: '#video-gallery',
		focusOnSelect: true,
		slidesToShow: 11,
		centerMode: true,
		responsive: [
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
			},
		]
	});



	// handle featured pages links
	jQuery('.featured-nav .nav-item').click(function(){
		var index = jQuery('.featured-nav .nav-item').index(jQuery(this));
		jQuery('.leader-slides').slick('slickGoTo', index);

		jQuery('.featured-nav .nav-item').removeClass('active');
		jQuery('.featured-nav .nav-item:eq('+index+')').addClass('active');

		jQuery('.featured-group').removeClass('active');
		jQuery('.featured-pages .featured-group:eq('+index+')').addClass('active');
	});

	/* event tab */
	jQuery('.event-item-wrap .nav-item').click(function() {

		jQuery(this).addClass('active').siblings().removeClass('active');

		var eventKey = jQuery(this).data('eventKey');

		jQuery('.section-upcoming-events .item-boxes.events').hide();

		jQuery('.section-upcoming-events .item-boxes[data-event-type="' + eventKey + '"]').show();

	});

	// init equal heights
	//jQuery('.single-vg_listings article .col, .single-vg_events article .col').matchHeight();

	// handle action buttons
	jQuery('.btn-print').click(function(){
		window.print();
	});

	// hide trip advisor until it loads
	var checkExist = setInterval(function() {
		if (jQuery('#CDSWIDSSP').length) {
		  jQuery('.ta-loading').hide();
		  jQuery('.ta-wrap').show();
		  clearInterval(checkExist);
		}
	}, 100); // check every 100ms

	// handle page gallery video player
	jQuery('.page-gallery-slider .video a, #video-gallery a').click(function(){
		var video = jQuery(this).data('video');
		jQuery('.remodal.video #video-container').html('<iframe width="100%" height="480" src="https://www.youtube.com/embed/'+video+'?controls=0&amp;showinfo=0&amp;autoplay=1&amp;rel=0" frameborder="0" allowfullscreen></iframe>');
	});

	jQuery(document).on('closed', '.remodal.video', function () {
		jQuery('.remodal.video #video-container').html('');
	});

	// handle pool trip contact button
	jQuery('.section-pool-trips .btn-contact').click(function(){
		var userEmail = atob(jQuery(this).data('e'));
		jQuery('#field_jbgm3').val(userEmail);
	});

	// handle webcam switching in Live Ferry Queue and Panoramic Webcams
	jQuery('.webcam-tabs .clickable').click(function(){
		activeCam = jQuery(this).data('cam');
		var src = jQuery('.webcam-embeds #webcam-'+activeCam).data('src');

		jQuery('.webcam-tabs .tab-item, .webcam-embeds .webcam-embed-item').removeClass('active');
		jQuery('.webcam-embeds iframe').attr('src', '');

		jQuery(this).addClass('active');
		jQuery('.webcam-embeds #webcam-'+activeCam).addClass('active');
		jQuery('.webcam-embeds #webcam-'+activeCam+' iframe').attr('src', src);

	});
	
	if(jQuery('.webcam-embeds-https').length){

		var activeCam = 1;

		if(jQuery('.webcam-embeds-https').hasClass('panoramic')){
			jQuery.each(jQuery('#panoramic-dummy > div'), function(){
				var wc = jQuery(this).attr('id').split('-')[1];
				// var iFrameSrc = jQuery(this).find('iframe').attr('src');
				// var t = iFrameSrc.split('iframe?t=')[1];

				// var wcFrame = jQuery('.webcam-embeds-https #webcam-'+wc+' iframe');
				// var src = jQuery(wcFrame).attr('src');
				// jQuery(wcFrame).attr('src', src+t);
				// console.log(wc, t);

				console.log('cloning ' + wc);
				jQuery(this).find('iframe').clone().appendTo('.webcam-embeds-https.panoramic #webcam-' + wc);
			});
		}

		// load first cam by default
		setTimeout(function(){
			jQuery('.webcam-embeds-https #webcam-1').addClass('active');
		}, 1000);

		// handle webcam switching in Live Ferry Queue
		jQuery('.webcam-tabs.https .clickable').click(function(){

			activeCam = jQuery(this).data('cam');
			console.log('Switching to... '+activeCam);

			jQuery('.webcam-tabs.https .tab-item, .webcam-embeds-https .jwplayer').removeClass('active');

			jQuery(this).addClass('active');
			jQuery('.webcam-embeds-https .webcam-embed-item').removeClass('active');
			jQuery('.webcam-embeds-https #webcam-'+activeCam).addClass('active');

		});
	}

	// // hide Panoramic webcams after few seconds
	// setTimeout(function(){
	// 	console.log('hide cams');
	// 	jQuery('.webcam-embed-item.panoramic').css('display', 'none');
	// 	jQuery('.webcam-embed-item.panoramic#webcam-1').addClass('active');
	// }, 3000);
	
	

	/* Search System */
	var currentSearchResults = {};
	var currentSearchTerm = '';
	var termSelected = false;

	var $searchAutoComplete = jQuery( ".overlay-search-input input").autocomplete({
        source: function(request, cb) {

        	currentSearchTerm = request.term;

            var form = jQuery.param({
				keywords : request.term,
				search_lang : vgCurrentLang,
				min_score : 0.25
			});

            var qs = {
				action : 'vg_search',
				form : form
			};

			jQuery('.overlay-search-input').addClass('searching');

            jQuery.getJSON(
                vgAjaxURL, 
                qs, 
                function(response){

                	jQuery('.overlay-search-input').removeClass('searching');

                	if (response.data.length) {
                		jQuery('.no-search-results').addClass('none');
                		ga('send', 'event', 'Search', 'results_returned', 'Term: ' + request.term, response.data.length);
                	} else {
                		jQuery('.no-search-results').removeClass('none');
                		ga('send', 'event', 'Search', 'no_results_returned', 'Term: ' + request.term, response.data.length);
                	}

                    //console.log('response');
                    //console.log(response);

                    var myGozoEntries = vgMyGozo.get_entries();

					for (var i in response.data) {
						response.data[i].in_my_gozo = _.contains(myGozoEntries, response.data[i].post_id);
						response.data[i].permalink  += '?search_term=' + request.term;
					}

					currentSearchResults = response.data;

                    var template = _.template(jQuery('#tmpl_auto_complete_search').html());

                    var results = [];

                    for (var i in response.data) {
                        var html =  template(response.data[i]);

                        results.push({
                            value : response.data[i].title,
                            label : html
                        });

                    }

                    return cb(results);

                },
                'json'
            );

        },
        minLength: 3,
        open : function() {
            
        },
        select: function( event, ui ) {

        	termSelected = true;

        	var resp = _.findWhere(currentSearchResults, {title : ui.item.value});

        	ga('send', 'event', 'Search', 'search_result_clicked', 'Term: ' + currentSearchTerm + 'Clicked: ' + resp.title);

        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {

		ul.addClass('item-boxes listings oflow');

        return jQuery('<li class="item-box"></li>')
            .data("item.autocomplete", item)
            .append(item.label)
            .appendTo(ul);
    };

    jQuery( ".overlay-search-input input").data("ui-autocomplete").close = function (ul, item) {
    	// do nothing since we always want this to show
    };

    
    /* My Gozo Interaction */
	vgMyGozo.init(function(entries) {

		jQuery('.quick-links .icon-heart a').attr('data-count', entries.length);

	});

	jQuery('body').on('click', '.my-gozo-trigger', function(e) {

		e.stopPropagation();
		e.preventDefault();

		var $el = jQuery(this);

		$el.addClass('adding');

		var entries = vgMyGozo.get_entries();

		var postID = jQuery(this).data('listingId');

		var index = entries.indexOf(postID);

		if (index === -1) {

			vgMyGozo.add_entry(postID, function(entries) {

				jQuery('.quick-links .icon-heart a').attr('data-count', entries.length);

				$el.removeClass('adding').addClass('added');

			});


		} else {

			vgMyGozo.delete_entry(postID, function(entries) {

				jQuery('.quick-links .icon-heart a').attr('data-count', entries.length);

				$el.removeClass('adding added');

			});

		}

		

	});
	
	jQuery( ".subscribe-button-container input.button" ).click(function() {
		event.preventDefault();	
		if (jQuery('input#terms-newsletter').is(':checked')) {			
			if ( (jQuery('input#mce-EMAIL').val() !== '') && ( validateEmail(jQuery('input#mce-EMAIL').val())  ) ) {
				
				jQuery('#mc-embedded-subscribe-form').submit();
			}
			else{
				alert('You have to complete your email with a valid address');
				jQuery('input#mce-EMAIL').focus();
			}
		}
		else{
			alert('You have to accept Terms & Conditions');
			jQuery('input#terms-newsletter').focus();
		}
	});
	
	
	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	

});

/* My Gozo */
var vgMyGozo = {
	entries : [],
	init : function(cb) {
		this.load_entries(cb);
	},
	set_entries : function(entries) {
		this.entries = entries;
	},
	get_entries : function() {
		return this.entries;
	},
	add_entry : function(vg_post_id, cb) {

		var self = this;

		jQuery.post( vgAjaxURL, { action: 'vg_insert_my_gozo_entry', vg_post_id: vg_post_id }, function( data ) {

			self.entries.push(vg_post_id);

			cb(self.entries);

		}, "json");


	},
	delete_entry : function(vg_post_id, cb) {

		var self = this;

		jQuery.post( vgAjaxURL, { action: 'vg_delete_my_gozo_entry', vg_post_id: vg_post_id }, function( data ) {

			var index = self.entries.indexOf(vg_post_id);

			if (index !== -1) {

				delete self.entries[index];

			}

			cb(self.entries);

		}, "json");


	},
	load_entries : function(cb) {

		var self = this;

		jQuery.post( vgAjaxURL, { action: 'vg_get_my_gozo_entries' }, function( data ) {
			self.set_entries(data);
			cb(data);
		}, "json");
	}

};

