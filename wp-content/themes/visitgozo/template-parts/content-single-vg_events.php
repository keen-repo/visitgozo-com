<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php
		/* EVENT GALLERY */
		vg_render_event_gallery();
	?>

	<div class="entry-content row oflow">
		
		<div class="item-sidebar col col-25 fr">
			<?php $data = rwmb_meta( $prefix.'listing-locality' ); if(!empty($data)){ ?>
				<p class="location"><?php echo $data; ?></p>
			<?php } ?>
			<p class="info">
				<?php $data = rwmb_meta( $prefix.'event-venue' ); if(!empty($data)){ ?>
					<span class="info-item"><?php _e( 'Venue', 'vg-front' ); ?>: <?php echo $data; ?> <?php echo rwmb_meta( $prefix.'listing-address-2' ); ?></span>
				<?php } ?>
				<?php $data = rwmb_meta( $prefix.'event-website' ); 
				if(!empty($data)){ 
					if(substr($data, 0, 4) != "http") {
						$data = 'http://' . $data;
					}
				?>
					<span class="info-item">Web: <a href="<?php echo $data; ?>" target="_blank"><?php _e('Visit Website', 'vg-front'); ?></a></span>
				<?php } ?>
				<?php $data = rwmb_meta( $prefix.'event-contact-no' ); if(!empty($data)){ ?>
					<span class="info-item">Tel: <a href="tel:<?php echo $data; ?>"><?php echo $data; ?></a></span>
				<?php } ?>
				<?php $data = rwmb_meta( $prefix.'event-email' ); if(!empty($data)){ ?>
					<span class="info-item">Email: <a href="mailto:<?php echo $data; ?>" target="_blank"><?php echo $data; ?></a></span>
				<?php } ?>
			</p>
			<ul class="item-actions">
				<li><span class="btn-print clickable icon-print"><?php _e( 'Print', 'vg-front' ); ?></span></li>
				<li><span class="btn-share clickable icon-share" data-remodal-target="social-share"><?php _e( 'Share', 'vg-front' ); ?></span></li>
				<li><span class="btn-add-mygozo clickable icon-heart my-gozo-trigger" data-listing-id="<?php the_ID(); ?>"><?php _e( 'Add to My Gozo', 'vg-front' ); ?></span></li>
			</ul>
			<div class="remodal social-share" data-remodal-id="social-share">
				<button data-remodal-action="close" class="remodal-close"></button>
				<h2><?php _e( 'Share link on Social Media', 'vg-front' ); ?></h2>
				<?php do_action( 'addthis_widget', get_the_permalink(), get_the_title(), 'large_toolbox' ); ?>	
			</div>
		</div>
		<div class="item-desc col col-70">
			<div class="row">
				<?php 
					$poster = rwmb_meta( $prefix.'event-poster' );
					$poster = current($poster);

					if(!empty($poster)){ ?>
						<div class="col col-50 item-poster"><?php echo wp_get_attachment_image( $poster['ID'], 'medium-uncropped' ); ?></div>
						<div class="col col-50"><?php 
							vg_render_event_date();
							the_content(); 
						?></div>
					<?php } else {
						the_content();

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vg' ),
							'after'  => '</div>',
						) );
					}
				?>
			</div>
		</div>
	</div><!-- .entry-content -->

	<?php 
		$eventVideo = rwmb_meta( $prefix.'event-video' );
		if( !empty($eventVideo) ){
			parse_str( parse_url( $eventVideo, PHP_URL_QUERY ), $params ); ?>
		<div class="section section-event-video section-padding-top-bottom">
			<iframe width="853" height="480" src="https://www.youtube.com/embed/<?php echo $params['v']; ?>?controls=0&amp;showinfo=0&amp;rel=0" frameborder="0" allowfullscreen></iframe>
		</div>
		<?php }
	?>

	<?php

		$lat = rwmb_meta( $prefix.'event-latitude' );
		$lon = rwmb_meta( $prefix.'event-longitude' );

		if( !empty($lat) && !empty($lon) ) {
			vg_render_maps( get_the_title(), $lat, $lon );
		} ?>

	<footer class="entry-footer">
		<?php vg_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

