<?php

global $prefix;

/* CHILD PAGES SECTION */
$show_child_pages = get_post_meta(icl_object_id(get_the_ID(), 'page', false, 'en'), $prefix . 'show_child_pages', true);

if ($show_child_pages) {
	$subpages = get_posts(array(
		'post_parent' => get_the_ID(), 
		'fields' 	  => 'ids',
		'post_type'   => 'page',
		'numberposts' => -1,
		'orderby'	  => 'post_title',
		'order'	  	  => 'ASC',
	));
} else {
	/* SUB PAGES SECTION */
	$subpages = rwmb_meta( $prefix . 'subpages', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );
}

/* SUB PAGES SECTION */
//$subpages = rwmb_meta( $prefix . 'subpages', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );

$count = 1;

if( count($subpages) > 0 ){ ?>
	<div class="subpages wrap oflow"> 
		<div class="row oflow">
			<?php foreach( $subpages as $subpageID ){
				$featuredID = get_post_thumbnail_id( icl_object_id($subpageID, 'page', false, 'en') );
				if( !empty($featuredID) ){
					$featuredArr = wp_get_attachment_image_src( $featuredID, 'subpage-featured', true );
					$featuredImage = $featuredArr[0];
				} else {
					$featuredImage = '';
				} ?>
				<a class="subpage-box" href="<?php echo get_permalink( icl_object_id($subpageID, 'page', false) ); ?>">
					<div class="subpage-image-wrap relative oflow">
						<div class="subpage-image bg-cover fullwidth-fullheight" style="background-image: url('<?php echo $featuredImage; ?>')"></div>
					</div>
					<h2 class="subpage-title"><?php echo get_the_title( icl_object_id($subpageID, 'page', false) ); ?></h2>
					<p class="subpage-intro"><?php echo rwmb_meta( $prefix . 'page-intro-text', array(), icl_object_id($subpageID, 'page', false) ); ?></p>
				</a>
			<?php 
				if( $count%2 == 0 && $count != count($subpages) ){
					echo '</div><div class="row oflow">';
				}

				$count++;
			} ?>
		</div>
	</div>
<?php }

?>