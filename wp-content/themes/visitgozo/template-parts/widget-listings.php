<?php


$categories = explode(',', $atts['categories']);
$localities = explode(',', $atts['localities']);

$widget_type = $atts['widget_type'];

?>
<form id="vg_search_form" method="post" action="" />
	<div class="listings-filters row">
		<div class="col col-75 row">
			<div class="col col-50">
			<h3 class="section-title"><?php _e( 'Category', 'vg-front' ); ?></h3>
				<ul class="category-list fl">

				<?php
				
					/* Categories */
					if ($widget_type == 'listing') {
						$taxonomy = 'vg_listings_cats';
						$term_list = vg_build_term_tree($taxonomy);
					}

					if ($widget_type == 'event') {
						$taxonomy = 'vg_events_cats';
						$term_list = vg_build_term_tree($taxonomy);
					}

					foreach ($term_list as $term_id=>$term_tree) {

						if (in_array($atts['allow_unselected_categories'], array('','no')) && !empty($categories)) {

							if (!in_array($term_id, $categories)) {
								$ancestors = get_ancestors( $term_id, $taxonomy );

								$found_ancestor = false;
								foreach ($ancestors as $ancestor) {

								 	if (in_array($ancestor, $categories)) {
								 		$found_ancestor = true;
								 	}

								}

								if (!$found_ancestor) {
									continue;
								}

							}

						}

						/* take code from vg_frm_location_posts */

						$class = array();
						$checked = '';
						if (in_array($term_id, $categories)) {
							$checked = 'checked="checked"';
							$class[] = 'active';
						}

						echo '<li class="' . implode(' ', $class) . '"><label><input type="checkbox" name="categories[]" ' . $checked . ' value="' . $term_id . '" />' . $term_tree . '</label></li>';

					}

				?>

				</ul>
			</div>
			<div class="col col-50">
				<?php 

				/* Locality */
				$args = array(
				    'taxonomy' => 'vg_locality_cats',
				);

				if ($atts['allow_unselected_localities'] == 'no' && !empty($localities)) {
					$args['include'] = $localities;
				}

				$terms = get_terms($args);

				?>
				<h3 class="section-title"><?php _e( 'Location', 'vg-front' ); ?></h3>
				<ul style="float: left;" class="locality-list">
					<?php 
					foreach ($terms as $term) {

						$class = array();
						$checked = '';

						if (in_array($term->term_id, $localities)) {
							$checked = ' checked="checked" ';
							$class[] = 'active';
						}

						echo '<li class="' . implode(' ', $class) . '"><label><input type="checkbox" name="localities[]"' . $checked . 'value="' . $term->term_id . '" />' . $term->name . '</label></li>';
					}
					?>
				</ul>
			</div>
		</div>

		<div class="col col-25">
			<h3>&nbsp;</h3>
			<input type="text" name="keywords" value="" placeholder="<?php _e( 'Keywords', 'vg-front' ); ?>" />
			<input type="submit" name="submit" class="btn-submit clickable" value="<?php _e( 'Search', 'vg-front' ); ?>" />
		</div>

		<div class="none">
			<input type="checkbox" name="geo_query" value="1" />
			<input type="text" name="latitude" value="" />
			<input type="text" name="longitude" value="" />
			<input type="text" name="letter_range" value="" />
			<input type="hidden" name="widget_type" value="<?php echo $widget_type; ?>" />
			<input type="hidden" name="search_lang" value="<?php echo ICL_LANGUAGE_CODE; ?>" />
		</div>
	</div>
</form>

<h3 class="section-title section-padding-top"><?php _e( 'Results found', 'vg-front' ); ?></h3>

<div class="listings-sorting oflow">

	<ul class="letter-filters fl">
		<li class="label"><?php _e( 'View', 'vg-front' ); ?>:</li>
		<li><span class="clickable active" data-letter-range=""><?php _e( 'All', 'vg-front' ); ?></span></li>
		<li><span class="clickable" data-letter-range="a-f">A-F</span></li>
		<li><span class="clickable" data-letter-range="g-l">G-L</span></li>
		<li><span class="clickable" data-letter-range="m-r">M-R</span></li>
		<li><span class="clickable" data-letter-range="s-z">S-Z</span></li>
	</ul>

	<ul class="sorting-options fl">
		<li class="label"><?php _e( 'Sort by', 'vg-front' ); ?>:</li>
		<?php if ($widget_type == 'event') { ?>
			<li><span data-sort-by="relevance" class="sort-by clickable active"><?php _e( 'Date', 'vg-front' ); ?></span></li>
		<?php } else { ?>
			<li><span data-sort-by="relevance" class="sort-by clickable active"><?php _e( 'Relevance', 'vg-front' ); ?></span></li>
		<?php } ?>
		<li><span data-sort-by="distance" class="sort-by clickable"><?php _e( 'Distance', 'vg-front' ); ?></span></li>
	</ul>

	<?php

	if ($widget_type == 'listing') {
		$filepath = get_template_directory() . '/cache/listing_filters.json';
	}

	if ($widget_type == 'event') {
		$filepath = get_template_directory() . '/cache/event_filters.json';
	}
	

	$json = file_get_contents($filepath);

	?>
</div>

<div id="listing-data-container" class="item-boxes listings oflow">
<!-- this is where we embed all listing data -->
</div>

<div id="pagination" class="section section-padding-top"></div>
<script type="text/template" id="vg_listing_item_template">
    <% for (var i = 0, len = data.length; i < len; i++) { %>
    	<a class="item-box" href="<%= data[i].permalink %>">
			<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<%= data[i].thumbnail[0] %>');"></div>
			<?php if ($widget_type == 'event') { ?>
				<div class="item-date">
					<span class="event-day"><%= data[i].date.date %></span>
					<span class="event-mon"><%= data[i].date.month %></span>
				</div>
			<?php } ?>
			<div class="item-title row">
				<span class="col col-80"><%= data[i].title %></span>
				<span class="col col-20 actions"><span data-listing-id="<%= data[i].post_id %>" class="icon-heart my-gozo-trigger <% if (data[i].in_my_gozo) { %>added<% } %>"></span></span>
			</div>
		</a>
    <% } %>
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {

		<?php if (isset($_GET['filters']) && !empty($_GET['filters'])) { ?>

			var presetFilters = <?php echo json_encode($_GET['filters']); ?>;

		<?php } else { ?>

			var presetFilters = {};

		<?php } ?>

		<?php if (isset($_GET['pageNum']) && !empty($_GET['pageNum'])) { ?>

			var presetpageNum = <?php echo (int)$_GET['pageNum']; ?>;

		<?php } else { ?>

			var presetpageNum = 1;

		<?php } ?>

		(function(window,undefined){

		    // Bind to StateChange Event
		    History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
		        var State = History.getState(); // Note: We are using History.getState() instead of event.state
		    });

		})(window);

		var vgFilters = { 
			initialized     : false,
			pageNum 		: presetpageNum,
			pager 			: null,
			dataSnapshot 	: null,
			registeredFilters : {
				'category' : {
					selector : 'category-list',
					prefix 	 : 'c'
				},
				'locality' : {
					selector : 'locality-list',
					prefix 	 : 'l'
				}
			},
			init : function(snapshot) {

				this.dataSnapshot = snapshot;

				this.parseState();

				/* First set initial filters just in case a filter is already selected */
				for (var f in this.registeredFilters) {

					this.adjustFilters(f);

				}

				/* Initial Form Submission */
				this.submitForm();

				/* Setup Pagination */
				this.pager = jQuery('#pagination');

			},
			adjustFilters : function(filter_type) {

				var the_filter = this.registeredFilters[filter_type];

				var selectedVals = [];
				jQuery('.' + the_filter.selector + ' li.active input:checked').each(function() {
					selectedVals.push(jQuery(this).val());
				});

				var args = [];
				for (var i in selectedVals) {
					args.push(the_filter.prefix + '="' + selectedVals[i] + '"');
				}

				var query = (args.length) ? '//*[' + args.join(' or ') + ']' : '//*';

				var found = JSON.search(this.dataSnapshot, query);  

				for (var f in this.registeredFilters) {

					var filter = this.registeredFilters[f];

					/* dont perform checking on the currently changed filter */
					if (f == filter_type) continue;

					var unique_data = _.chain(found).pluck(filter.prefix).flatten().union().value();

					jQuery('.' + filter.selector + ' li').hide().removeClass('active');

					_.each(unique_data, function(v) {

						jQuery('input[value="' + v + '"]').parent().parent().show().addClass('active');

					});

				}

			},
			parseState : function() {

				for (var filter_type in presetFilters) {

					jQuery('.' + presetFilters[filter_type].selector + ' li.active input:checked').removeAttr('checked').parent().parent().removeClass('active');

					for (var i in presetFilters[filter_type]) {
						jQuery('input[value="' + presetFilters[filter_type][i] + '"]').attr('checked', 'checked').parent().parent().addClass('active');
					}

				}


			},
			setState : function() {

				var state = {filters : {}};

				for (var filter_type in this.registeredFilters) {

					var selectedVals = [];
					jQuery('.' + this.registeredFilters[filter_type].selector + ' li.active input:checked').each(function() {
						selectedVals.push(jQuery(this).val());
					});

					state['filters'][filter_type] = selectedVals;

				}

				state['pageNum'] = this.pager.pagination('getSelectedPageNum');

				var qs = jQuery.param(state);

				var title = jQuery('title').text();

				History.replaceState(state, title, '?' + qs);

			},
			submitForm : function() {

				var self = this;

				/* We run this to setup the counts */
				var form = jQuery('#vg_search_form').serialize();

				jQuery.getJSON( '<?php echo admin_url('admin-ajax.php'); ?>', { action: "vg_count_search", form : form } )
				  .done(function( json ) {

				    self.configurePagination(json.totals);

				  })
				  .fail(function( jqxhr, textStatus, error ) {
				    var err = textStatus + ", " + error;
				    console.log( "Request Failed: " + err );
				});

			},
			configurePagination : function(totals) {

				var self = this;

				if (this.initialized) {
					this.pager.pagination('destroy');
				}

				var qs = {
					action : 'vg_search',
					form : jQuery('#vg_search_form').serialize()
				};

				var search_args = jQuery.param( qs );

				this.pager.pagination({
				    dataSource: '<?php echo admin_url('admin-ajax.php'); ?>?' + search_args,
					locator : 'data',
					totalNumber: totals.all,
				    pageSize: 12,
				    pageNumber : this.pageNum,
				    afterInit : function () {

				    	if (self.initialized === false) {
							self.pageNum = 1;

							setTimeout(function() {
								self.initialized = true;
							}, 1000);
							
						}

				    },
				    callback: function(data, pagination) {

				    	var myGozoEntries = vgMyGozo.get_entries();

				    	for (var i in data) {
				    		data[i].in_my_gozo = _.contains(myGozoEntries, data[i].post_id);
				    	}

				        var template = _.template(jQuery('#vg_listing_item_template').html());
				        var html = template({
				            data: data
				        });

				        jQuery('#listing-data-container').html(html);
				    },
				    afterPaging : function(pageNum) {

				    	if (self.initialized === true) {
				    		self.setState();

				    		jQuery('html,body').animate({
							   scrollTop: jQuery(".listings-sorting").offset().top - 180
							});
				    	}

				    	
				    }
				});

			}
		};


		var dynamicWidgetPool = <?php echo $json; ?>;

		Defiant.getSnapshot(dynamicWidgetPool, function(snapshot) {
		  	// executed when the snapshot is created
		 	vgFilters.init(snapshot);
		 	
		});


		/* Geolocator configuration */
		geolocator.config({
	        language: "en",
	        google: {
	            version: "3",
	            key: "AIzaSyCkHD2VGifA898zYqRHSAhKg9C_vzPegHE"
	        }
	    });

		/* 
		 *
		 * Dom Interaction 
		 *
  		 *
		*/
		jQuery('.category-list input').change(function() {

			vgFilters.adjustFilters('category');

			vgFilters.submitForm();

			vgFilters.setState();

		});


		jQuery('.locality-list input').change(function() {

			vgFilters.adjustFilters('locality');

			vgFilters.submitForm();

			vgFilters.setState();

		});

		jQuery('.letter-filters span').click(function(e) {

			e.preventDefault();

			jQuery('.letter-filters span').removeClass('active')

			jQuery(this).addClass('active');

			var letter_range = jQuery(this).data('letterRange');

			jQuery('input[name="letter_range"]').val(letter_range);

			vgFilters.submitForm();

		});

		jQuery('.sort-by').click(function(e) {

			e.preventDefault();

			var sort_by = jQuery(this).data('sortBy');

			if (sort_by == 'relevance') {
				jQuery('input[name="geo_query"]').removeAttr('checked');

				vgFilters.submitForm();
			}

			if (sort_by == 'distance') {

				var options = {
		            enableHighAccuracy: true,
		            timeout: 6000,
		            maximumAge: 0,
		            desiredAccuracy: 30,
		            fallbackToIP: true, // fallback to IP if Geolocation fails or rejected
		            addressLookup: false,
		            timezone: false,
		        };
		        geolocator.locate(options, function (err, location) {

		            if (err) {
		            	alert('There was an error obtaining your location.');
		            	return console.log(err);
		            }

		            jQuery('input[name="latitude"]').val(location.coords.latitude);
		            jQuery('input[name="longitude"]').val(location.coords.longitude);

		            jQuery('input[name="geo_query"]').attr('checked', 'checked');

		            vgFilters.submitForm();

		        });

				

			}

			

		});

		jQuery('#vg_search_form').submit(function(e) {

			e.preventDefault();

			vgFilters.submitForm();


		});


		

		

	});
</script>

<?php


/* embed all listings into spastic elastic gymnastic 

$listings = get_posts(array('post_type' => 'page', 'numberposts' => -1));

foreach ($listings as $listing) {

	$resp = vg_elastic_index_post($listing->ID, $listing);

	echo '<pre>';
	print_r($resp);
	echo '</pre>';

}


$listings = get_posts(array('post_type' => 'vg_events', 'numberposts' => -1));

foreach ($listings as $listing) {

	$resp = vg_elastic_index_event($listing->ID, $listing);

	echo '<pre>';
	print_r($resp);
	echo '</pre>';

}

exit('END');

*/

?>
