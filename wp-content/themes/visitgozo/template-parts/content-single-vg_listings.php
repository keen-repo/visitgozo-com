<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php
		/* LISTING GALLERY */
		vg_render_listing_gallery();
	?>

	<div class="entry-content row oflow">
		<div class="item-sidebar col col-25 fr">
			<?php $data = rwmb_meta( $prefix.'listing-locality' ); if(!empty($data)){ ?>
				<p class="location"><?php echo $data; ?></p>
			<?php } ?>
			<p class="info">
				<?php $data = rwmb_meta( $prefix.'listing-address-1' ); if(!empty($data)){ ?>
					<span class="info-item"><?php _e( 'Address', 'vg-front' ); ?>: <?php echo $data; ?> <?php echo rwmb_meta( $prefix.'listing-address-2' ); ?></span>
				<?php } ?>
				<?php $data = rwmb_meta( $prefix.'listing-website' ); 
				if(!empty($data)){ 
					if(substr($data, 0, 4) != "http") {
						$data = 'http://' . $data;
					}
				?>
					<span class="info-item">Web: <a href="<?php echo $data; ?>" target="_blank"><?php _e('Visit Website', 'vg-front'); ?></a></span>
				<?php } ?>
				<?php $data = rwmb_meta( $prefix.'listing-email' ); if(!empty($data)){ ?>
					<span class="info-item">Email: <a href="mailto:<?php echo $data; ?>" target="_blank"><?php echo $data; ?></a></span>
				<?php } ?>
				<?php $data = rwmb_meta( $prefix.'listing-contact-no' ); if(!empty($data)){ ?>
					<span class="info-item">Tel: <?php echo $data; ?></span>
				<?php } ?>

				<?php
				$opening_hours = get_post_meta($prefix . 'opening-hours', true);
				?>
			</p>
			<ul class="item-actions">
				<li><span class="btn-print clickable icon-print"><?php _e( 'Print', 'vg-front' ); ?></span></li>
				<li><span class="btn-share clickable icon-share" data-remodal-target="social-share"><?php _e( 'Share', 'vg-front' ); ?></span></li>
				<li><span class="btn-add-mygozo clickable icon-heart my-gozo-trigger" data-listing-id="<?php the_ID(); ?>"><?php _e( 'Add to My Gozo', 'vg-front' ); ?></span></li>
			</ul>
			<div class="remodal social-share" data-remodal-id="social-share">
				<button data-remodal-action="close" class="remodal-close"></button>
				<h2><?php _e( 'Share link on Social Media', 'vg-front' ); ?></h2>
				<?php do_action( 'addthis_widget', get_the_permalink(), get_the_title(), 'large_toolbox' ); ?>	
			</div>
		</div>
		<div class="item-desc col col-70">
			<?php

			$the_content = get_the_content(); 

			if (ICL_LANGUAGE_CODE != 'en') {
				$lang_content = get_post_meta(get_the_ID(), $prefix . 'listing-' . ICL_LANGUAGE_CODE . '-description', true);

				if (!empty($lang_content) && $lang_content != $the_content) {
					$the_content = $lang_content;
				}
			}

			echo apply_filters( 'the_content', $the_content );

			?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vg' ),
					'after'  => '</div>',
				) );
			?>
		</div>
	</div><!-- .entry-content -->

	<?php

		$lat = rwmb_meta( $prefix.'listing-latitude', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );
		$lon = rwmb_meta( $prefix.'listing-longitude', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );

		if( !empty($lat) && !empty($lon) ) {
			vg_render_maps( get_the_title(), $lat, $lon );
		} ?>

	<?php
	$tripadvisor_id = get_post_meta(get_the_ID(), $prefix . 'listing-tripadvisor-id', true);

	if (!empty($tripadvisor_id)) {
	?>

	<div class="section section-ta review section-padding-bottom relative">
		<div class="ta-loading txt-aligncenter section-padding-top-bottom"><?php _e( 'Loading reviews', 'vg-front' ); ?>...</div>
		<div class="ta-wrap none">
			<div id="TA_selfserveprop106" class="TA_selfserveprop">
			<ul id="B7OSLg1gK" class="TA_links Qj5n8KUXQ">
			<li id="9U6nTJEll" class="5YvttpI">
			<a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
			</li>
			</ul>
			</div>
			<script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=106&amp;locationId=<?php echo $tripadvisor_id; ?>&amp;lang=en_US&amp;rating=true&amp;nreviews=4&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=true&amp;border=false&amp;display_version=2"></script>
		</div>
	</div>

	<?php } ?>

	<footer class="entry-footer">
		<?php vg_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

