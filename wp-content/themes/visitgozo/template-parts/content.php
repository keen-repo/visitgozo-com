<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

?>

<a class="item-box" href="<?php the_permalink(); ?>">
	<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<?php the_post_thumbnail_url( 'subpage-featured' ); ?>"></div>
	<div class="item-title"><span class="blog-date"><?php the_date(); ?><br></span><?php the_title(); ?></div>
</a>
