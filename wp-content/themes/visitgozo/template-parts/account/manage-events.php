<?php get_header(); ?>

	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php 

			while ( have_posts() ) : the_post(); 

				echo do_shortcode( '[formidable id=9]' ); 

			endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>