<?php get_header(); ?>

	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php 

			while ( have_posts() ) : the_post(); 

				$doing_tripadvisor_call = false;

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {

					if (isset($_POST['task']) && $_POST['task'] == 'populate_tripadvisor_data') {

						preg_match_all("/d([0-9]*)-/", $_POST['trip_advisor_link'], $out, PREG_PATTERN_ORDER);

						if (!empty($out) && isset($out[1][0])) {

							$listingID = $out[1][0];

							$listingDataResponse = vg_tripadvisor_get_single_listing($listingID);

							if ($listingDataResponse['success']) {

								$listingData = $listingDataResponse['data'];

								$doing_tripadvisor_call = true;

								$terms = get_terms('vg_locality_cats', array(
									'hide_empty' => false, 
									'meta_query' => array(
									array(
										'key' => '_vg_tripadvisor_name',
										'value' => $listingData->address_obj->city
									)
								)));

								$city = '';

								if (!empty($terms)) {
									$city = $terms[0]->term_id;
								}

								/* the form below will automatically be submitted */

								?>

								<h1>working...</h1>
								<form method="get" action="" id="vg_auto_submit">
									<input type="hidden" name="listing_url" value="<?php echo esc_html($_POST['trip_advisor_link']); ?>" />
									<input type="hidden" name="listing_name" value="<?php echo $listingData->name; ?>" />
									<input type="hidden" name="listing_address_1" value="<?php echo $listingData->address_obj->street1; ?>" />
									<input type="hidden" name="listing_address_2" value="<?php echo $listingData->address_obj->street2; ?>" />
									<input type="hidden" name="listing_city" value="<?php echo $city; ?>" />
									<input type="hidden" name="listing_latitude" value="<?php echo $listingData->latitude; ?>" />
									<input type="hidden" name="listing_longitude" value="<?php echo $listingData->longitude; ?>" />
									<input type="submit" name="go" value="" />
								</form>
								<?php

							}

						}	

					}

				}

				if (!$doing_tripadvisor_call) {

					/*
				
					*/

					if (!isset($_GET['frm_action'])) {

						?>
						<form method="post" action="">

							<input type="hidden" name="task" value="populate_tripadvisor_data" />

							<label>Trip Advisor Link:</label>
							<input type="text" name="trip_advisor_link" value="" />
							<input type="submit" value="Populate Data" />
						</form>

						<?php


					}


					echo do_shortcode( '[formidable id=8]' ); 

				}

				

				

			endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>