<?php

global $prefix;

$categories = array(
	'page' => array(
		'title' => __('Points of Interest', 'vg-front'),
		'data' => array(),
	),
	'vg_listings' => array(
		'title' => __('Listings', 'vg-front'),
		'data' => array(),
	),
	'vg_events' => array(
		'title' => __('Events', 'vg-front'),
		'data' => array(),
	),
);

foreach ($entries as $entry) {

	$post = get_post($entry);

	$categories[$post->post_type]['data'][] = $post;

}

foreach ($categories as $category) {

	if (empty($category['data'])) continue;

	?>
	<h2 class="section-title"><?php echo $category['title']; ?></h2>

	<div class="item-boxes oflow">
		<?php
		foreach ($category['data'] as $post) { 

			if ($post->post_type == 'vg_events') {
				$thumbnail_id = get_post_meta($post->ID, $prefix . 'event-poster', true);
			} else {
				$thumbnail_id = get_post_thumbnail_id($post->ID);
			}

			$image = wp_get_attachment_image_src($thumbnail_id, 'medium-uncropped');

		?>
    	<a class="item-box" href="<?php echo get_permalink($post->ID); ?>">
			<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<?php echo $image[0]; ?>');"></div>
			<div class="item-title row">
				<span class="col col-80"><?php echo $post->post_title; ?></span>
				<span class="col col-20 actions"><span data-listing-id="<?php echo $post->ID; ?>" class="icon-heart my-gozo-trigger added"></span></span>
			</div>
		</a>
		<?php
		}

		wp_reset_postdata();
		?>
	</div>
	<?php

}

?>