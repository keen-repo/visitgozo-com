<?php
/**
 * The template for displaying all pages.
 * Template Name: Panoramic Webcams
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>
			
			<?php /*
			<div class="note" style="text-align: center;">
				<p><strong>We are currently improving our Live Ferry Queue Stream service.</strong></p>
				<p>Cameras 3 and 4 will be back again next week.  We’re sorry for any inconvenience caused.</p>
			</div>
			*/ ?>

			<div class="section section-webcams section-padding-bottom">
				<ul class="webcam-tabs https oflow">
					<li><span class="tab-item clickable active" data-cam="1">
						<small><?php _e( 'Camera 1', 'vg-front' ); ?></small>
						<?php _e( 'Gozo Cathedral and Victoria', 'vg-front' ); ?>
					</span></li>
					<li><span class="tab-item clickable" data-cam="2">
						<small><?php _e( 'Camera 2', 'vg-front' ); ?></small>
						<?php _e( 'Gozo Cittadella', 'vg-front' ); ?>
					</span></li>
					<li><span class="tab-item clickable" data-cam="3">
						<small><?php _e( 'Camera 3', 'vg-front' ); ?></small>
						<?php _e( 'Dwejra', 'vg-front' ); ?>
					</span></li>
					<li><span class="tab-item clickable" data-cam="4">
						<small><?php _e( 'Camera 4', 'vg-front' ); ?></small>
						<?php _e( 'Ta\' Pinu &amp; Gurdan Lighthouse', 'vg-front' ); ?>
					</span></li>
				</ul>
				<div class="webcam-embeds-https panoramic oflow">
					<div class="webcam-embed-item" id="webcam-1">
                        <!-- <iframe src="https://live.streamdays.com/hfd56wj2/iframe?t=" name="webcam-583" scrolling="no" width="800" height="370" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" frameborder="0" position="absolute" style="width: 800px; height: 370px;"></iframe> -->
					</div>
					<div class="webcam-embed-item" id="webcam-2">
                        <!-- <iframe src="https://live.streamdays.com/4j3i8pju/iframe?t=" name="webcam-585" scrolling="no" width="800" height="370" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" frameborder="0" position="absolute" style="width: 800px; height: 370px;"></iframe> -->
					</div>
					<div class="webcam-embed-item" id="webcam-3">
                        <!-- <iframe src="https://live.streamdays.com/xl36s2wo/iframe?t=" name="webcam-586" scrolling="no" width="800" height="370" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" frameborder="0" position="absolute" style="width: 800px; height: 370px;"></iframe> -->
					</div>
					<div class="webcam-embed-item" id="webcam-4">
                        <!-- <iframe src="https://live.streamdays.com/sau00r92/iframe?t=" name="webcam-586" scrolling="no" width="800" height="370" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" frameborder="0" position="absolute" style="width: 800px; height: 370px;"></iframe> -->
					</div>
				</div>
				<div id="panoramic-dummy" style="height: 0; overflow: hidden;">
					<div id="wc-1"><script src="https://live.streamdays.com/hfd56wj2" type="text/javascript" language="javascript"></script></div>
					<div id="wc-2"><script src="https://live.streamdays.com/4j3i8pju" type="text/javascript" language="javascript"></script></div>
					<div id="wc-3"><script src="https://live.streamdays.com/xl36s2wo" type="text/javascript" language="javascript"></script></div>
					<div id="wc-4"><script src="https://live.streamdays.com/sau00r92" type="text/javascript" language="javascript"></script></div>
				</div>
			</div>

			<div class="section section-disclaimer section-padding-bottom">
				<h3><?php _e( 'Disclaimer', 'vg-front' ); ?></h3>
				<small><?php echo rwmb_meta( $prefix.'page-intro-text' ); ?></small>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

