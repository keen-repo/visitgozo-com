<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

				<?php 

					if(!isset($_POST['autosubmit'])){ ?>
						<form name="autosubmit" method="post" id="frmAutoSubmit">
							<input type="hidden" name="autosubmit" />
							<input type="text" name="title" value="my auto title" />
							<input type="text" name="mainImage" value="6478" />
							<input type="text" name="otherImages[]" value="6477" />
							<input type="text" name="otherImages[]" value="6478" />
							<textarea name="description">Nulla quis lorem ut libero malesuada feugiat. Curabitur aliquet quam id dui posuere blandit. Nulla quis lorem ut libero malesuada feugiat. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Proin eget tortor risus.</textarea>
							<input type="submit" />

							<script>
								jQuery('#frmAutoSubmit').submit();
							</script>
						</form>
					<?php } else {
						echo do_shortcode( '[formidable id=5]' );

						echo FrmProEntriesController::entry_edit_link(array('id' => 4, 'label' => 'Edit', 'page_id' => 6474));
					}

				?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
