<?php
/**
 * The template for displaying all pages.
 * Template Name: Live Ferry Queue
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>
			
			<?php /*
			<div class="note" style="text-align: center;">
				<p><strong>We are currently improving our Live Ferry Queue Stream service.</strong></p>
				<p>Cameras 3 and 4 will be back again next week.  We’re sorry for any inconvenience caused.</p>
			</div>
			*/ ?>

			<div class="section section-webcams section-padding-bottom">
				<ul class="webcam-tabs https oflow">
					<li><span class="tab-item clickable active" data-cam="1">
						<small><?php _e( 'Camera 1', 'vg-front' ); ?></small>
						<?php _e( ' Mġarr Marshalling Area', 'vg-front' ); ?>
					</span></li>
					<li><span class="tab-item clickable" data-cam="2">
						<small><?php _e( 'Camera 2', 'vg-front' ); ?></small>
						<?php _e( 'Shore Street lower part', 'vg-front' ); ?>
					</span></li>
					<li><span class="tab-item clickable" data-cam="3">
						<small><?php _e( 'Camera 3', 'vg-front' ); ?></small>
						<?php _e( 'Shore Street upper part', 'vg-front' ); ?>
					</span></li>
					<li><span class="tab-item clickable" data-cam="4">
						<small><?php _e( 'Camera 4', 'vg-front' ); ?></small>
						<?php _e( 'Mġarr Road', 'vg-front' ); ?>
					</span></li>
				</ul>
				<div class="webcam-embeds-https oflow">
					<div class="webcam-embed-item" id="webcam-1">
						<iframe src="http://g0.ipcamlive.com/player/player.php?alias=5975bfa3e7f2d" width="800px" height="600px" 
frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="webcam-embed-item" id="webcam-2">
						<iframe src="http://g0.ipcamlive.com/player/player.php?alias=5979b0b2141aa" width="800px" height="600px" 
frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="webcam-embed-item" id="webcam-3">
						<iframe src="http://g0.ipcamlive.com/player/player.php?alias=598d6542f2f4d" width="800px" height="600px" 
frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="webcam-embed-item" id="webcam-4">
						<iframe src="http://g0.ipcamlive.com/player/player.php?alias=598d64ffc350e" width="800px" height="600px" 
frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<!-- <script src="https://content.jwplatform.com/libraries/qtn90nlU.js"></script> -->
			</div>

			<div class="section section-disclaimer section-padding-bottom">
				<h3><?php _e( 'Disclaimer', 'vg-front' ); ?></h3>
				<small><?php echo rwmb_meta( $prefix.'page-intro-text' ); ?></small>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

