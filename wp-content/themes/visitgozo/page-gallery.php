<?php
/**
 * The template for displaying all pages.
 *
 * Template Name: Gallery Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'gallery' ); ?>

			<?php endwhile; // End of the loop. ?>

			<div class="section section-gallery-images section-padding-bottom">

				<?php 

					$images = rwmb_meta( $prefix.'page-gallery', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );
					
					if( count($images) > 0 ){ ?>
						<div class="gallery-wrap" id="photo-gallery">
						<?php foreach( $images as $id=>$item ){
							echo wp_get_attachment_image( $id, 'full' );
						} ?>
						</div>
						<div class="gallery-thumbs" id="photo-gallery-thumbs">
						<?php foreach( $images as $id=>$item ){
							echo wp_get_attachment_image( $id, 'thumbnail' );
						} ?>
						</div>
					<?php } 
				?>

			</div>

			<div class="section section-gallery-videos section-padding-bottom">
				<div class="entry-header">
					<h2 class="entry-title"><?php _e( 'Video Gallery', 'vg-front' ); ?></h2>
				</div>

				<?php 

					$videos = rwmb_meta( $prefix.'page-video-gallery', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );
					
					if( count($videos) > 0 ){ ?>
						<div class="gallery-wrap" id="video-gallery">
						<?php foreach( $videos as $item ){ ?>
							<a data-remodal-target="lb-video" href="https://www.youtube.com/watch?v=<?php echo $item; ?>" data-video="<?php echo $item; ?>" rel="nofollow" target="_blank" title="<?php _e( 'Play Video', 'vg-front' ); ?>">
								<img src="//img.youtube.com/vi/<?php echo $item; ?>/0.jpg" alt="" />
							</a>
						<?php } ?>
						</div>
						<div class="gallery-thumbs" id="video-gallery-thumbs">
						<?php foreach( $videos as $item ){ ?>
							<img src="//img.youtube.com/vi/<?php echo $item; ?>/0.jpg" alt="" />
						<?php } ?>
						</div>
					<?php } 
				?>
				<div class="remodal video" data-remodal-id="lb-video">
					<button data-remodal-action="close" class="remodal-close"></button>
					<div id="video-container"></div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
