<?php
/**
 * Template Name: Contact Us
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>

						<?php if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php vg_posted_on(); ?>
						</div><!-- .entry-meta -->
						<?php endif; ?>
					</header><!-- .entry-header -->

					<div class="row">
						<div class="col col-40">
							<div class="entry-content">
								<?php
									the_content( sprintf(
										/* translators: %s: Name of current post. */
										wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'vg' ), array( 'span' => array( 'class' => array() ) ) ),
										the_title( '<span class="screen-reader-text">"', '"</span>', false )
									) );
								?>

								<?php
									wp_link_pages( array(
										'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vg' ),
										'after'  => '</div>',
									) );
								?>
							</div><!-- .entry-content -->
						</div>

						<div class="col col-60">
							<p class="contact-intro"><?php echo rwmb_meta( $prefix.'contact-intro-text' ); ?></p>
							<?php echo do_shortcode( '[formidable id=12]' ); ?>
						</div>
					</div>
				</article><!-- #post-## -->


				<?php endwhile; // End of the loop. ?>

				

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
