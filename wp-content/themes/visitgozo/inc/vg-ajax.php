<?php


add_action( 'wp_ajax_vg_search', 'vg_search' );
add_action( 'wp_ajax_nopriv_vg_search', 'vg_search' );

function vg_search() {

	parse_str($_GET['form'], $form_data);

	if (isset($_GET['pageSize'])) {
		$form_data['pageSize'] 		= $_GET['pageSize'];
		$form_data['pageNumber'] 	= $_GET['pageNumber'];
	}

	
	if (isset($form_data['keywords'])) {

		//echo $form_data['keywords'] . PHP_EOL;
		$form_data['keywords'] = remove_accents($form_data['keywords']);
		$form_data['keywords'] = str_replace('&', 'and', $form_data['keywords']);

		//echo $form_data['keywords'];
	}

	/*
	echo '<pre>';
	print_r($form_data);
	echo '</pre>';
	*/

	$resp = vg_front_search($form_data);
	
	echo json_encode($resp);
	exit();
    
}

add_action( 'wp_ajax_vg_count_search', 			'vg_count_search' );
add_action( 'wp_ajax_nopriv_vg_count_search', 	'vg_count_search' );

function vg_count_search() {

	parse_str($_GET['form'], $form_data);

	if (isset($form_data['keywords'])) {
		$form_data['keywords'] = remove_accents($form_data['keywords']);
		$form_data['keywords'] = str_replace('&', 'and', $form_data['keywords']);
	}

	$form_data['count_only'] = true;

	$resp = vg_front_search($form_data);
	
	echo json_encode($resp);
	exit();
    
}

/* My Gozo */
add_action( 'wp_ajax_vg_get_my_gozo_entries', 			'vg_ajax_get_my_gozo_entries' );
add_action( 'wp_ajax_nopriv_vg_get_my_gozo_entries', 	'vg_ajax_get_my_gozo_entries' );

function vg_ajax_get_my_gozo_entries() {

	$entries = vg_get_my_gozo_entries();
	
	echo json_encode($entries);
	exit();
    
}


add_action( 'wp_ajax_vg_insert_my_gozo_entry', 			'vg_ajax_insert_my_gozo_entry' );
add_action( 'wp_ajax_nopriv_vg_insert_my_gozo_entry', 	'vg_ajax_insert_my_gozo_entry' );

function vg_ajax_insert_my_gozo_entry() {

	vg_insert_my_gozo_entry($_POST['vg_post_id']);
	
	echo json_encode(array('success' => 1));
	exit();
    
}


add_action( 'wp_ajax_vg_delete_my_gozo_entry', 			'vg_ajax_delete_my_gozo_entry' );
add_action( 'wp_ajax_nopriv_vg_delete_my_gozo_entry', 	'vg_ajax_delete_my_gozo_entry' );

function vg_ajax_delete_my_gozo_entry() {

	vg_delete_my_gozo_entry($_POST['vg_post_id']);
	
	echo json_encode(array('success' => 1));
	exit();
    
}

add_action( 'wp_ajax_vg_get_my_gozo_entries_render', 		'vg_ajax_get_my_gozo_entries_render' );
add_action( 'wp_ajax_nopriv_vg_get_my_gozo_entries_render', 'vg_ajax_get_my_gozo_entries_render' );

function vg_ajax_get_my_gozo_entries_render() {

	$entries = vg_get_my_gozo_entries();

	include(locate_template('template-parts/my-gozo-listings.php'));
	
	exit();
    
}
