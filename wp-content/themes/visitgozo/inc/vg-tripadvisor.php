<?php

function vg_tripadvisor_get_single_listing($listing_ids) {

	if (!is_array($listing_ids)) $listing_ids = array($listing_ids);

	$listing_ids = implode(',', $listing_ids);


	$result = array(
		'data' 		=> array(),
		'success' 	=> false,
		'error' 	=> '',
	);

	$response = wp_remote_get('http://api.tripadvisor.com/api/partner/2.0/location/' . $listing_ids . '?key=27a9b8ec87714f7186108fea1dc1c9ba');

	if( is_array($response) ) {

		$body = json_decode($response['body']);

		if (isset($body->error)) {

			$result['error'] = 'Unable to obtain tripadvisor information.';

			//echo 'Error obtaining tripadivor data.<br />';

		} else {

			//echo 'data is good!<br />';

			$result['success'] 	= true;
			$result['data'] 	= $body;
			
		}


	} else {
		// failed response 

		$result['error'] = 'Unable to obtain tripadvisor information.';

		//echo 'response is not array!<br />';
	}

	return $result;


}