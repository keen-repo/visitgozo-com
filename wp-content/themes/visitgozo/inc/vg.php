<?php

$prefix = '_vg_';

require get_template_directory() . '/inc/vg-meta.php';
require get_template_directory() . '/inc/vg-ajax.php';
require get_template_directory() . '/inc/vg-shortcodes.php';
require get_template_directory() . '/inc/vg-listings.php';
require get_template_directory() . '/inc/vg-taxonomies.php';
require get_template_directory() . '/inc/vg-events.php';
require get_template_directory() . '/inc/vg-pool-trips.php';
require get_template_directory() . '/inc/vg-account.php';
require get_template_directory() . '/inc/vg-pending-listings.php';
require get_template_directory() . '/inc/vg-formidable.php';
require get_template_directory() . '/inc/vg-tripadvisor.php';
require get_template_directory() . '/inc/vg-settings.php';
require get_template_directory() . '/inc/vg-elastic.php';
require get_template_directory() . '/inc/vg-my-gozo.php';

function vg_get_pages(){
	$pages = get_pages();
	$pagesArr = array();

	foreach($pages as $page){
		$title = $page->post_title;

		$prefix = '';
		$ancestors = get_ancestors( $page->ID, 'page' );
		if(count($ancestors) > 0){
			foreach($ancestors as $ans)
				$prefix = '—'.$prefix;
		}

		$pagesArr[$page->ID] = $prefix.' '.$title;
	}

	return $pagesArr;
}

function vg_get_accom(){
	$accom = get_posts(
		array(
			'post_type' => 'vg_listings',
			'numberposts' => -1,
			'vg_listings_cats' => 'where-to-stay',
			'orderby' => 'title',
			'order' => 'asc'
		)
	);

	$accomArr = array();

	foreach($accom as $acc)
		$accomArr[$acc->ID] = $acc->post_title;
	

	return $accomArr;
}


function vg_build_term_tree($taxonomy, $sep = ' &raquo; ') {

	$terms = get_terms($taxonomy, array('hide_empty' => false));

    $term_id_title_list = array();

    foreach ($terms as $term) {

    	$eng_term_id = icl_object_id($term->term_id, 'vg_listings_cats', true, 'en');

      	$term_id_title_list[$eng_term_id] = $term->name;

    }

    $term_list = array();

    foreach ($terms as $term) {

        $term_children = get_term_children( $term->term_id, $taxonomy );

        if (empty($term_children)) {

          $eng_term_id = icl_object_id($term->term_id, 'vg_listings_cats', true, 'en');

          $ancestors = get_ancestors( $term->term_id, $taxonomy );

          $term_tree = array();

          $ancestors = array_reverse($ancestors);

          foreach ($ancestors as $termID) {

          	$anc_eng_term_id = icl_object_id($termID, 'vg_listings_cats', true, 'en');

            $term_tree[$anc_eng_term_id] = $term_id_title_list[$anc_eng_term_id];
          }

          $term_tree[$eng_term_id] = $term->name;

          $term_list[$eng_term_id] = implode($sep, $term_tree);

        }

    }

    asort($term_list);

    return $term_list;

}

function vg_get_seasons() {

	$seasons = array(
        'summer' => __('Summer', 'vg-front'),
        'autumn' => __('Autumn', 'vg-front'),
        'winter' => __('Winter', 'vg-front'),
        'spring' => __('Spring', 'vg-front'),
    );

    return $seasons;

}

function vg_get_accompanied() {

	$accompanied = array(
        'couple'            => __('Couple', 'vg-front'),
        'family'            => __('Family', 'vg-front'),
        'group_of_friends'  => __('Group of Friends', 'vg-front'),
        'individual'        => __('Individual', 'vg-front'),
        'senior_citizen'    => __('Senior Citizen', 'vg-front')
    );

    return $accompanied;
    
}

function vg_get_interests() {

	$interests = array(
        'art_and_culture'               => __('Art & Culture', 'vg-front'),
        'beaches'                       => __('Beaches', 'vg-front'),
        'conferences_and_incentives'    => __('Conferences & Incentives', 'vg-front'),
        'dining_out'                    => __('Dining Out', 'vg-front'),
        'diving'                        => __('Diving', 'vg-front'),
        'history_and_archaeology'       => __('History & Archaeology', 'vg-front'),
        'learning'                      => __('Learning', 'vg-front'),
        'nature_and_countryside'        => __('Nature & Countryside', 'vg-front'),
        'nightlife_and_entertainment'   => __('Nightlife & Entertainment', 'vg-front'),
        'relaxation'                    => __('Relaxation', 'vg-front'),
        'religious'                     => __('Religious', 'vg-front'),
        'rural_tourism'                 => __('Rural Tourism', 'vg-front'),
        'shopping'                      => __('Shopping', 'vg-front'),
        'sports_and_adventure'          => __('Sports & Adventure', 'vg-front'),
        'tours'                         => __('Tours', 'vg-front'),
        'weddings_and_honeymoons'       => __('Weddings & Honeymoons', 'vg-front'),
        'wellness_services'             => __('Wellness Services', 'vg-front')
    );

    return $interests;
    
}

function vg_render_upcoming_events( $count=-1 ){

	global $prefix, $wpdb;

	$current_timestamp = current_time('timestamp');

	$current_time = mktime(0,0,0, date('n', $current_timestamp), date('j', $current_timestamp), date('Y', $current_timestamp));

	$single_dates_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 0 AND `date` > %d GROUP BY post_id ORDER BY `date` ASC LIMIT 30', $current_time);

	$date_ranges_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 1 AND end_date_range > %d GROUP BY post_id ORDER BY start_date_range ASC LIMIT 30', $current_time);

	$single_dates_results = $wpdb->get_results($single_dates_sql);

	$date_ranges_results = $wpdb->get_results($date_ranges_sql);

	$time_list 				= array();
	$exhibition_time_list 	= array();

	$exhibition_category_id = 149;

	foreach ($single_dates_results as $k=>$row) {

		$categories = wp_get_object_terms($row->post_id, 'vg_events_cats', array(
			'fields' => 'ids',
		));

		if (get_post_status($row->post_id) == 'publish') {
			if (in_array($exhibition_category_id, $categories)) {
				$exhibition_time_list[$k . '-single_dates'] = $row->date;
			} else {
				$time_list[$k . '-single_dates'] = $row->date;
			}
		}
	}

	foreach ($date_ranges_results as $k=>$row) {

		$categories = wp_get_object_terms($row->post_id, 'vg_events_cats', array(
			'fields' => 'ids',
		));

		if (get_post_status($row->post_id) == 'publish') {
			if (in_array($exhibition_category_id, $categories)) {
				$exhibition_time_list[$k . '-date_range'] = $row->start_date_range;
			} else {
				$time_list[$k . '-date_range'] = $row->start_date_range;
			}
		}

	}

	asort($time_list, SORT_NUMERIC);

	$time_list 				= array_slice($time_list, 			 0, 9, true); 
	$exhibition_time_list 	= array_slice($exhibition_time_list, 0, 9, true); 

	$event_list = array(
		'upcoming_events' => array(
			'tab_label' => __('Upcoming Events', 'vg-front'),
			'class' 	=> 'active',
			'events' 	=> $time_list,
		),
		'upcoming_exhibition' => array(
			'tab_label' => __('Upcoming Exhibitions', 'vg-front'),
			'class' 	=> '',
			'events'	=> $exhibition_time_list
		)
	);

	$total_tabs = 1;

	if (!empty($time_list))
		$total_tabs++;

	if (!empty($exhibition_time_list))
		$total_tabs++;

 ?>

	<div class="section section-upcoming-events section-padding-top-bottom">
		<?php if ($total_tabs === 1) { ?>
			<h2 class="section-title"><?php _e( 'Upcoming Events', 'vg-front' ); ?></h2>
		<?php } else { ?>
			<div class="event-item-wrap txt-aligncenter">
				<?php 

				foreach ($event_list as $event_key=>$event_tab_data) { 

					if (empty($event_tab_data['events'])) continue;

				?>
				<span class="nav-item <?php echo $event_tab_data['class']; ?>" data-event-key="<?php echo $event_key; ?>"><?php echo $event_tab_data['tab_label']; ?></span>
				<?php } ?>
				<a class="cultural-calendar" href="<?php echo get_permalink(icl_object_id(74, 'page', false, 'en')); ?>"><?php _e('Cultural Calendar', 'vg-front'); ?></a>
			</div>
		<?php } ?>

		<?php
		foreach ($event_list as $event_key=>$event_tab_data) {

			$style = (empty($event_tab_data['class'])) ? 'style="display: none;"' : '';
		?>
			<div class="item-boxes events oflow" data-event-type="<?php echo $event_key; ?>" <?php echo $style; ?>>
				<?php
				foreach ($event_tab_data['events'] as $index=>$start_time) {

					list($index, $type) = explode('-', $index);

					if ($type == 'single_dates') {

						$row = $single_dates_results[$index];

						$event_day = date('d', $row->date);
						$event_mon = date('M', $row->date);
					}

					if ($type == 'date_range') {

						$row = $date_ranges_results[$index];
						
						$event_day = date('d');
						$event_mon = date('M');

						if ($row->start_date_range > $current_time) {
							$event_day = date('d', $row->start_date_range);
							$event_mon = date('M', $row->start_date_range);
						}
					}

					$event = get_post($row->post_id);

					$poster = rwmb_meta( $prefix.'event-poster', array(), $event->ID );
					$poster = current($poster);
					$posterImage = wp_get_attachment_image_src( $poster['ID'], 'medium-uncropped' ); 

					?>
					<a class="item-box" href="<?php echo get_permalink( $event->ID ); ?>">
						<div class="item-image fullwidth-fullheight bg-cover" style="background-image: url('<?php echo $posterImage[0]; ?>'); ?>"></div>
						<div class="item-date">
							<span class="event-day"><?php echo $event_day; ?></span>
							<span class="event-mon"><?php echo $event_mon; ?></span>
						</div>
						<div class="item-title"><?php echo $event->post_title; ?></div>
					</a>
				<?php } ?>
			</div>
		<?php } ?>
		<div class="view-more">
			<a href="<?php echo get_permalink(icl_object_id( 96, 'page', false )); ?>"><?php _e( 'View More', 'vg-front' ); ?></a>
		</div>
	</div>

<?php }

function vg_render_newsletter_form(){
	$form =  '<!-- Begin MailChimp Signup Form -->
    <div id="mc_embed_signup" class="footer-newsletter-form">
		<form action="https://visitgozo.us11.list-manage.com/subscribe/post?u=9ae3f285d8227331afd3a6e53&amp;id=dc132a4e88" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll" class="mc_embed_signup_scroll_container">

                <div class="mc-field-group">
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email*" type="email">
                </div>
                <div class="mc-field-group">
                    <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                </div>
                <div class="mc-field-group">
                    <input type="text" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Surname">
                </div>

                <div id="mce-responses" class="clear" style="display:none;">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
 <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_46dac67f5c86bbad29504ca04_9e0e6ddfa6" tabindex="-1" value=""></div>';

    if(is_page(21518)){
    	$form .= '<div class="note">
			<input type="checkbox" name="terms" value="terms" id="terms-newsletter">
				I have read and understood the <a href="'.get_permalink(icl_object_id(7215, 'page', false, 'en')).'" target="_blank">Terms & Conditions</a>.
			</div><div class="clear subscribe-button-container">
				<input value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button clickable" type="button"></div>
            </div>
        </form>
    </div>
    <!--End mc_embed_signup-->';
    }
	else {
		$form .='<div class="clear subscribe-button-container">
					<div class="note" id="note-responsive" style="display:none">
						<input type="checkbox" name="terms" value="terms" id="terms-newsletter">
							I have read and understood the <a href="'.get_permalink(icl_object_id(7215, 'page', false, 'en')).'" target="_blank">Terms & Conditions</a>.
					</div>
				<input value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button clickable" type="button"></div>
				</div>
			<div id="note-not-responsive" class="note" style="">
				<input type="checkbox" name="terms" value="terms" id="terms-newsletter">
					I have read and understood the <a href="'.get_permalink(icl_object_id(7215, 'page', false, 'en')).'" target="_blank">Terms & Conditions</a>.
			</div>
        </form>
    </div>
    <!--End mc_embed_signup-->';
	}


    return $form;
}

function vg_render_also_check_out( $pageID ){

	global $prefix;

	$hideCheckout = rwmb_meta( $prefix . 'hide_checkout', array(), $pageID );

	if( !$hideCheckout ){ 

		$settings = get_option( 'vg_settings' );

		if( isset( $settings['check-out-subpages'] ) ){

			shuffle( $settings['check-out-subpages'] ); ?>

			<div class="section section-check-out section-padding-bottom wrap">
				<h3 class="section-title"><?php _e( 'Also check out', 'vg-front' ); ?></h3>
				<div class="checkout-slider slides-wrap">
				<?php foreach( $settings['check-out-subpages'] as $coPageID ){
					if( $coPageID == $pageID )
						continue;

					$featuredID = get_post_thumbnail_id( $coPageID );
					if( !empty($featuredID) ){
						$featuredArr = wp_get_attachment_image_src( $featuredID, 'subpage-featured', true );
						$featuredImage = $featuredArr[0];
					} else {
						$featuredImage = '';
					} ?>
					<a class="slide oflow relative" href="<?php echo the_permalink(icl_object_id( $coPageID, 'page', false )); ?>">
						<div class="slide-image bg-cover fullwidth-fullheight" style="background-image: url('<?php echo $featuredImage; ?>'); ?>"></div>
						<div class="slide-title valign-middle"><?php echo get_the_title( icl_object_id( $coPageID, 'page', false ) ); ?></div>
					</a>
				<?php } ?>
				</div>
			</div>

			<?php 
				// do not show if on newsletter subscribe page
				if(!is_page(21518)){ ?>
					<div class="section section-newsletter section-padding-bottom wrap txt-aligncenter">
						<h3 class="section-title"><?php _e( 'Newsletter', 'vg_front' ); ?></h3>
						<?php echo vg_render_newsletter_form(); ?>
					</div>
				<?php }
			?>
		<?php }

	}
}

function vg_render_page_gallery(){
	global $prefix;

	$videoGallery = rwmb_meta( $prefix . 'page-video-gallery', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );

	$pageGallery = rwmb_meta( $prefix . 'page-gallery', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );

	if( count($pageGallery) > 0 || count($videoGallery) > 0){ ?>
		<div class="section section-page-gallery section-padding-top-bottom wrap">
			<div class="page-gallery-slider">
			<?php foreach( $videoGallery as $item ){ ?>
				<div class="slide oflow relative video">
					<a data-remodal-target="lb-video" href="https://www.youtube.com/watch?v=<?php echo $item; ?>" data-video="<?php echo $item; ?>" rel="nofollow" target="_blank" title="<?php _e( 'Play Video', 'vg-front' ); ?>">
						<img src="//img.youtube.com/vi/<?php echo $item; ?>/0.jpg" alt="<?php get_the_title( get_the_ID() ); ?> video" />
					</a>
				</div>
			<?php } ?>
			<?php foreach( $pageGallery as $item ){ ?>
				<div class="slide oflow relative">
					<?php echo wp_get_attachment_image( $item['ID'], 'subpage-featured' ); ?>
				</div>
			<?php } ?>
			</div>
		</div>
		<div class="remodal video" data-remodal-id="lb-video">
			<button data-remodal-action="close" class="remodal-close"></button>
			<div id="video-container"></div>
		</div>
	<?php }
}

function vg_render_listing_gallery(){
	global $prefix, $post;

	if (is_page()) {
		$meta_key = 'page-gallery';
	} else {
		$meta_key = 'listing-gallery';
	}

	$pageGallery = rwmb_meta( $prefix . 'page-gallery', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );

	if(empty($pageGallery) || count($pageGallery) < 3){
		$pageGallery = rwmb_meta( $prefix . 'listing-gallery', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );
	}

	if( count($pageGallery) >= 3 ) { ?>
		<div class="section section-page-gallery section-padding-top-bottom wrap">
			<div class="page-gallery-slider">
			<?php foreach( $pageGallery as $item ){ ?>
				<div class="slide oflow relative">
					<?php echo wp_get_attachment_image( $item['ID'], 'subpage-featured' ); ?>
				</div>
			<?php } ?>
			</div>
		</div>
	<?php }
}

function vg_render_event_gallery(){
	global $prefix;

	$pageGallery = rwmb_meta( $prefix . 'event-gallery', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );

	if( count($pageGallery) >= 3 ){ ?>
		<div class="section section-page-gallery section-padding-top-bottom wrap">
			<div class="page-gallery-slider">
			<?php foreach( $pageGallery as $item ){ ?>
				<div class="slide oflow relative">
					<?php echo wp_get_attachment_image( $item['ID'], 'subpage-featured' ); ?>
				</div>
			<?php } ?>
			</div>
		</div>
	<?php }
}

function vg_render_maps($title, $lat, $lng, $width='100%', $height='560px'){ ?>
	<div id="map" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>"></div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
	<script>

		var planes = [
			["<?php echo htmlentities($title); ?>",<?php echo $lat; ?>,<?php echo $lng; ?>]
		];

		var map = L.map('map').setView([<?php echo $lat; ?>, <?php echo $lng; ?>], 17);
		mapLink = 
			'<a href="http://openstreetmap.org">OpenStreetMap</a>';
		L.tileLayer(
			'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; ' + mapLink + ' Contributors',
			maxZoom: 18,
			}).addTo(map);

		for (var i = 0; i < planes.length; i++) {
			marker = new L.marker([planes[i][1],planes[i][2]])
				.addTo(map);
		}
			
	</script>
<?php }

function vg_render_event_date(){
	global $post, $prefix;


	$occurance = get_post_meta(get_the_ID(), $prefix . 'event-occurance', true);

	if ($occurance == 'specific_days') {

		$dates = rwmb_meta( $prefix.'event-dates' );

		if(count($dates) > 0){ ?>
			<ul class="event-dates">
			<?php foreach( $dates as $ts ){
				$timeString = str_replace( '/', ' ', $ts['date']['formatted']);
				$time = strtotime( $timeString );

				$start_time = strtotime($ts['start-time']['formatted']);
				$end_time = strtotime( $ts['ending-time']['formatted'] ); 

				$time_str = '';

				if (!empty($ts['start-time']['formatted']) && $ts['start-time']['formatted'] != '00:00') {
					$time_str = __( 'at', 'vg_front' ) . '  ' . date( 'g:ia', $start_time );

					if (!empty($ts['ending-time']['formatted']) && $ts['ending-time']['formatted'] != '00:00') {
						$time_str .= ' ' . __( 'to', 'vg_front' ) . '  ' . date( 'g:ia', $end_time );
					}
				}

				?>
				<li class="icon-calendar-empty">
					<?php echo date( 'D, jS M Y', $time ) . ' ' . $time_str; ?>
				</li>
			<?php } ?>
			</ul>
		<?php }

	}

	if ($occurance == 'date_range') {

		$start_date = get_post_meta(get_the_ID(), 	$prefix . 'event-start-date', 	true);
		$end_date 	= get_post_meta(get_the_ID(), 	$prefix . 'event-end-date', 	true);
		$start_time = strtotime(get_post_meta(get_the_ID(), 	$prefix . 'event-start-time', 	true));
		$end_time 	= strtotime(get_post_meta(get_the_ID(), 	$prefix . 'event-end-time', 	true));

		$time_str = '';

		if (!empty($start_time) && $start_time != '00:00') {
			$time_str = date( 'g:ia', $start_time );

			if (!empty($end_time) && $end_time != '00:00') {
				$time_str .= ' ' . __( 'to', 'vg_front' ) . '  ' . date( 'g:ia', $end_time ) . ' ' . __( 'everyday', 'vg_front' );
			}
		}

		?>
		<ul class="event-dates">
			<li class="icon-calendar-empty">
				<?php echo date( 'D, jS M Y', $start_date ); ?> <?php _e( 'to', 'vg_front' ); ?> <?php echo date( 'D, jS M Y', $end_date ); ?> 
			</li>
			<?php if (!empty($time_str)) { ?>
				<li class="icon-clock"> 
					<?php echo $time_str; ?>
				</li>
			<?php } ?>
		</ul>
		<?php


	}

	

}

function vg_wpml_get_code( $lang = "" ) {
 
    $langs = icl_get_languages( 'skip_missing=0' );
    if( isset( $langs[$lang]['default_locale'] ) ) {
        return $langs[$lang]['default_locale'];
    }
 
    return false;
}

/**
 * Hide editor for specific pages.
 *
 */


add_action( 'admin_init', 'vg_hide_editor' );

function vg_hide_editor() {

	// Get the Post ID.
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$post_type = $_GET['post_type'];

	if( !isset( $post_id ) ) return;

	// Get the name of the Page Template file.
	//$template_file = get_post_meta($post_id, '_wp_page_template', true);

    $onPages = array(4);

    if(in_array($post_id,$onPages)) { 
    	remove_post_type_support('page', 'editor');
    }

}

/*
 * We look for old users possibly 
*/
add_action( 'init', 'vg_apply_redirects', 99 );

function vg_apply_redirects() {

	$url = $_SERVER['REQUEST_URI'];

	/* remove blank white space */
	$url = str_replace('%E2%80%8B', "", $url);

	if (strpos($url, '/item/') !== false) {

		$parts = explode('-', $url);

		$internal_id = array_pop($parts);

		list($actual_internal_id, $rest) = explode('/', $internal_id);

		//echo 'actual internal ID: ' . $actual_internal_id;

		$matchingPosts = get_posts(array(
			'post_type' => 'vg_listings',
			'meta_query' => array(
				array(
					'key' => '_imported_listing_id',
					'value' => $actual_internal_id,
				)
			)
		));

		/*
		echo 'matching posts:<pre>';
		print_r($matchingPosts);
		echo '</pre>';
		*/
		

		if (!empty($matchingPosts)) {

			$languages = array('en', 'fr', 'de', 'it', 'ru', 'es', 'sv');

			$current_lang = 'en';

			foreach ($languages as $lang) {

				if (strpos($url, '/' . $lang . '/') !== false) {
					$current_lang = $lang;
					break;
				}

			}

			//echo $current_lang . '<br />';

			$en_permalink = str_replace('/' . ICL_LANGUAGE_CODE . '/', '/', get_permalink($matchingPosts[0]->ID));

			//echo $en_permalink . '<br />';

			$final_permalink = apply_filters( 'wpml_permalink', $en_permalink, $current_lang );

			//echo $final_permalink . '<br />';

			wp_redirect($final_permalink, 301);
			exit();

		}

		

	} 


}


// Hide Instagram Captions

function custom_instagram_settings($code){
    if(strpos($code, 'instagr.am') !== false || strpos($code, 'instagram.com') !== false){ // if instagram embed
        $return = preg_replace("@data-instgrm-captioned@", "", $code); // remove caption class
        return $return;     
    }
return $code;
}

add_filter('embed_handler_html', 'custom_instagram_settings');
add_filter('embed_oembed_html', 'custom_instagram_settings');


/**
 * Modify Main Query
 *
 */

/*
add_action( 'pre_get_posts', 'vg_modify_main_query' );

function vg_modify_main_query( $query ) {

 	if ( is_post_type_archive( 'post_type_id' ) && $query->is_main_query() && !is_admin() ) { 
		
		$query->query_vars['order'] = 'ASC';
		$query->query_vars['orderby'] = 'menu_order';

 	}

}
*/

/**
 * Override default user dropdown filter in edit listings
 */
function filter_wp_dropdown_users_args( $query_args, $parsed_args ) {

	$query_args['who'] = '';

	return $query_args;
}

if( is_admin() && $_GET['action'] == 'edit' ) {
	add_filter( 'wp_dropdown_users_args', 'filter_wp_dropdown_users_args', 10, 2 );
}

?>