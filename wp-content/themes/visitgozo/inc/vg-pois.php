<?php

// Register POIS Custom Post Type
function vg_pois() {

	$labels = array(
		'name'                  => _x( 'POIS', 'Post Type General Name', 'vg' ),
		'singular_name'         => _x( 'POI', 'Post Type Singular Name', 'vg' ),
		'menu_name'             => __( 'POIS', 'vg' ),
		'name_admin_bar'        => __( 'POIS', 'vg' ),
		'archives'              => __( 'POI Archives', 'vg' ),
		'parent_item_colon'     => __( 'Parent POI:', 'vg' ),
		'all_items'             => __( 'All POIS', 'vg' ),
		'add_new_item'          => __( 'Add New POI', 'vg' ),
		'add_new'               => __( 'Add New', 'vg' ),
		'new_item'              => __( 'New POI', 'vg' ),
		'edit_item'             => __( 'Edit POI', 'vg' ),
		'update_item'           => __( 'Update POI', 'vg' ),
		'view_item'             => __( 'View POI', 'vg' ),
		'search_items'          => __( 'Search POI', 'vg' ),
		'not_found'             => __( 'Not found', 'vg' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'vg' ),
		'featured_image'        => __( 'Featured Image', 'vg' ),
		'set_featured_image'    => __( 'Set featured image', 'vg' ),
		'remove_featured_image' => __( 'Remove featured image', 'vg' ),
		'use_featured_image'    => __( 'Use as featured image', 'vg' ),
		'insert_into_item'      => __( 'Insert into POI', 'vg' ),
		'uploaded_to_this_item' => __( 'Uploaded to this POI', 'vg' ),
		'items_list'            => __( 'POIS list', 'vg' ),
		'items_list_navigation' => __( 'POIS list navigation', 'vg' ),
		'filter_items_list'     => __( 'Filter items list', 'vg' ),
	);
	$rewrite = array(
		'slug'                  => 'pois',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'POIS', 'vg' ),
		'description'           => __( 'visitgozo.com pois', 'vg' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'vg_listings_cats' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-list-view',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'vg_pois', $args );

}
add_action( 'init', 'vg_pois', 0 );