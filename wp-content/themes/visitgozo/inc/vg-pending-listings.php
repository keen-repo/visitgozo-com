<?php

/* Listing Pending Updates Page */
add_action('admin_menu', 'vg_pending_listing_updates_menu');
 
function vg_pending_listing_updates_menu() {

	global $wpdb;

	$sql = $wpdb->prepare('SELECT COUNT(*) as total FROM vg_listing_relationships WHERE status=%d', 1);

	$resp = $wpdb->get_row($sql);

   add_menu_page(
		'Pending Updates',
		'Pending Updates (' . $resp->total . ')',
		'edit_posts',
		'listing-pending-updates',
		'vg_pending_listing_updates_page',
		'',
		6
    );

}


function vg_pending_listing_updates_page() {

	global $wpdb;

	$sql = $wpdb->prepare('SELECT * FROM vg_listing_relationships WHERE status=%d', 1);

	$rows = $wpdb->get_results($sql);

	?>
	<div class="wrap">
		<h2>Pending Updates</h2>

		<?php 

		if (!empty($rows)) {

		?>
		<table class="wp-list-table widefat fixed striped posts" cellspacing="0">
		    <thead>
		    <tr>
	            <th class="manage-column" scope="col">User</th>
	            <th class="manage-column" scope="col">Type</th>
	            <th class="manage-column" scope="col">Form Entry</th>
	            <th class="manage-column" scope="col">Listing</th>
	            <th class="manage-column" scope="col"></th>
		    </tr>
		    </thead>
		    <tfoot>
		    <tr>
	            <th class="manage-column" scope="col">User</th>
	            <th class="manage-column" scope="col">Type</th>
	            <th class="manage-column" scope="col">Form Entry</th>
	            <th class="manage-column" scope="col">Listing</th>
	            <th class="manage-column" scope="col"></th>
		    </tr>
		    </tfoot>
		    <tbody>
		    	<?php 

		    	foreach ($rows as $row) {

		    		$user_edit_url = get_edit_user_link($row->user_id);

		    		$user_data = get_userdata($row->user_id);

		    		$listing_edit_url = '';

		    		if ($row->listing_id) {

		    			$listing_edit_url = get_edit_post_link($row->listing_id);

		    		}

		    		$update_url = admin_url('admin.php?page=formidable-entries&frm_action=edit&id=' . $row->form_entry_id);

		    		$entry = FrmEntry::getOne($row->form_entry_id, true);

		    		if ($row->type == 'event') 
		    			$meta_field_id = 98;
		    		else if ($row->type == 'listing') 
		    			$meta_field_id = 75;
		    	?>
		        <tr>
		            <td class="title column-title has-row-actions column-primary page-title">
		            	<strong><a href="<?php echo $user_edit_url; ?>"><?php echo $user_data->display_name; ?></a></strong>
		            </td>
		            <td><?php echo $row->type; ?></td>
		            <td><?php echo $entry->metas[$meta_field_id]; ?></td>
		            <td>
		            	<?php if ($listing_edit_url) { ?>
		            		<a href="<?php echo $listing_edit_url; ?>"><?php echo get_the_title($row->listing_id); ?></a>
		            	<?php } else { ?>
		            		No Connected Listing.
		            	<?php } ?>
		            </td>
		            <td>
		            	<a href="<?php echo $update_url; ?>">View Updates</a>
		            </td>
		        </tr>
		        <?php } ?>
		    </tbody>
		</table>

		<?php } else { ?>

			<strong>No Pending Updates</strong>

		<?php }  ?>

	</div>
	<?php
}



add_action('frm_edit_entry_sidebar', 'vg_frm_show_entry_sidebar');

function vg_frm_show_entry_sidebar($record) {

	global $wpdb;

	$sql = $wpdb->prepare('SELECT * FROM vg_listing_relationships WHERE form_entry_id=%d AND status=%d', $record->id, 1);

	$pending_item = $wpdb->get_row($sql);

	if ($pending_item) {
	?>
	<div class="postbox review-entry-wrapper">
	    <h3 class="hndle"><span><?php _e( 'Approval Options', 'vg' ) ?></span></h3>
	    <div class="inside review-entry-actions">
	        <p>Changes have been made to the form. Please review all changes and then either approve or decline the change.</p>

	        <div class="actions">

	        	<a href="#" data-task="approve" class="button button-primary">Approve</a>

	        	<a href="#" data-task="decline" class="button">Decline</a>

	       	</div>

	       	<div class="message"></div>

	        <input type="hidden" id="listing_relationship_id" value="<?php echo $pending_item->id; ?>" />
	    </div>
	</div>
	<script type="text/javascript">

		jQuery(document).ready(function() {

			jQuery('.review-entry-actions a.button').click(function(e) {

				e.preventDefault();

				var task = jQuery(this).data('task');

				var row_id = jQuery('#listing_relationship_id').val();

				jQuery('.review-entry-wrapper .message').html('Performing action...');
				jQuery('.review-entry-wrapper .actions').hide();

				jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', { 
					action : 'vg_review_entry_task',
					task 	: task,
					row_id 	: row_id
				}, function(d) {

					if (d.success) {

						jQuery('.review-entry-wrapper .message').html('<strong>Successfully performed action:' + task + '</strong>');

					} else {
						jQuery('.review-entry-wrapper .message').html('<strong>There was an error performing :' + task + '</strong>');
						jQuery('.review-entry-wrapper .actions').show();
					}
				
				}, 'json');

			});

		});

	</script>
	<?php
	}

}

add_action( 'wp_ajax_vg_review_entry_task', 'vg_review_entry_task' );

function vg_review_entry_task() {

	global $wpdb, $prefix;

	$task = $_POST['task'];
	$row_id = $_POST['row_id'];

	$sql = $wpdb->prepare('SELECT * FROM vg_listing_relationships WHERE id=%d', $row_id);

	$item = $wpdb->get_row($sql);

	$result = array('success' => false);

	if (!empty($item)) {

		if ($task == 'decline') {

			/*
			echo '<pre>';
			print_r($item);
			echo '</pre>';
			*/

			$user = get_user_by('id', $item->user_id);

			if ($user) {

				$email = $user->get('user_email');

				if ($email) {

					//echo 'send email!<br />';

					//$email = 'developer@keen-advertising.com';

					if ($item->type == 'listing') {
						$subject = 'VisitGozo.com - Pending Listing has been declined.';
					}

					if ($item->type == 'event') {
						$subject = 'VisitGozo.com - Pending Event has been declined.';
					}

					$message = array();
					$message[] = 'Your recent pending ' . $item->type . ' has been declined by a VisitGozo.com administrator. If you feel this is a mistake please contact VisitGozo support at visitgozo@gov.mt.';

					wp_mail($email, $subject, implode('<br />', $message));

				}

			}

			$wpdb->delete( 
				'vg_listing_relationships', 
				array( 
					'id' => $row_id 
				), 
				array( 
					'%d' 
				)
			);

			$result = array(
				'success' => true
			);

		} // end task decline


		if ($task == 'approve') {

			if ($item->type == 'listing') {
				$result = vg_approve_listing($item, $row_id);
			}

			if ($item->type == 'event') {
				$result = vg_approve_event($item, $row_id);
			}

			

		} // end task approve

	}


	echo json_encode($result);
	exit();

}

function vg_approve_event($item, $row_id) {

	global $wpdb, $prefix;

	$result = array();

	$entry = FrmEntry::getOne($item->form_entry_id, true);

	$postData = array(
	  'post_title'    => $entry->metas[98],
	  'post_content'  => $entry->metas[109],
	  'post_status'   => 'publish',
	  'post_author'   => $entry->user_id,
	  'post_type' 	  => 'vg_events',
	  'post_status'   => 'publish',
	);

	if ($item->listing_id) {

		$postData['ID'] = $item->listing_id;

		$post_id = wp_update_post($postData);

	} else {

		$post_id = wp_insert_post( $postData );

	}

	if (is_wp_error($post_id)) {

	} else {

		/* venue */
		update_post_meta($post_id, $prefix . 'event-venue',			$entry->metas[120]);
		/* contact no */
		update_post_meta($post_id, $prefix . 'event-contact-no',	$entry->metas[158]);
		/* email */
		update_post_meta($post_id, $prefix . 'event-email',			$entry->metas[105]);
		/* website */
		update_post_meta($post_id, $prefix . 'event-website',		$entry->metas[106]);
		/* video */
		update_post_meta($post_id, $prefix . 'event-video',			$entry->metas[107]);

		/* set featured image */
		update_post_meta($post_id, $prefix . 'event-poster', 		$entry->metas[114]);

		/* set gallery */
		delete_post_meta($post_id, $prefix . 'event-gallery');

		foreach ($entry->metas[115] as $aid) {
			add_post_meta($post_id, $prefix . 'event-gallery', $aid);
		}

		/* cordinates */
		update_post_meta($post_id, $prefix . 'event-latitude', 		$entry->metas[118]);
		update_post_meta($post_id, $prefix . 'event-longitude', 	$entry->metas[119]);

		/* Event Occurance */


		/* Event Dates */
		update_post_meta($post_id, $prefix . 'event-occurance', $entry->metas[151]);

		$wpdb->delete( 'vg_event_dates', array( 'post_id' => $post_id ), array( '%d' ) );

		if ($entry->metas[151] == 'specific_days') {

			$event_dates = array();

			foreach ($entry->metas[111] as $entryID) {

				$subEntry = FrmEntry::getOne($entryID, true);

				$timestamp = strtotime($subEntry->metas[121]);

				if ($timestamp < current_time('timestamp')) continue;

				$event_dates[] = array(
					'date' 		 => array(
						'timestamp' => $timestamp,
						'formatted' => date('d/M/Y', $timestamp),
					),
					'start-time' => array(
						'formatted' => $subEntry->metas[122]
					),
					'ending-time' => array(
						'formatted' => $subEntry->metas[150]
					),
				);

				$wpdb->insert( 
				    'vg_event_dates', 
				    array( 
				      'post_id' 	=> $post_id,
				      'date'     	=> $timestamp,
				      'start_time'  => $subEntry->metas[122],
				      'end_time'    => $subEntry->metas[150],
				      'range'		=> 0,
				    ), 
				    array( 
				      '%d',
				      '%d',
				      '%s',
				      '%s',
				      '%d'
				    )
				);
				
			}

			update_post_meta($post_id, $prefix . 'event-dates', $event_dates);

		}

		/* Event Date Range */

		if ($entry->metas[151] == 'date_range') {

	        update_post_meta($post_id, $prefix . 'event-start-date', 	strtotime($entry->metas[154]));
			update_post_meta($post_id, $prefix . 'event-end-date', 		strtotime($entry->metas[155]));
			update_post_meta($post_id, $prefix . 'event-start-time', 	$entry->metas[156]);
			update_post_meta($post_id, $prefix . 'event-end-time', 		$entry->metas[157]);

			$wpdb->insert( 
			    'vg_event_dates', 
			    array( 
			      'post_id' 			=> $post_id,
			      'range'				=> 1,
			      'start_date_range'  	=> strtotime($entry->metas[154]),
			      'end_date_range'    	=> strtotime($entry->metas[155]),
			      'start_time_range'    => $entry->metas[156],
			      'end_time_range' 		=> $entry->metas[157]
			      
			    ), 
			    array( 
			      '%d',
			      '%d',
			      '%d',
			      '%d',
			      '%s',
			      '%s'
			    )
			);

		}


		/* category */
		wp_set_object_terms($post_id, array((int)$entry->metas[100]), 'vg_events_cats');

		/* locality */
		wp_set_object_terms($post_id, array((int)$entry->metas[103]), 'vg_locality_cats');


		
		$wpdb->update( 
			'vg_listing_relationships', 
			array( 
				'listing_id' 	=> $post_id,
				'status' 		=> 0,
			), 
			array( 'id' => $row_id ), 
			array( 
				'%d',
				'%d'
			), 
			array( '%d' ) 
		);

		$post = get_post($post_id);

		$response = vg_elastic_index_event($post_id, $post);

		if ($response['success'] === true) {

			$result = array(
				'success' => true,
				'vg_elastic_index_listing_response' => $response
			);

		} else {

			$result = array(
				'success' => false, 
				'vg_elastic_index_listing_response' => $response
			);
			
		}


	}

	return $result;

}

function vg_approve_listing($item, $row_id) {

	global $wpdb, $prefix;

	$result = array();

	$entry = FrmEntry::getOne($item->form_entry_id, true);

	$postData = array(
	  'post_title'    => $entry->metas[75],
	  'post_content'  => $entry->metas[86],
	  'post_status'   => 'publish',
	  'post_author'   => $entry->user_id,
	  'post_type' 	  => 'vg_listings',
	  'post_status'   => 'publish',
	);

	if ($item->listing_id) {

		$postData['ID'] = $item->listing_id;

		$post_id = wp_update_post($postData);

	} else {

		$post_id = wp_insert_post( $postData );

	}

	if (is_wp_error($post_id)) {

	} else {

		/* tripadvisor url */
		update_post_meta($post_id, $prefix . 'listing-tripadvisor-url', $entry->metas[76]);

		/* tripadvisor ID */
		preg_match_all("/d([0-9]*)-/", $entry->metas[76], $out, PREG_PATTERN_ORDER);

		$listingID = '';

		if (!empty($out) && isset($out[1][0])) {

			$listingID = $out[1][0];

		}

		update_post_meta($post_id, $prefix . 'listing-tripadvisor-id', $listingID);

		/* address */
		update_post_meta($post_id, $prefix . 'listing-address-1', $entry->metas[78]);
		update_post_meta($post_id, $prefix . 'listing-address-2', $entry->metas[79]);

		/* contact info */
		update_post_meta($post_id, $prefix . 'listing-contact-no', 	$entry->metas[81]);
		update_post_meta($post_id, $prefix . 'listing-email', 		$entry->metas[82]);
		update_post_meta($post_id, $prefix . 'listing-website', 	$entry->metas[83]);

		/* video url */
		update_post_meta($post_id, $prefix . 'listing-video', 		$entry->metas[84]);

		/* remarks */
		update_post_meta($post_id, $prefix . 'listing-remarks', 	$entry->metas[85]);

		/* set featured image */
		set_post_thumbnail($post_id, $entry->metas[91]);

		/* set gallery */
		delete_post_meta($post_id, $prefix . 'listing-gallery');

		if (isset($entry->metas[92])) {

			foreach ($entry->metas[92] as $aid) {
				add_post_meta($post_id, $prefix . 'listing-gallery', $aid);
			}

		}

		/* cordinates */
		update_post_meta($post_id, $prefix . 'listing-latitude', 	$entry->metas[95]);
		update_post_meta($post_id, $prefix . 'listing-longitude', 	$entry->metas[96]);

		/* Do Opening Hours */
		$opening_hours = array();

		if (isset($entry->metas[88])) {

			foreach ($entry->metas[88] as $entryID) {

				$subEntry = FrmEntry::getOne($entryID, true);

				$opening_hours[] = array(
					'opening-day' 	=> $subEntry->metas[70],
					'opening-hour' 	=> $subEntry->metas[71],
					'closing-hour' 	=> $subEntry->metas[72],
				);
				
			}

		}

		update_post_meta($post_id, $prefix . 'opening-hours', $opening_hours);

		/* category */
		$catID = (int)$entry->metas[77];

		$ancestors = get_ancestors($catID, 'vg_listings_cats' );
		$ancestors[] = $catID;

		wp_set_object_terms($post_id, $ancestors, 'vg_listings_cats');

		/* locality */
		wp_set_object_terms($post_id, array((int)$entry->metas[80]), 'vg_locality_cats');

		/* Language */
		update_post_meta($post_id, $prefix . 'listing-fr-description', 	$entry->metas[125]);
		update_post_meta($post_id, $prefix . 'listing-de-description', 	$entry->metas[126]);
		update_post_meta($post_id, $prefix . 'listing-it-description', 	$entry->metas[127]);
		update_post_meta($post_id, $prefix . 'listing-ru-description', 	$entry->metas[128]);
		update_post_meta($post_id, $prefix . 'listing-es-description', 	$entry->metas[129]);
		update_post_meta($post_id, $prefix . 'listing-sv-description', 	$entry->metas[130]);

		$post = get_post($post_id);

		$response = vg_elastic_index_post($post_id, $post);

		update_arcgis_listing($post);

		if ($response['success'] === true) {

			$wpdb->update( 
				'vg_listing_relationships', 
				array( 
					'listing_id' 	=> $post_id,
					'status' 		=> 0,
				), 
				array( 'id' => $row_id ), 
				array( 
					'%d',
					'%d'
				), 
				array( '%d' ) 
			);

			$result = array(
				'success' => true,
				'vg_elastic_index_listing_response' => $response
			);

		} else {

			$result = array(
				'success' => false, 
				'vg_elastic_index_listing_response' => $response
			);
			
		}

		


	}

	return $result;

}

function delete_arcgis_listing($pID) {
	delete_arcgis_feature($pID, 'listing');
}

function delete_arcgis_event($pID) {
	delete_arcgis_feature($pID, 'event');
}

function delete_arcgis_feature($pID, $layer) {

	$mapObjectID 	= get_post_meta($pID, '_vg_map_object_id', true);

	if (empty($mapObjectID)) {
		return;
	}

	if ($layer == 'listing') {
		$layerID = '4';
	}

	if ($layer == 'event') {
		$layerID = '7';
	}

	$featureServer = 'http://54.194.53.236:6080/arcgis/rest/services/Visit_Gozo/Visit_Gozo/FeatureServer/' . $layerID . '/';

	$args = array(
		'f' 		=> 'json',
		'objectIds' => $mapObjectID,
	);

	$resource = $featureServer . 'deleteFeatures';

	$response = wp_remote_post( $resource, array(
		'method' => 'POST',
		'timeout' => 45,
		'redirection' => 5,
		'httpversion' => '1.0',
		'blocking' => true,
		'headers' => array(),
		'body' => $args
	));

	$jsonResponse = json_decode($response['body']);

	if (isset($jsonResponse->deleteResults)) {
		$objectID = $jsonResponse->deleteResults[0]->objectId;

		delete_post_meta($pID, '_vg_map_object_id');
	} else {
		return;
	}

}

function update_arcgis_listing($post) {

	$pm = get_post_meta($post->ID);

	$lat 			= get_post_meta($post->ID, '_vg_listing-latitude', true);
	$lng 			= get_post_meta($post->ID, '_vg_listing-longitude', true);
	$mapObjectID 	= get_post_meta($post->ID, '_vg_map_object_id', true);

	if (empty($lat) || empty($lng)) {
		return;
	}

	$featureServer = 'http://54.194.53.236:6080/arcgis/rest/services/Visit_Gozo/Visit_Gozo/FeatureServer/4/';

	$args = array(
		'where' 		=> '',
		'geometryType' 	=> 'esriGeometryEnvelope',
		'spatialRel' 	=> 'esriSpatialRelIntersects',
		'f' 			=> 'json',
		'outFields' 	=> '*',
	);

	if (!empty($mapObjectID)) {
		$args['objectIds'] = $mapObjectID;
	} else {
		$args['where'] = 'vg_web_id=' . $post->ID;
	}

	$qs = http_build_query($args);

	$response = file_get_contents($featureServer . 'query?' . $qs);

	$json = json_decode($response);

	/* Get Geometry Data */
	$geometryObject = array(
		'geometryType' => 'esriGeometryPoint', 
		'geometries' => array(
			array(
				'x' => $lng,
				'y' => $lat
			)
		)
	);

	$geometryOutput = json_encode($geometryObject);

	$geometryArgs = array(
		'inSR' 			=> 4326,
		'outSR' 		=> 102100,
		'geometries' 	=> $geometryOutput,
		'f' 			=> 'json'
	);

	$geometryServer = 'http://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer/';


	$geometryQS = http_build_query($geometryArgs);

	$geometryResponse = file_get_contents($geometryServer . 'project?' . $geometryQS);

	$geometryResponseObject = json_decode($geometryResponse);

	$x = 0;
	$y = 0;


	if (isset($geometryResponseObject->geometries[0])) {
		$x = $geometryResponseObject->geometries[0]->x;
		$y = $geometryResponseObject->geometries[0]->y;
	} else {
		return;
	}

	/* Prepare Listing Data */
	$category_id = 0;
	$category_name = '';

	$terms = wp_get_object_terms( $post->ID, 'vg_listings_cats' );
    if( $terms ){

    	foreach ($terms as $term) {

    		$termchildren = get_term_children( $term->term_id, 'vg_listings_cats' );

    		if (empty($termchildren)) {
    			$category_id = icl_object_id($term->term_id, 'vg_listings_cats', true, 'en');
				$category_name = $term->name;
    		}

    	}
        
    }

    $terms = wp_get_object_terms($post->ID, 'vg_locality_cats');

	$locality_id   = 0;
	$locality_name = '';
	foreach ($terms as $term) {
		$locality_id	= $term->term_id;
		$locality_name 	= $term->name;
		break;
	}

	$url = get_permalink($post->ID);

	$thumbnail_id = get_post_thumbnail_id($post->ID);

	$image = wp_get_attachment_image_src($thumbnail_id, 'full');

	$the_content = strip_tags($post->post_content); 

	$maxLength = 2900;
	$extraText = '...';

	$the_content = mb_strimwidth($the_content, 0, $maxLength, $extraText);

	if ($post->post_type == 'page') {

		/* FR */
		$lang_id = icl_object_id($post->ID, 'page', false, 'fr');
		$lang_post = get_post($lang_id);
		$lang_content = strip_tags($lang_post->post_content);
		$fr_descrip = ($lang_id) ? mb_strimwidth($lang_content, 0, $maxLength, $extraText) : '';

		/* DE */
		$lang_id = icl_object_id($post->ID, 'page', false, 'de');
		$lang_post = get_post($lang_id);
		$lang_content = strip_tags($lang_post->post_content);
		$de_descrip = ($lang_id) ? mb_strimwidth($lang_content, 0, $maxLength, $extraText) : '';

		/* IT */
		$lang_id = icl_object_id($post->ID, 'page', false, 'it');
		$lang_post = get_post($lang_id);
		$lang_content = strip_tags($lang_post->post_content);
		$it_descrip = ($lang_id) ? mb_strimwidth($lang_content, 0, $maxLength, $extraText) : '';

		/* RU */
		$lang_id = icl_object_id($post->ID, 'page', false, 'ru');
		$lang_post = get_post($lang_id);
		$lang_content = strip_tags($lang_post->post_content);
		$ru_descrip = ($lang_id) ? mb_strimwidth($lang_content, 0, $maxLength, $extraText) : '';

		/* ES */
		$lang_id = icl_object_id($post->ID, 'page', false, 'es');
		$lang_post = get_post($lang_id);
		$lang_content = strip_tags($lang_post->post_content);
		$es_descrip = ($lang_id) ? mb_strimwidth($lang_content, 0, $maxLength, $extraText) : '';

		/* SV */
		$lang_id = icl_object_id($post->ID, 'page', false, 'sv');
		$lang_post = get_post($lang_id);
		$lang_content = strip_tags($lang_post->post_content);
		$sv_descrip = ($lang_id) ? mb_strimwidth($lang_content, 0, $maxLength, $extraText) : '';

	} else {
		$fr_descrip = (isset($pm['_vg_listing-fr-description'][0])) ? mb_strimwidth($pm['_vg_listing-fr-description'][0], 0, $maxLength, $extraText) : '';
		$de_descrip = (isset($pm['_vg_listing-de-description'][0])) ? mb_strimwidth($pm['_vg_listing-de-description'][0], 0, $maxLength, $extraText) : '';
		$it_descrip = (isset($pm['_vg_listing-it-description'][0])) ? mb_strimwidth($pm['_vg_listing-it-description'][0], 0, $maxLength, $extraText) : '';
		$ru_descrip = (isset($pm['_vg_listing-ru-description'][0])) ? mb_strimwidth($pm['_vg_listing-ru-description'][0], 0, $maxLength, $extraText) : '';
		$es_descrip = (isset($pm['_vg_listing-es-description'][0])) ? mb_strimwidth($pm['_vg_listing-es-description'][0], 0, $maxLength, $extraText) : '';
		$sv_descrip = (isset($pm['_vg_listing-sv-description'][0])) ? mb_strimwidth($pm['_vg_listing-sv-description'][0], 0, $maxLength, $extraText) : '';
	}

	$feature = array(
		'geometry' => array(
			'x' => $x,
			'y' => $y
		),
		'attributes' => array(
			'NAME' 				=> html_entity_decode($post->post_title),
			'ROUTE' 			=> '',
			'Route_ID' 			=> '',
			'DISTANCE' 			=> '',
			'Description' 		=> html_entity_decode($the_content),
			'Classification' 	=> $category_name,
			'URL' 				=> $url,
			'vg_web_id' 		=> $post->ID,
			'category_i' 		=> $category_id,
			'category_n' 		=> $category_name,
			'locality_i' 		=> $locality_id,
			'locality_n' 		=> $locality_name,
			'address_1' 		=> (isset($pm['_vg_listing-address-1'][0])) ? $pm['_vg_listing-address-1'][0] : '',
			'address_2' 		=> (isset($pm['_vg_listing-address-2'][0])) ? $pm['_vg_listing-address-2'][0] : '',
			'contact_no' 		=> (isset($pm['_vg_listing-contact-no'][0])) ? $pm['_vg_listing-contact-no'][0] : '',
			'email' 			=> (isset($pm['_vg_listing-email'][0])) ? $pm['_vg_listing-email'][0] : '',
			'website' 			=> (isset($pm['_vg_listing-website'][0])) ? $pm['_vg_listing-website'][0] : '',
			'vg_web_url' 		=> $url,
			'Image' 			=> $image[0],
			'fr_descrip' 		=> $fr_descrip,
			'de_descrip' 		=> $de_descrip,
			'it_descrip' 		=> $it_descrip,
			'ru_descrip' 		=> $ru_descrip,
			'es_descrip' 		=> $es_descrip,
			'sv_descrip'        => $sv_descrip,
			'cn_descrip'  		=> '',
		)
	);

	if (empty($json->features)) {
		/* Add Feature */

		$features_body = json_encode(array($feature));

		$resource = $featureServer . 'addFeatures';

		$response = wp_remote_post( $resource, array(
			'method' => 'POST',
			'timeout' => 45,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking' => true,
			'headers' => array(),
			'body' => array( 
				'f' => 'json', 
				'features' => $features_body
			)
		));

		$jsonResponse = json_decode($response['body']);

		if (isset($jsonResponse->addResults)) {
			$objectID = $jsonResponse->addResults[0]->objectId;

			update_post_meta($post->ID, '_vg_map_object_id', $objectID);
		} else {
			return;
		}


	} else {

		$feature['attributes']['OBJECTID'] 			= $json->features[0]->attributes->OBJECTID;
		if ($post->post_type == 'page') {
			$feature['attributes']['ROUTE'] 			= $json->features[0]->attributes->ROUTE;
			$feature['attributes']['Route_ID'] 			= $json->features[0]->attributes->Route_ID;
			$feature['attributes']['DISTANCE'] 			= $json->features[0]->attributes->DISTANCE;
			$feature['attributes']['Classification'] 	= $json->features[0]->attributes->Classification;
		}

		$features_body = json_encode(array($feature));

		$resource = $featureServer . 'updateFeatures';

		$response = wp_remote_post( $resource, array(
			'method' => 'POST',
			'timeout' => 45,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking' => true,
			'headers' => array(),
			'body' => array( 
				'f' => 'json', 
				'features' => $features_body
			)
		));

		$jsonResponse = json_decode($response['body']);


		if (isset($jsonResponse->updateResults)) {
			$objectID = $jsonResponse->updateResults[0]->objectId;

			update_post_meta($post->ID, '_vg_map_object_id', $objectID);
		} else {
			return;
		}

	}

}


function update_arcgis_event($post) {

	global $wpdb;

	$pm = get_post_meta($post->ID);

	$lat 			= get_post_meta($post->ID, '_vg_event-latitude', true);
	$lng 			= get_post_meta($post->ID, '_vg_event-longitude', true);
	$mapObjectID 	= get_post_meta($post->ID, '_vg_map_object_id', true);

	if (empty($lat) || empty($lng)) {
		return;
	}

	$featureServer = 'http://54.194.53.236:6080/arcgis/rest/services/Visit_Gozo/Visit_Gozo/FeatureServer/7/';

	$args = array(
		'where' 		=> '',
		'geometryType' 	=> 'esriGeometryEnvelope',
		'spatialRel' 	=> 'esriSpatialRelIntersects',
		'f' 			=> 'json',
		'outFields' 	=> '*',
	);

	if (!empty($mapObjectID)) {
		$args['objectIds'] = $mapObjectID;
	} else {
		$args['where'] = 'vg_web_id=' . $post->ID;
	}

	$qs = http_build_query($args);

	$response = file_get_contents($featureServer . 'query?' . $qs);

	$json = json_decode($response);

	/* Get Geometry Data */
	$geometryObject = array(
		'geometryType' => 'esriGeometryPoint', 
		'geometries' => array(
			array(
				'x' => $lng,
				'y' => $lat
			)
		)
	);

	$geometryOutput = json_encode($geometryObject);

	$geometryArgs = array(
		'inSR' 			=> 4326,
		'outSR' 		=> 102100,
		'geometries' 	=> $geometryOutput,
		'f' 			=> 'json'
	);

	$geometryServer = 'http://sampleserver1.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer/';


	$geometryQS = http_build_query($geometryArgs);

	$geometryResponse = file_get_contents($geometryServer . 'project?' . $geometryQS);

	$geometryResponseObject = json_decode($geometryResponse);

	$x = 0;
	$y = 0;

	if (isset($geometryResponseObject->geometries[0])) {
		$x = $geometryResponseObject->geometries[0]->x;
		$y = $geometryResponseObject->geometries[0]->y;
	} else {
		return;
	}

	/* Prepare Listing Data */
	$category_id = 0;
	$category_name = '';


	$terms = wp_get_object_terms( $post->ID, 'vg_events_cats' );

	if ( class_exists('WPSEO_Primary_Term') && count($terms) > 1 ) {
		// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
		$wpseo_primary_term = new WPSEO_Primary_Term( 'vg_events_cats', $post->ID );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();

		$term = get_term( $wpseo_primary_term );

		$category_id = icl_object_id($term->term_id, 'vg_events_cats', true, 'en');
		$category_name = $term->name;

	} else {

	    if( $terms ){

	    	foreach ($terms as $term) {

	    		$termchildren = get_term_children( $term->term_id, 'vg_events_cats' );

	    		if (empty($termchildren)) {
	    			$category_id = icl_object_id($term->term_id, 'vg_events_cats', true, 'en');
					$category_name = $term->name;
	    		}

	    	}
	        
	    }

	}

	

    $category_name = str_replace(array('&amp;', '&'), 'and', $category_name);

    $terms = wp_get_object_terms($post->ID, 'vg_locality_cats');

	$locality_id   = 0;
	$locality_name = '';
	foreach ($terms as $term) {
		$locality_id	= $term->term_id;
		$locality_name 	= $term->name;
		break;
	}

	$url = get_permalink($post->ID);

	$thumbnail_id = $pm['_vg_event-poster'][0];

	$image = wp_get_attachment_image_src($thumbnail_id, 'full');

	$the_content = strip_tags($post->post_content); 

	$maxLength = 950;
	$extraText = '...';

	$the_content = mb_strimwidth($the_content, 0, $maxLength, $extraText);


	/*
	$fr_descrip = (isset($pm['_vg_listing-fr-description'][0])) ? mb_strimwidth($pm['_vg_listing-fr-description'][0], 0, $maxLength, $extraText) : '';
	$de_descrip = (isset($pm['_vg_listing-de-description'][0])) ? mb_strimwidth($pm['_vg_listing-de-description'][0], 0, $maxLength, $extraText) : '';
	$it_descrip = (isset($pm['_vg_listing-it-description'][0])) ? mb_strimwidth($pm['_vg_listing-it-description'][0], 0, $maxLength, $extraText) : '';
	$ru_descrip = (isset($pm['_vg_listing-ru-description'][0])) ? mb_strimwidth($pm['_vg_listing-ru-description'][0], 0, $maxLength, $extraText) : '';
	$es_descrip = (isset($pm['_vg_listing-es-description'][0])) ? mb_strimwidth($pm['_vg_listing-es-description'][0], 0, $maxLength, $extraText) : '';
	$sv_descrip = (isset($pm['_vg_listing-sv-description'][0])) ? mb_strimwidth($pm['_vg_listing-sv-description'][0], 0, $maxLength, $extraText) : '';
	*/

	$fr_descrip = '';
	$de_descrip = '';
	$it_descrip = '';
	$ru_descrip = '';
	$es_descrip = '';
	$sv_descrip = '';

/*
	OBJECTID ( type: esriFieldTypeOID , alias: OBJECTID , editable: false , nullable: false )
type ( type: esriFieldTypeString , alias: type , editable: true , nullable: true , length: 80 , Coded Values: [Clubbing: Clubbing] , [Cultural: Cultural] , [Dance: Dance] , ...9 more... )
header ( type: esriFieldTypeString , alias: header , editable: true , nullable: true , length: 80 )
blurb ( type: esriFieldTypeString , alias: blurb , editable: true , nullable: true , length: 168 )
url ( type: esriFieldTypeString , alias: url , editable: true , nullable: true , length: 134 )
locality ( type: esriFieldTypeString , alias: locality , editable: true , nullable: true , length: 80 )
last_edite ( type: esriFieldTypeString , alias: last_edite , editable: true , nullable: true , length: 80 )
datefrom ( type: esriFieldTypeDate , alias: datefrom , editable: true , nullable: true , length: 36 )
dateto ( type: esriFieldTypeDate , alias: dateto , editable: true , nullable: true , length: 36 )
Description ( type: esriFieldTypeString , alias: Description , editable: true , nullable: true , length: 1000 )
*/

	$single_dates_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 0 AND post_id = %d ORDER BY `date` ASC LIMIT 10', $post->ID);

	$date_ranges_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 1 AND post_id = %d ORDER BY start_date_range ASC LIMIT 10', $post->ID);

	$single_dates_results = $wpdb->get_results($single_dates_sql);

	$date_ranges_results = $wpdb->get_results($date_ranges_sql);

	$start_date = 0;
	$end_date = 0;

	if (!empty($single_dates_results)) {
		$start_date = $single_dates_results[0]->date;

		$last_date = array_pop($single_dates_results);

		$end_date = $last_date->date;

	}

	if (!empty($date_ranges_results)) {
		$start_date = $date_ranges_results[0]->start_date_range;
		$end_date = $date_ranges_results[0]->end_date_range;
		
	}

	$blurb = '';

	if (isset($pm['_vg_event-venue'][0])) {
		$blurb .= 'Venue: ' . $pm['_vg_event-venue'][0];
	}


	$feature = array(
		'geometry' => array(
			'x' => $x,
			'y' => $y
		),
		'attributes' => array(
			'type' 			=> $category_name,
			'Classification'=> $category_name,
			'header' 		=> $post->post_title,
			'Name'			=> html_entity_decode($post->post_title),
			'blurb' 		=> $blurb,
			'url' 			=> $url,
			'locality' 		=> $locality_name,
			'datefrom' 		=> $start_date * 1000,
			'dateto' 		=> $end_date * 1000,
			'Description' 	=> html_entity_decode($the_content),
			'vg_web_id' 	=> $post->ID,
			'Image' 		=> $image[0],
		)
	);

	/*
	echo '<pre>';
	print_r($feature);
	echo '</pre>';
	*/

	if (empty($json->features)) {
		/* Add Feature */

		//echo 'add feature!<br />';

		$features_body = json_encode(array($feature));

		$resource = $featureServer . 'addFeatures';

		$response = wp_remote_post( $resource, array(
			'method' => 'POST',
			'timeout' => 45,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking' => true,
			'headers' => array(),
			'body' => array( 
				'f' => 'json', 
				'features' => $features_body
			)
		));

		/*
		echo '<pre>';
		print_r($response);
		echo '</pre>';
		*/
		

		$jsonResponse = json_decode($response['body']);

		/*
		echo '<pre>';
		print_r($jsonResponse);
		echo '</pre>';
		*/
		

		if (isset($jsonResponse->addResults)) {
			$objectID = $jsonResponse->addResults[0]->objectId;

			update_post_meta($post->ID, '_vg_map_object_id', $objectID);

			//exit('CREATE');
		} else {
			//exit('CREATE');
			return;
		}

		//exit('CREATE');


	} else {

		$feature['attributes']['OBJECTID'] 			= $json->features[0]->attributes->OBJECTID;

		$features_body = json_encode(array($feature));

		$resource = $featureServer . 'updateFeatures';

		$response = wp_remote_post( $resource, array(
			'method' => 'POST',
			'timeout' => 45,
			'redirection' => 5,
			'httpversion' => '1.0',
			'blocking' => true,
			'headers' => array(),
			'body' => array( 
				'f' => 'json', 
				'features' => $features_body
			)
		));

		/*
		echo 'Update:<pre>';
		print_r($response);
		echo '</pre>';
		*/

		$jsonResponse = json_decode($response['body']);

		/*
		echo '<pre>';
		print_r($jsonResponse);
		echo '</pre>';
		*/

		if (isset($jsonResponse->updateResults)) {
			$objectID = $jsonResponse->updateResults[0]->objectId;

			update_post_meta($post->ID, '_vg_map_object_id', $objectID);

			//exit('UPDATE');
		} else {
			//exit('UPDATE');
			return;
		}

		//exit('UPDATE');

	}

}