<?php

// Register Custom Post Type
function vg_events() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'vg' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'vg' ),
		'menu_name'             => __( 'Events', 'vg' ),
		'name_admin_bar'        => __( 'Events', 'vg' ),
		'archives'              => __( 'Event Archives', 'vg' ),
		'parent_item_colon'     => __( 'Parent Event:', 'vg' ),
		'all_items'             => __( 'All Events', 'vg' ),
		'add_new_item'          => __( 'Add New Event', 'vg' ),
		'add_new'               => __( 'Add New', 'vg' ),
		'new_item'              => __( 'New Event', 'vg' ),
		'edit_item'             => __( 'Edit Event', 'vg' ),
		'update_item'           => __( 'Update Event', 'vg' ),
		'view_item'             => __( 'View Event', 'vg' ),
		'search_items'          => __( 'Search Event', 'vg' ),
		'not_found'             => __( 'Not found', 'vg' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'vg' ),
		'featured_image'        => __( 'Featured Image', 'vg' ),
		'set_featured_image'    => __( 'Set featured image', 'vg' ),
		'remove_featured_image' => __( 'Remove featured image', 'vg' ),
		'use_featured_image'    => __( 'Use as featured image', 'vg' ),
		'insert_into_item'      => __( 'Insert into Event', 'vg' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Event', 'vg' ),
		'items_list'            => __( 'Events list', 'vg' ),
		'items_list_navigation' => __( 'Events list navigation', 'vg' ),
		'filter_items_list'     => __( 'Filter Events list', 'vg' ),
	);
	$rewrite = array(
		'slug'                  => 'events',
		'with_front'            => false,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Event', 'vg' ),
		'description'           => __( 'visitgozo.com events manager', 'vg' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'vg_events_cats', 'vg_locality_cats' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'vg_events', $args );

}
add_action( 'init', 'vg_events', 0 );

add_filter('wpseo_breadcrumb_links', 'vg_events_wpseo_breadcrumb_links');

function vg_events_wpseo_breadcrumb_links($current_crumbs) {

	global $prefix;

	if (is_singular('vg_events')) {

		$crumbs = array();
		$crumbs[] = $current_crumbs[0];

		$eventPageID = 96;

		$translated_page_id = icl_object_id($eventPageID, 'page', true);

		$crumbs[] = array(
			'text' => get_the_title($translated_page_id),
			'url' => get_permalink($translated_page_id),
			'allow_html' => 1,
		);

		$crumbs[] = $current_crumbs[1];

		$current_crumbs = $crumbs;


	}

	return $current_crumbs;

}