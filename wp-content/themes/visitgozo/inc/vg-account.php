<?php


add_action('init', 	'vg_init');
add_action( 'template_redirect', 'vg_template_redirect');

function vg_sub_account_pages() {

	return array('manage-listings', 'manage-events');

}

function vg_init() {

	$sap = vg_sub_account_pages();

	foreach ($sap as $page) {
		add_rewrite_endpoint($page, EP_PAGES);
	}

}

function vg_template_redirect() {

	global $wp_query;

	$sap = vg_sub_account_pages();

	if (is_page_template( 'page-templates/account.php' )) {
		/* We know that we are on the my account page */

		/* Permission Check - Start */
		$current_user = wp_get_current_user();

		if (!is_user_logged_in()) {
			return;
		}
		/* Permission Check - End */


		foreach ($sap as $page) {
			if (isset($wp_query->query_vars[$page])) {

				include get_template_directory() . '/template-parts/account/' . $page . '.php';

				exit();
			}
		}

	} else {

		/* We are not on my account page but lets make sure that no other page can access our endpoints */
		foreach ($sap as $page) {
			if (isset($wp_query->query_vars[$page])) {
				wp_redirect(get_permalink(), 301);
				exit();
			}
		}

	}

	return;	

}


add_filter('wpseo_breadcrumb_links', 'vg_account_wpseo_breadcrumb_links');

function vg_account_wpseo_breadcrumb_links($current_crumbs) {

	global $wp_query;

	if (is_page_template( 'page-templates/account.php' )) {

		$sap = vg_sub_account_pages();

		foreach ($sap as $page) {

			if (isset($wp_query->query_vars[$page])) {

				$crumbs = array();

				$crumbs[] = $current_crumbs[0];

				$crumbs[] = array(
        			'text' => get_the_title($current_crumbs[1]['id']),
        			'url' => get_permalink($current_crumbs[1]['id']),
        			'allow_html' => 1,
        		);

        		$page_name = '';

        		if($page == 'manage-events') {
        			$page_name = __('Manage Events', 'vg-front');
        		}

        		if($page == 'manage-listings') {
        			$page_name = __('Manage Listings', 'vg-front');
        		}

        		$crumbs[] = array(
        			'text' => $page_name
        		);

				$current_crumbs = $crumbs;


			}

		}



	}

	return $current_crumbs;

}

/* Wordpress Login Page */
function vg_my_login_logo() { 

	wp_enqueue_style( 'vg-fonts', get_template_directory_uri() . '/css/vg-fonts.css' );
	?>
    <style type="text/css">

    	body.login { color: #000;background: #fff; }
		body.login, input, textarea { font-family: 'Sinkin Sans', sans-serif; font-size: 14px; }

        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_template_directory_uri(); ?>/images/logo-visitgozo.svg');
            width: 160px;
            height: 160px;
            -webkit-background-size:160px;
            background-size:160px;
        }

        body.login.wp-core-ui .button-primary {
	        background: #e15151;
		    border: 0;
		    border-radius: 0;
		    -webkit-box-shadow: none;
		    box-shadow: none;
		    text-shadow: none;
		    padding: 15px 20px;
		    height: auto;
		    line-height: 100%;	    
		}

		body.login .message {
		    border-left: 4px solid #e15151;
		}

		body.login form .input, 
		body.login input[type=text] {
			color: #555;
		    background-color: #fff;
		    border-color: #ccc;
		    border-width: 1px;
		    border-style: solid;
		    -moz-border-radius: 0;
		    -webkit-border-radius: 0;
		    border-radius: 0;
		    width: 100%;
		    max-width: 100%;
		    font-size: 14px;
		    padding: 6px 10px;
		    -webkit-box-sizing: border-box;
		    -moz-box-sizing: border-box;
		    box-sizing: border-box;
		    outline: 0;
		    font-weight: normal;
		    box-shadow: 0 1px 1px rgba(0,0,0,.075) inset;
		}
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'vg_my_login_logo' );

function vg_my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'vg_my_login_logo_url' );

function vg_my_login_logo_url_title() {
    return 'Visit Gozo';
}
add_filter( 'login_headertitle', 'vg_my_login_logo_url_title' );


add_filter( 'retrieve_password_message', 'vg_retrieve_password_message' );

function vg_retrieve_password_message( $message ) {
	return str_replace(array('<', '>'), '', $message);
}

?>