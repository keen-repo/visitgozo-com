<?php

require_once(get_template_directory() . '/inc/vendor/autoload.php');

use Elasticsearch\ClientBuilder;

function vg_elastic_index_post($postID, $post) {

	global $prefix;

	$client = ClientBuilder::create()->build();

	$result = array(
		'success' => false,
	);

	$performDelete = false;

	/* make sure post is published or else delete from index */
	if ($post->post_status !== 'publish') {
		$performDelete = true;
	}

	$unsearchable = (int)get_post_meta($postID, $prefix . 'is_unsearchable', true);

	if ($unsearchable) {
		$performDelete = true;
	}

	/*
	 *
	 * make sure that we remove the search term when the post is not published anymore,
	 * because it should not be searchable
	 *
	*/
	if ($performDelete) {

		try {

			$params = array();
		    $params['index'] = 'vg';
		    $params['type'] = 'listings';
		    $params['id'] = $postID;
		    $response = $client->delete($params);

		    $result = array('success' => true, 'action' => 'deleted', 'response' => $response, 'params' => $params);

		} catch (Exception $e) {
	    	//echo 'Caught exception: ',  $e->getMessage(), "\n";
	    	$result = array('success' => false, 'action' => 'deleted', 'error' => $e->getMessage(), 'params' => $params);
		}

		delete_arcgis_listing($postID);

		return $result;

	}

	$terms = wp_get_object_terms($postID, 'vg_listings_cats');

	$categories = array();
	foreach ($terms as $term) {
		/* add categories using english term ids since we use them to search */
		$eng_term_id = icl_object_id($term->term_id, 'vg_listings_cats', true, 'en');
		$categories[] = $eng_term_id;
	}

	$terms = wp_get_object_terms($postID, 'vg_locality_cats');

	$localities = array();
	foreach ($terms as $term) {
		$localities[] = $term->term_id;
	}

	$terms = wp_get_object_terms($postID, 'vg_search_tags');

	$search_tags = array();
	foreach ($terms as $term) {
		$search_tags[] = $term->name;
	}

	$post_type = '';
	$language = '';

	if ($post->post_type == 'vg_listings') {
		$post_type = 'listings';
		$language = 'en_listing';
	}

	if ($post->post_type == 'page') {
		$post_type = 'page';

		$language_details = apply_filters( 'wpml_post_language_details', NULL, $postID ) ;

		$language = $language_details['language_code'];
	}	

	if ($post->post_type == 'post') {

		$post_type = 'post';

		$language_details = apply_filters( 'wpml_post_language_details', NULL, $postID ) ;

		$language = $language_details['language_code'];
	}

	$lat = get_post_meta($postID, '_vg_listing-latitude', true);
	$lon = get_post_meta($postID, '_vg_listing-longitude', true);

	if (empty($lat)) $lat = 0;
	if (empty($lon)) $lon = 0;

	$tripadvisor_ranking = get_post_meta($postID, '_vg_listing-tripadvior-ranking', true);

	if (empty($tripadvisor_ranking)) $tripadvisor_ranking = 0;

	$title_prefix = mb_strtolower(mb_substr($post->post_title, 0, 1, 'UTF-8'), 'UTF-8');

	$title = str_replace('&', 'and', $post->post_title);

	$params = [
	    'index' => 'vg',
	    'type' => 'listings',
	    'id' => $postID,
	    'body' => [
	    	'title' 		=> $title,
	    	//'title_exact' 	=> $title,
	    	'title_prefix' 	=> $title_prefix,
	    	'post_id' 		=> $postID,
	    	'post_type' 	=> $post_type,
	    	'language'		=> $language,
	    	'location' => array(
	    		'lat' => $lat,
	    		'lon' => $lon,
	    	),
	    	'tripadvisor_ranking' => $tripadvisor_ranking,
	    	'categories' => $categories,
	    	'amenities' => array(),
	    	'localities' => $localities,
	    	'tags' => $search_tags,
	    ]
	];

	try {

		$response = $client->index($params);

		$result = array('success' => true, 'action' => 'created', 'response' => $response, 'params' => $params);

	} catch (Exception $e) {
    	//echo 'Caught exception: ',  $e->getMessage(), "\n";
    	$result = array('success' => false, 'action' => 'created', 'error' => $e->getMessage(), 'params' => $params);

	}

	return $result;

}


function vg_save_post_vg_listings($postID, $post, $update) {

	if (wp_is_post_revision( $postID ) ) return;

	if ( !current_user_can( 'edit_post', $postID ) ) return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

	if (defined('DOING_AJAX') && DOING_AJAX) return;
	
	if (!is_admin()) return;

	$resp = vg_elastic_index_post($postID, $post);

	if ($post->post_type == 'page') {

		//$map_object_id = get_post_meta($postID, '_vg_map_object_id', true);

		//if ($map_object_id) {
			update_arcgis_listing($post);
		//}
		
	}

}

add_action( 'save_post_vg_listings', 'vg_save_post_vg_listings', 99, 3 );
add_action( 'save_post_page', 'vg_save_post_vg_listings', 99, 3 );



function vg_save_post_vg_listings_post($postID, $post, $update) {

	if (wp_is_post_revision( $postID ) ) return;

	if ( !current_user_can( 'edit_post', $postID ) ) return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

	if (defined('DOING_AJAX') && DOING_AJAX) return;
	
	if (!is_admin()) return;

	$resp = vg_elastic_index_post($postID, $post);


	// var_dump($resp);

	// die();

	// if ($post->post_type == 'page') {

	// 	//$map_object_id = get_post_meta($postID, '_vg_map_object_id', true);

	// 	//if ($map_object_id) {
	// 		update_arcgis_listing($post);
	// 	//}
		
	// }

}

//add_action( 'save_post_vg_listings', 'vg_save_post_vg_listings_post', 99, 3 );
add_action( 'save_post_post', 'vg_save_post_vg_listings_post', 99, 3 );




function vg_elastic_index_event($postID, $post) {

	global $prefix, $wpdb;

	$current_timestamp = current_time('timestamp');

	$current_time = mktime(0,0,0, date('n', $current_timestamp), date('j', $current_timestamp), date('Y', $current_timestamp));

	$client = ClientBuilder::create()->build();

	$result = array(
		'success' => false,
	);

	$performDelete = false;

	/* make sure post is published or else delete from index */
	if ($post->post_status !== 'publish') {
		$performDelete = true;
	}

	/*
	 *
	 * make sure that we remove the search term when the post is not published anymore,
	 * because it should not be searchable
	 *
	*/
	if ($performDelete) {

		try {

			$params = array();
		    $params['index'] = 'vg';
		    $params['type'] = 'listings';
		    $params['id'] = $postID;
		    $response = $client->delete($params);

		    $result = array('success' => true, 'action' => 'deleted', 'response' => $response, 'params' => $params);

		} catch (Exception $e) {
	    	//echo 'Caught exception: ',  $e->getMessage(), "\n";
	    	$result = array('success' => false, 'action' => 'deleted', 'error' => $e->getMessage(), 'params' => $params);
		}

		delete_arcgis_event($postID);

		return $result;

	}

	$terms = wp_get_object_terms($postID, 'vg_events_cats');

	$categories = array();
	foreach ($terms as $term) {
		$categories[] = $term->term_id;
	}

	$terms = wp_get_object_terms($postID, 'vg_locality_cats');

	$localities = array();
	foreach ($terms as $term) {
		$localities[] = $term->term_id;
	}

	$terms = wp_get_object_terms($postID, 'vg_search_tags');

	$search_tags = array();
	foreach ($terms as $term) {
		$search_tags[] = $term->name;
	}


	$post_type = 'events';
	$language = 'en_events';

	$lat = get_post_meta($postID, '_vg_event-latitude', true);
	$lon = get_post_meta($postID, '_vg_event-longitude', true);

	if (empty($lat)) $lat = 0;
	if (empty($lon)) $lon = 0;

	$tripadvisor_ranking = 0;

	$title_prefix = mb_strtolower(mb_substr($post->post_title, 0, 1, 'UTF-8'), 'UTF-8');

	$single_dates_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 0 AND `date` > %d AND post_id = %d ORDER BY `date` ASC LIMIT 10', $current_time, $postID);

	$date_ranges_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 1 AND end_date_range > %d AND post_id = %d ORDER BY start_date_range ASC LIMIT 10', $current_time, $postID);

	$single_dates_results = $wpdb->get_results($single_dates_sql);

	$date_ranges_results = $wpdb->get_results($date_ranges_sql);

	$earliest_event_date = 0;

	if (!empty($single_dates_results)) {
		
		foreach ($single_dates_results as $row) {

			if ($row->date > 0) {
				$earliest_event_date = $row->date;
				break;
			}
			
		}

	}

	if (!empty($date_ranges_results)) {

		foreach ($date_ranges_results as $row) {

			if ($row->start_date_range > 0) {

				$earliest_event_date = $row->start_date_range;
				
				break;
			}
			
		}


	}

	$title = str_replace('&', 'and', $post->post_title);

	$params = [
	    'index' => 'vg',
	    'type' => 'listings',
	    'id' => $postID,
	    'body' => [
	    	'title' 		=> $title,
	    	//'title_exact' 	=> $post->post_title,
	    	'title_prefix' 	=> $title_prefix,
	    	'post_id' 		=> $postID,
	    	'post_type' 	=> $post_type,
	    	'language'		=> $language,
	    	'location' => array(
	    		'lat' => $lat,
	    		'lon' => $lon,
	    	),
	    	'tripadvisor_ranking' => $tripadvisor_ranking,
	    	'categories' => $categories,
	    	'amenities' => array(),
	    	'localities' => $localities,
	    	'tags' => $search_tags,
	    	'start_date' => $earliest_event_date
	    ]
	];

	try {

		$response = $client->index($params);

		$result = array('success' => true, 'action' => 'created', 'response' => $response, 'params' => $params);

	} catch (Exception $e) {
    	//echo 'Caught exception: ',  $e->getMessage(), "\n";
    	$result = array('success' => false, 'action' => 'created', 'error' => $e->getMessage(), 'params' => $params);

	}

	return $result;

}


function vg_save_post_vg_events($postID, $post, $update) {

	global $prefix, $wpdb;

	if (wp_is_post_revision( $postID ) ) return;

	if ( !current_user_can( 'edit_post', $postID ) ) return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

	if (defined('DOING_AJAX') && DOING_AJAX) return;
	
	if (!is_admin()) return;

	$wpdb->delete( 'vg_event_dates', array( 'post_id' => $postID ), array( '%d' ) );

	$occurance = get_post_meta($postID, $prefix . 'event-occurance', true);

	if ($occurance == 'specific_days') {

		$event_dates = get_post_meta($postID, $prefix . 'event-dates', true);

		foreach ($event_dates as $event_date) {

			$date 		= $event_date['date']['timestamp'];
			$start_time = (isset($event_date['start-time']['formatted'])) ? $event_date['start-time']['formatted'] : 0;
			$end_time 	= (isset($event_date['ending-time']['formatted'])) ? $event_date['ending-time']['formatted'] : 0;

			$time_parts = explode(':', $start_time);

			$date = mktime($time_parts[0], $time_parts[1], 0, date('n', $date), date('j', $date), date('Y', $date));

			$wpdb->insert( 
			    'vg_event_dates', 
			    array( 
			      'post_id' 	=> $postID,
			      'date'     	=> $date,
			      'start_time'  => $start_time,
			      'end_time'    => $end_time,
			      'range'		=> 0,
			    ), 
			    array( 
			      '%d',
			      '%d',
			      '%s',
			      '%s',
			      '%d'
			    )
			);

		}

	}

	if ($occurance == 'date_range') {

		$start_date = get_post_meta($postID, 	$prefix . 'event-start-date', 	true);
		$end_date 	= get_post_meta($postID, 	$prefix . 'event-end-date', 	true);
		$start_time = get_post_meta($postID, 	$prefix . 'event-start-time', 	true);
		$end_time 	= get_post_meta($postID, 	$prefix . 'event-end-time', 	true);

		$wpdb->insert( 
		    'vg_event_dates', 
		    array( 
		      'post_id' 			=> $postID,
		      'range'				=> 1,
		      'start_date_range'  	=> $start_date,
		      'end_date_range'    	=> $end_date,
		      'start_time_range'    => $start_time,
		      'end_time_range' 		=> $end_time
		      
		    ), 
		    array( 
		      '%d',
		      '%d',
		      '%d',
		      '%d',
		      '%s',
		      '%s'
		    )
		);

	}

	$resp = vg_elastic_index_event($postID, $post);

	if ($resp['action'] !== 'deleted') {
		update_arcgis_event($post);
	}

}

add_action( 'save_post_vg_events', 'vg_save_post_vg_events', 99, 3 );

function vg_build_elastic_query_body($args) {

	$bool_query = array(
		'should' => array(),
		'must' => array(),
	);

	if (!empty($args['keywords'])) {

		$bool_query['should'][] = [ 
        	"match" => [ 
        		"title" => $args['keywords'] 
        	]
        ];

        $bool_query['should'][] = [ 
        	"match" => [ 
        		"tags" => [
        			'query' => $args['keywords'],
        			'boost' => 2
        		]
        	]
        ];

	}

	if (isset($args['widget_type']) && $args['widget_type'] == 'event') {

		$lang = array('en_events');

	} else if (isset($args['widget_type']) && $args['widget_type'] == 'listing') {

		$lang = array($args['search_lang'], 'en_listing');

	} else {
		
		$lang = array($args['search_lang'], 'en_listing', 'en_events');

	}

	$bool_query['must'][] = [ 
    	"terms" => [ 
    		"language" => $lang
    	]
    ];

	if (!empty($args['title_prefix'])) {

		$bool_query['must'][] = [ 
        	"terms" => [ 
        		"title_prefix" => $args['title_prefix']
        	]
        ];

	}

	if (!empty($args['categories'])) {

		$bool_query['must'][] = [ 
        	"terms" => [ 
        		"categories" => $args['categories']
        	]
        ];

	}

	if (!empty($args['localities'])) {

		$bool_query['must'][] = [ 
        	"terms" => [ 
        		"localities" => $args['localities']
        	]
        ];

	}

	$query = array(
		'filtered' => array(
			'query' => array(
				'bool' => $bool_query,
			)
		)
	);

	$sort = array();

	if (!isset($args['is_count'])) {

		if (empty($args['keywords'])) {

			if (isset($args['widget_type']) && $args['widget_type'] == 'event') {
				$sort = array(
					array(
						'start_date' => array(
							'order' => 'asc'
						)
					)
				);
			} else {
				$sort = array(
					array(
						'title_prefix' => array(
							'order' => 'asc'
						)
					)
				);
			}

			

		}

	}

	if (isset($args['geo_query'])) {

		 $query['filtered']['filter'] = array(
			'geo_bounding_box' => array(
				'type' => 'indexed',
				'location' => array(
					'top_left' => array(
						'lat' => 36.085348,
						'lon' => 14.169654,
					),
					'bottom_right' => array(
						'lat' => 36.002135,
						'lon' => 14.354808,
					)
				)
			)
		);

		if (!isset($args['is_count'])) {

			$sort = array(
				array(
					'_geo_distance' => array(
						'location' => array(
							'lat' => $args['latitude'],
							'lon' => $args['longitude'],
						),
						'order' => 'asc',
						'unit' => 'km',
						'distance_type' => 'plane',
					)
				)
			);
		}


	}

	$body = array(
	    'query' => $query,
	);

	if (!empty($sort)) {
		$body['sort'] = $sort;
	}

	if (!isset($args['is_count'])) {
		$body['size'] = $args['size'];
		$body['from'] = $args['from'];
	}

	return $body;

}

function vg_front_search($args, $min_score=0.10) {

	global $prefix, $wpdb;

	$current_timestamp = current_time('timestamp');

	$current_time = mktime(0,0,0, date('n', $current_timestamp), date('j', $current_timestamp), date('Y', $current_timestamp));

	if (isset($args['min_score'])) {
		$min_score = $args['min_score'];
	}
    
	$client = ClientBuilder::create()->build();

	$totals = array();

	/* Lets get title prefix totals first */
	$lang_ranges = array(
		'a-f' => range('a', 'f'), 
		'g-l' => range('g', 'l'), 
		'm-r' => range('m', 'r'), 
		's-z' => range('s', 'z')
	);

	if (!empty($args['letter_range'])) {
		$args['title_prefix'] = $lang_ranges[$args['letter_range']];
	}

	foreach ($lang_ranges as $lang_key=>$lang) {

		$temp_args = $args;

		$temp_args['title_prefix'] 	= $lang;
		$temp_args['is_count'] 		= true;

		$body = vg_build_elastic_query_body($temp_args);

		$params = [
		    'index' => 'vg',
		    'type' => 'listings',
		    'body' => $body
		];

		try {

			$response = $client->count($params);

			$totals[$lang_key] = $response['count'];

		} catch (Exception $e) {

			echo $e->getMessage();

		} 

	}

	/* now lets get absolute totals */
	$temp_args = $args;

	$temp_args['is_count'] 		= true;

	$body = vg_build_elastic_query_body($temp_args);

	$params = [
	    'index' => 'vg',
	    'type' => 'listings',
	    'body' => $body
	];

	try {

		$response = $client->count($params);

		$totals['all'] = $response['count'];

	} catch (Exception $e) {

		echo $e->getMessage();

	} 

	$result = array(
		'success' => false, 
		'totals' => $totals,
		'data' => array()
	);

	if (!isset($args['count_only'])) {

		if (isset($args['pageSize'])) {
			$args['size'] = $args['pageSize'];

			if ($args['pageNumber'] == 1) {
				$args['from'] = 0;
			} else {
				$args['from'] = ($args['pageNumber']-1)*$args['pageSize'];
			}

		} else {
			$args['size'] = 20;
		}

		$body = vg_build_elastic_query_body($args);

		$params = [
		    'index' => 'vg',
		    'type' => 'listings',
		    'body' => $body
		];

		try {

			$response = $client->search($params);

		} catch (Exception $e) {

			echo $e->getMessage();

		} 

		$data = array();

		foreach ($response['hits']['hits'] as $hit) {
			
			if ($hit['_score'] < $min_score && !empty($args['keywords'])) continue;

			$source = $hit['_source'];

			$source['_score'] = (isset($hit['sort'])) ? $hit['sort'][0] : $hit['_score'];

			$translated_post_id = icl_object_id($source['post_id'], get_post_type($source['post_id']), true, $args['search_lang']);

			$en_post_id = icl_object_id($source['post_id'], get_post_type($source['post_id']), true, 'en');

			$source['title'] = get_the_title($translated_post_id);

			$source['translated_post_id'] = $translated_post_id;

			$source['permalink'] = get_permalink($translated_post_id);

			// $source['permalink'] = apply_filters( 'wpml_permalink', $source['permalink'], $args['search_lang'] ); 
			$source['permalink'] = apply_filters( 'wpml_permalink', $source['permalink'], 'en' ); 

			if ($source['post_type'] == 'events') {


				$thumbnail_id = get_post_meta($en_post_id, $prefix . 'event-poster', true);

				/*
				$single_dates_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 0 AND `date` > %d AND post_id = %d ORDER BY `date` ASC LIMIT 10', $current_time, $en_post_id);

				$date_ranges_sql = $wpdb->prepare('SELECT * FROM vg_event_dates WHERE `range` = 1 AND end_date_range > %d AND post_id = %d ORDER BY start_date_range ASC LIMIT 10', $current_time, $en_post_id);

				$single_dates_results = $wpdb->get_results($single_dates_sql);

				$date_ranges_results = $wpdb->get_results($date_ranges_sql);

				$event_day = '';
				$event_mon = '';

				if (!empty($single_dates_results)) {

					foreach ($single_dates_results as $row) {

						if ($row->date > 0) {
							$event_day = date('d', $row->date);
							$event_mon = date('M', $row->date);
							break;
						}
						
					}

				}

				if (!empty($date_ranges_results)) {

					foreach ($date_ranges_results as $row) {

						if ($row->start_date_range > 0) {

							if ($row->start_date_range < $current_time) {
								$event_day = date('d', $current_time);
								$event_mon = date('M', $current_time);
							} else {
								$event_day = date('d', $row->start_date_range);
								$event_mon = date('M', $row->start_date_range);
							}
							
							break;
						}
						
					}


				}
				*/

				$event_day = '';
				$event_mon = '';

				if (isset($source['start_date'])) {

					if ($source['start_date'] < $current_time) {
						$source['start_date'] = $current_time;
					}

					$event_day = date('d', $source['start_date']);
					$event_mon = date('M', $source['start_date']);



				}


				$source['date']['date'] = $event_day;
				$source['date']['month'] = $event_mon;

			} else {
				$thumbnail_id = get_post_thumbnail_id($en_post_id);
			}

			$source['thumbnail'] = wp_get_attachment_image_src($thumbnail_id, 'medium-uncropped');

			$data[] = $source;

		}

		$result['data'] = $data;

		if (!empty($data))
			$result['success'] = true;

	} else {
		$result['success'] = true;
	}

	

	return $result;

}

?>