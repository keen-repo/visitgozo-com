<?php require_once( '../../../../../wp-load.php' ); ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>VisitGozo.com Panoramas</title>
    <meta name="robots" content="noindex, nofollow">
    <style>
      html, body {
        margin: 0;
        width: 100%;
        height: 100%;
        overflow: hidden;
      }
    </style>
</head>
<body>
    <script src="<?php bloginfo( 'template_url' ); ?>/js/three.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/js/panolens.min.js"></script>
    <?php if(isset($_GET['pano'])){ $pano = $_GET['pano']; ?>
    <script>
        var panorama, viewer;

        panorama = new PANOLENS.ImagePanorama( '<?php bloginfo('template_url'); ?>/inc/panos/img/<?php echo $pano; ?>.jpg' );

        viewer = new PANOLENS.Viewer();
        viewer.add( panorama );
    </script>
    <?php } ?>
</body>
</html>