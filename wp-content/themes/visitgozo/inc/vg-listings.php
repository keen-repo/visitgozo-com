<?php

// Register Listings Custom Post Type
function vg_listings() {

	$labels = array(
		'name'                  => _x( 'Listings', 'Post Type General Name', 'vg' ),
		'singular_name'         => _x( 'Listing', 'Post Type Singular Name', 'vg' ),
		'menu_name'             => __( 'Listings', 'vg' ),
		'name_admin_bar'        => __( 'Listings', 'vg' ),
		'archives'              => __( 'Listing Archives', 'vg' ),
		'parent_item_colon'     => __( 'Parent Listing:', 'vg' ),
		'all_items'             => __( 'All Listings', 'vg' ),
		'add_new_item'          => __( 'Add New Listing', 'vg' ),
		'add_new'               => __( 'Add New', 'vg' ),
		'new_item'              => __( 'New Listing', 'vg' ),
		'edit_item'             => __( 'Edit Listing', 'vg' ),
		'update_item'           => __( 'Update Listing', 'vg' ),
		'view_item'             => __( 'View Listing', 'vg' ),
		'search_items'          => __( 'Search Listing', 'vg' ),
		'not_found'             => __( 'Not found', 'vg' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'vg' ),
		'featured_image'        => __( 'Featured Image', 'vg' ),
		'set_featured_image'    => __( 'Set featured image', 'vg' ),
		'remove_featured_image' => __( 'Remove featured image', 'vg' ),
		'use_featured_image'    => __( 'Use as featured image', 'vg' ),
		'insert_into_item'      => __( 'Insert into Listing', 'vg' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Listing', 'vg' ),
		'items_list'            => __( 'Listings list', 'vg' ),
		'items_list_navigation' => __( 'Listings list navigation', 'vg' ),
		'filter_items_list'     => __( 'Filter items list', 'vg' ),
	);
	$rewrite = array(
		'slug'                  => 'directory/%vg_listings_cats%',
		'with_front'            => false,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Listing', 'vg' ),
		'description'           => __( 'visitgozo.com listings', 'vg' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'author' ),
		'taxonomies'            => array( 'vg_listings_cats' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-list-view',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	
	register_post_type( 'vg_listings', $args );

}
add_action( 'init', 'vg_listings', 0 );

function prepare_listing_widget_args($postID) {

		global $prefix;

		$widget_type = get_post_meta(icl_object_id($postID, 'page', false, 'en'), $prefix . 'lwf_widget_type', true);

		if ($widget_type == 'event') {
			$default_categories = get_post_meta(icl_object_id($postID, 'page', false, 'en'), $prefix . 'lwf_default_event_categories', true);
		} else {
			$default_categories = get_post_meta(icl_object_id($postID, 'page', false, 'en'), $prefix . 'lwf_default_categories', true);
		}

		$default_localities = get_post_meta(icl_object_id($postID, 'page', false, 'en'), $prefix . 'lwf_default_localities', true);

		$allow_unselected_localities = get_post_meta(icl_object_id($postID, 'page', false, 'en'), $prefix . 'lwf_allow_unselected_localities', true);
		$allow_unselected_categories = get_post_meta(icl_object_id($postID, 'page', false, 'en'), $prefix . 'lwf_allow_unselected_categories', true);

		$categories = array();

		foreach ($default_categories as $category_trees) {

			$categories[] = array_pop($category_trees['categories']);

		}

		$localities = explode(',', $default_localities);

		$shortcode_args = array(
			'categories'                  => $categories,
			'localities' 				  => $localities,
			'allow_unselected_localities' => $allow_unselected_localities,
			'allow_unselected_categories' => $allow_unselected_categories,
		);

		if (!empty($widget_type)) {
			$shortcode_args['widget_type'] = $widget_type;
		}

		$args = array();

		foreach ($shortcode_args as $k=>$v) {
			$args[] = $k . '="' . ((is_array($v)) ? implode(',', $v) : $v) . '"';
		}

		$shortcode = '[listing_widget ' . implode(' ', $args) . ']';

		$response = array(
			'html' => $shortcode,
			'args' => $shortcode_args
		);

		return $response;

}


function vg_shortcode_listing_widget( $atts ) {

	$atts = shortcode_atts( array(
		'categories' 					=> '',
		'localities'					=> '',
		'allow_unselected_localities' 	=> 'yes',
		'allow_unselected_categories' 	=> 'yes',
		'widget_type' 					=> 'listing',
	), $atts, 'listing_widget' );

	ob_start( );

	include(locate_template('template-parts/widget-listings.php'));

	$output = ob_get_clean( );

	return $output;

}

add_shortcode( 'listing_widget', 'vg_shortcode_listing_widget' );

/* Set up custom listing permalinks */
function vg_listing_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'vg_listings' ){
        $terms = wp_get_object_terms( $post->ID, 'vg_listings_cats' );
        if( $terms ){

        	foreach ($terms as $term) {

        		$termchildren = get_term_children( $term->term_id, 'vg_listings_cats' );

        		if (empty($termchildren)) {
        			return str_replace( '%vg_listings_cats%' , $term->slug , $post_link );
        		}

        	}
            
        }
    }
    return $post_link;
}
add_filter( 'post_type_link', 'vg_listing_permalinks', 1, 2 );


add_filter('wpseo_breadcrumb_links', 'vg_listings_wpseo_breadcrumb_links');

function vg_listings_wpseo_breadcrumb_links($current_crumbs) {

	global $prefix;

	if (is_singular('vg_listings')) {

		$lowest_term_id = 0;

		$terms = wp_get_object_terms( get_the_ID(), 'vg_listings_cats' );

        if( $terms ){

        	foreach ($terms as $term) {

        		$eng_term_id = icl_object_id($term->term_id, 'vg_listings_cats', true, 'en');

        		$termchildren = get_term_children( $eng_term_id, 'vg_listings_cats' );

        		if (empty($termchildren)) {
        			$lowest_term_id = $eng_term_id;
        			break;
        		}

        	}
            
        }

        if ($lowest_term_id) {



        	$connected_page = get_term_meta( $lowest_term_id, $prefix . 'connected-page', true );

        	if (!empty($connected_page)) {

        		$connected_page = icl_object_id($connected_page, 'page', true, 'en');

        		$crumbs = array();

        		$crumbs[] = $current_crumbs[0];

	        	$ancestors = get_ancestors( $connected_page, 'page' );

	        	$ancestors = array_reverse($ancestors);

	        	foreach ($ancestors as $pageID) {

	        		$translated_page_id = icl_object_id($pageID, 'page', true);

	        		$crumbs[] = array(
	        			'text' => get_the_title($translated_page_id),
	        			'url' => get_permalink($translated_page_id),
	        			'allow_html' => 1,
	        		);

	        	}

	        	$translated_page_id = icl_object_id($connected_page, 'page', true);

	        	$crumbs[] = array(
	    			'text' => get_the_title($translated_page_id),
	    			'url' => get_permalink($translated_page_id),
	    			'allow_html' => 1,
	    		);

	    		//$crumbs[] = $current_crumbs[1];
	    		$crumbs[] = $current_crumbs[2];

	    		$current_crumbs = $crumbs;

	    	}

        }



	}

	return $current_crumbs;

}