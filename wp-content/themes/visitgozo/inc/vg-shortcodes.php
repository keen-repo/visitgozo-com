<?php

function vg_newsletter_shortcode(){

	$formHTML = vg_render_newsletter_form();

	return '<div class="section section-newsletter newsletter-page section-padding-bottom wrap">'.$formHTML.'</div>';
}

add_shortcode( 'newsletterform', 'vg_newsletter_shortcode' );

?>