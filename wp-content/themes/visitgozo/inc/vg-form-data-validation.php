<?php


add_filter('frm_validate_field_entry', 'vg_custom_validation', 10, 3);
function vg_custom_validation($errors, $posted_field, $posted_value){
  if ( $posted_field->id == 74 && $posted_value != '' ){ // validate remarks field in data consolidation form (max 250 chars)
    $count = strlen($posted_value);
    if($count > 250)
    $errors['field'. $posted_field->id] = 'Remarks should be no longer than 250 characters.';
  }

  if ( $posted_field->id == 75 && $posted_value != '' ){ // validate description field in data consolidation form (min 200 max 800 chars)
    $count = strlen($posted_value);
    
    if($count < 200)
	$errors['field'. $posted_field->id] = 'Description should not be less than 200 characters.';
			
    if($count > 800)
      $errors['field'. $posted_field->id] = 'Remarks should be no longer than 800 characters.';
  }

  return $errors;
}