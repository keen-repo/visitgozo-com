<?php

function vg_get_my_gozo_entries() {

	if (is_user_logged_in()) {

		$my_gozo_listings = get_user_meta(get_current_user_id(), '_my_gozo', true);

		if (!is_array($my_gozo_listings)) {
			$my_gozo_listings = array();
		}

	} else {

		$my_gozo_listings = array();

		if (isset($_COOKIE['vg_my_gozo'])) {
			$my_gozo_listings = explode(',', $_COOKIE['vg_my_gozo']);
		}
	}

	foreach ($my_gozo_listings as $k=>$id) {
		$my_gozo_listings[$k] = (int)$id;
	}

	return $my_gozo_listings;

}

function vg_insert_my_gozo_entry($post_id) {


	if (is_user_logged_in()) {

		$my_gozo_listings = get_user_meta(get_current_user_id(), '_my_gozo', true);

		if (!is_array($my_gozo_listings)) {
			$my_gozo_listings = array();
		}

		$my_gozo_listings[] = $post_id;

		update_user_meta(get_current_user_id(), '_my_gozo', $my_gozo_listings);

	} else {

		$my_gozo_listings = array();

		if (isset($_COOKIE['vg_my_gozo'])) {
			$my_gozo_listings = explode(',', $_COOKIE['vg_my_gozo']);
		}

		$my_gozo_listings[] = $post_id;

		setcookie('vg_my_gozo', implode(',', $my_gozo_listings), 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );


	}

}


function vg_delete_my_gozo_entry($post_id) {

	if (is_user_logged_in()) {

		$my_gozo_listings = get_user_meta(get_current_user_id(), '_my_gozo', true);

		if (!is_array($my_gozo_listings)) {
			$my_gozo_listings = array();
		}

		foreach ($my_gozo_listings as $k=>$id) {

			if ($id == $post_id) {
				unset($my_gozo_listings[$k]);
			}

		}

		$my_gozo_listings = array_values($my_gozo_listings);

		update_user_meta(get_current_user_id(), '_my_gozo', $my_gozo_listings);

	} else {


		$my_gozo_listings = array();

		if (isset($_COOKIE['vg_my_gozo'])) {
			$my_gozo_listings = explode(',', $_COOKIE['vg_my_gozo']);
		}

		foreach ($my_gozo_listings as $k=>$id) {

			if ($id == $post_id) {
				unset($my_gozo_listings[$k]);
			}

		}

		$my_gozo_listings = array_values($my_gozo_listings);

		setcookie('vg_my_gozo', implode(',', $my_gozo_listings), 30 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );

	}

}

?>