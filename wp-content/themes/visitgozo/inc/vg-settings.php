<?php
add_filter( 'mb_settings_pages', 'vg_setting_pages' );

function vg_setting_pages( $settings_pages ){
	$settings_pages[] = array(
		'id'            => 'vg-settings',
		'option_name'   => 'vg_settings',
		'menu_title'    => __( 'Common', 'vg' ),
		'icon_url'      => 'dashicons-welcome-widgets-menus',
		'submenu_title' => __( 'Common Sections', 'vg' ),
	);
	return $settings_pages;
}

add_filter( 'rwmb_meta_boxes', 'vg_common_sections_settings' );

function vg_common_sections_settings( $meta_boxes ){
    
    $pagesList = vg_get_pages();

	$meta_boxes[] = array(
		'id'             => 'also-check-out',
		'title'          => __( 'Also Check Out Section', 'vg' ),
		'settings_pages' => 'vg-settings',
		'fields' => array(
            array(
                'name'  => __( 'Check Out Sub Pages', 'vg' ),
                'id'    => $prefix . 'check-out-subpages',
                'type'  => 'select_advanced',
                'options' => $pagesList,
                'multiple' => true
            ),
        )
	);

	return $meta_boxes;
}