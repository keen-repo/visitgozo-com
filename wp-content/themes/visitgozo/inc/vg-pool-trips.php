<?php

// Register Custom Post Type
function vg_pool_trips() {

	$labels = array(
		'name'                  => _x( 'Trips', 'Post Type General Name', 'vg' ),
		'singular_name'         => _x( 'Trip', 'Post Type Singular Name', 'vg' ),
		'menu_name'             => __( 'Trips', 'vg' ),
		'name_admin_bar'        => __( 'Trips', 'vg' ),
		'archives'              => __( 'Trip Archives', 'vg' ),
		'parent_item_colon'     => __( 'Parent Trip:', 'vg' ),
		'all_items'             => __( 'All Trips', 'vg' ),
		'add_new_item'          => __( 'Add New Trip', 'vg' ),
		'add_new'               => __( 'Add New', 'vg' ),
		'new_item'              => __( 'New Trip', 'vg' ),
		'edit_item'             => __( 'Edit Trip', 'vg' ),
		'update_item'           => __( 'Update Trip', 'vg' ),
		'view_item'             => __( 'View Trip', 'vg' ),
		'search_items'          => __( 'Search Trip', 'vg' ),
		'not_found'             => __( 'Not found', 'vg' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'vg' ),
		'featured_image'        => __( 'Featured Image', 'vg' ),
		'set_featured_image'    => __( 'Set featured image', 'vg' ),
		'remove_featured_image' => __( 'Remove featured image', 'vg' ),
		'use_featured_image'    => __( 'Use as featured image', 'vg' ),
		'insert_into_item'      => __( 'Insert into Trip', 'vg' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Trip', 'vg' ),
		'items_list'            => __( 'Trips list', 'vg' ),
		'items_list_navigation' => __( 'Trips list navigation', 'vg' ),
		'filter_items_list'     => __( 'Filter Trips list', 'vg' ),
	);
	$args = array(
		'label'                 => __( 'Trip', 'vg' ),
		'description'           => __( 'visitgozo.com trip manager', 'vg' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-share-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'vg_pool_trips', $args );

}
add_action( 'init', 'vg_pool_trips', 0 );