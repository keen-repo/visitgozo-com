<?php

// Register Listings Custom Taxonomy
function vg_listings_cats() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'vg' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'vg' ),
		'menu_name'                  => __( 'Category', 'vg' ),
		'all_items'                  => __( 'All Items', 'vg' ),
		'parent_item'                => __( 'Parent Item', 'vg' ),
		'parent_item_colon'          => __( 'Parent Item:', 'vg' ),
		'new_item_name'              => __( 'New Item Name', 'vg' ),
		'add_new_item'               => __( 'Add New Item', 'vg' ),
		'edit_item'                  => __( 'Edit Item', 'vg' ),
		'update_item'                => __( 'Update Item', 'vg' ),
		'view_item'                  => __( 'View Item', 'vg' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'vg' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'vg' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'vg' ),
		'popular_items'              => __( 'Popular Items', 'vg' ),
		'search_items'               => __( 'Search Items', 'vg' ),
		'not_found'                  => __( 'Not Found', 'vg' ),
		'no_terms'                   => __( 'No items', 'vg' ),
		'items_list'                 => __( 'Items list', 'vg' ),
		'items_list_navigation'      => __( 'Items list navigation', 'vg' ),
	);
	$rewrite = array(
		'slug'                       => 'listing-category',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'					 => false,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'vg_listings_cats', array( 'vg_listings', 'page' ), $args );

}
add_action( 'init', 'vg_listings_cats', 0 );

// Register Locality Custom Taxonomy
function vg_locality_cats() {

	$labels = array(
		'name'                       => _x( 'Localities', 'Taxonomy General Name', 'vg' ),
		'singular_name'              => _x( 'Locality', 'Taxonomy Singular Name', 'vg' ),
		'menu_name'                  => __( 'Locality', 'vg' ),
		'all_items'                  => __( 'All Items', 'vg' ),
		'parent_item'                => __( 'Parent Item', 'vg' ),
		'parent_item_colon'          => __( 'Parent Item:', 'vg' ),
		'new_item_name'              => __( 'New Item Name', 'vg' ),
		'add_new_item'               => __( 'Add New Item', 'vg' ),
		'edit_item'                  => __( 'Edit Item', 'vg' ),
		'update_item'                => __( 'Update Item', 'vg' ),
		'view_item'                  => __( 'View Item', 'vg' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'vg' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'vg' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'vg' ),
		'popular_items'              => __( 'Popular Items', 'vg' ),
		'search_items'               => __( 'Search Items', 'vg' ),
		'not_found'                  => __( 'Not Found', 'vg' ),
		'no_terms'                   => __( 'No items', 'vg' ),
		'items_list'                 => __( 'Items list', 'vg' ),
		'items_list_navigation'      => __( 'Items list navigation', 'vg' ),
	);
	$rewrite = array(
		'slug'                       => 'listing-locality',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'					 => false,
		'rewrite'                    => $rewrite,
	);

	register_taxonomy( 'vg_locality_cats', array( 'vg_listings', 'vg_events', 'page' ), $args );


}
add_action( 'init', 'vg_locality_cats', 0 );



// Register Custom Taxonomy
function vg_events_cats() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'vg' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'vg' ),
		'menu_name'                  => __( 'Categories', 'vg' ),
		'all_items'                  => __( 'All Categories', 'vg' ),
		'parent_item'                => __( 'Parent Item', 'vg' ),
		'parent_item_colon'          => __( 'Parent Item:', 'vg' ),
		'new_item_name'              => __( 'New Item Name', 'vg' ),
		'add_new_item'               => __( 'Add New Category', 'vg' ),
		'edit_item'                  => __( 'Edit Category', 'vg' ),
		'update_item'                => __( 'Update Category', 'vg' ),
		'view_item'                  => __( 'View Category', 'vg' ),
		'separate_items_with_commas' => __( 'Separate Categories with commas', 'vg' ),
		'add_or_remove_items'        => __( 'Add or remove Categories', 'vg' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'vg' ),
		'popular_items'              => __( 'Popular Categories', 'vg' ),
		'search_items'               => __( 'Search Categories', 'vg' ),
		'not_found'                  => __( 'Not Found', 'vg' ),
		'no_terms'                   => __( 'No Categories', 'vg' ),
		'items_list'                 => __( 'Categories list', 'vg' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'vg' ),
	);

	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);

	register_taxonomy( 'vg_events_cats', array( 'vg_events' ), $args );

}

add_action( 'init', 'vg_events_cats', 0 );



// Register Search Tag Custom Taxonomy
function vg_search_tags() {

	$labels = array(
		'name'                       => _x( 'Search Tags', 'Taxonomy General Name', 'vg' ),
		'singular_name'              => _x( 'Search Tag', 'Taxonomy Singular Name', 'vg' ),
		'menu_name'                  => __( 'Search Tag', 'vg' ),
		'all_items'                  => __( 'All Items', 'vg' ),
		'parent_item'                => __( 'Parent Item', 'vg' ),
		'parent_item_colon'          => __( 'Parent Item:', 'vg' ),
		'new_item_name'              => __( 'New Item Name', 'vg' ),
		'add_new_item'               => __( 'Add New Item', 'vg' ),
		'edit_item'                  => __( 'Edit Item', 'vg' ),
		'update_item'                => __( 'Update Item', 'vg' ),
		'view_item'                  => __( 'View Item', 'vg' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'vg' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'vg' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'vg' ),
		'popular_items'              => __( 'Popular Items', 'vg' ),
		'search_items'               => __( 'Search Items', 'vg' ),
		'not_found'                  => __( 'Not Found', 'vg' ),
		'no_terms'                   => __( 'No items', 'vg' ),
		'items_list'                 => __( 'Items list', 'vg' ),
		'items_list_navigation'      => __( 'Items list navigation', 'vg' ),
	);

	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'query_var'					 => false,
	);

	register_taxonomy( 'vg_search_tags', array( 'page' ), $args );


}
add_action( 'init', 'vg_search_tags', 0 );

?>