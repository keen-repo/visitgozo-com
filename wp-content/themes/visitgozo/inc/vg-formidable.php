<?php

/* Generate random number for pool trip submission titles */

add_filter('frm_get_default_value', 'vg_random_value_for_trip_title', 10, 2);
function vg_random_value_for_trip_title($new_value, $field){
  if($field->id == 149){ 
    $new_value = mt_rand();
  }
  return $new_value;
}

/* Block past dates on pool trip calendar */

add_action('frm_date_field_js', 'block_past_dates');
function block_past_dates($field_id){
  if($field_id == 'field_1yzfn'){ 
    echo ',minDate:0,maxDate:+90';
  }
}

/* Convert normal date to timestamp for post compatability */

add_filter( 'frm_new_post', 'vg_convert_trip_date_to_timestamp', 10, 2 );
function vg_convert_trip_date_to_timestamp( $post, $args ) {
  $field_id = 143; // change 25 to the id of the field to draw from
  if ( isset( $_POST['item_meta'][ $field_id ] ) ) {
    $field_value = strtotime( $_POST['item_meta'][ $field_id ] );
    $post['post_custom']['_vg_date'] = $field_value;
  }
  return $post;
}


add_filter('frm_validate_field_entry', 'vg_custom_validation', 10, 3);
function vg_custom_validation($errors, $posted_field, $posted_value){
  if ( $posted_field->id == 85 && $posted_value != '' ){ // validate remarks field in data consolidation form (max 250 chars)
    $count = strlen($posted_value);
    if($count > 250)
    $errors['field'. $posted_field->id] = 'Remarks should be no longer than 250 characters.';
  }

  /*
  if (in_array($posted_field->id, array(86)) && $posted_value != '' ){ // validate description field in data consolidation form (min 200 max 1200 chars)
    $count = strlen($posted_value);

    if($count < 200)
	     $errors['field'. $posted_field->id] = 'Description should not be less than 200 characters. Total Used: ' . $count;

			
    if($count > 1200)
      $errors['field'. $posted_field->id] = 'Remarks should be no longer than 1200 characters. Total Used: ' . $count;
  }
  */


  /* Event Gallery Upload - Limit to 0 or 3 uploads. */
  if ($posted_field->id == 115 || $posted_field->id == 92) {

    if (is_array($posted_value)) {

      $total_uploads = 0;

      foreach ($posted_value as $v) {
        if (!empty($v)) $total_uploads++;
      }

      if (in_array($total_uploads, array(1,2))) {
        $errors['field'.$posted_field->id] = 'If you would like to provide gallery images, please note we accept a minimum of 3 images and a maximum of 9.';
      }

    }
    
  }

  return $errors;
}


add_filter('frm_setup_new_fields_vars', 'vg_frm_location_posts', 20, 2);
add_filter('frm_setup_edit_fields_vars', 'vg_frm_location_posts', 20, 2);

function vg_frm_location_posts($values, $field) {

    if($field->id == 77 || $field->id == 100) { /* Form Category */

        if ($field->id == 77) {
          $taxonomy = 'vg_listings_cats';
        }

        if ($field->id == 100) {
           $taxonomy = 'vg_events_cats';
        }

        $term_list = vg_build_term_tree($taxonomy);

        $values['options'] = $term_list;

        $values['use_key'] = true;

    }

    if($field->id == 80 || $field->id == 103) { /* Form Locality */

        $terms = get_terms('vg_locality_cats', array('hide_empty' => false));

        $values['options'] = array('' => 'Choose Location');

        foreach ($terms as $term) {
          $values['options'][$term->term_id] = $term->name;
        }

        $values['use_key'] = true;

    }

    return $values;
    
}

/* by default formidable does not allow viewing of uploaded media, in this case we need access to it */
function vg_filter_formidable_media() {

  if (is_admin()) {
    remove_action( 'pre_get_posts', 'FrmProFileField::filter_media_library', 99 );
  }

}

add_action('init', 'vg_filter_formidable_media');



add_action('frm_after_update_entry', 'vg_after_entry_updated', 10, 2);

function vg_after_entry_updated($entry_id, $form_id) {

  global $wpdb;

  $type = false;

  $entry = FrmEntry::getOne($entry_id, true);

  if ($form_id == 8) {
    $type = 'listing';
    $userID = $entry->metas[94];
  }

  if ($form_id == 9) {
    $type = 'event';
    $userID = $entry->metas[117];
  }

  /* different form */
  if (!$type) return;

  $wpdb->update( 
    'vg_listing_relationships', 
    array( 
      'status' => 1,
      'user_id' => $userID,
    ), 
    array( 'form_entry_id' => $entry_id ), 
    array( 
      '%d',
      '%d',
    ), 
    array( '%d' ) 
  );

}

add_action('frm_after_create_entry', 'vg_after_entry_created', 10, 2);

function vg_after_entry_created($entry_id, $form_id) {

  global $wpdb;

  $type = false;

  if ($form_id == 8) {
    $type = 'listing';
  }

  if ($form_id == 9) {
    $type = 'event';
  }

  /* different form */
  if (!$type) return;

  $wpdb->insert( 
    'vg_listing_relationships', 
    array( 
      'form_entry_id' => $entry_id,
      'user_id'     => get_current_user_id(),
      'status'    => 1,
      'type'      => $type,
    ), 
    array( 
      '%d',
      '%d',
      '%d',
      '%s'
    )
  );

}


/*
 *
 * In the below function we listen when a post is deleted. If the post's ID is found in the vg_listing_relationships table
 * we then try to delete the corresponding form entry, and then the row itself from the vg_listing_relationships table
 * We do this because we want to clean up any orphan rows related to posts. 
 * There is no reason to keep a reference to an item in the vg_listing_relationships table as it will cause bugs.
 * There is no point keeping the form entry either as this will cause confusion.
 *
 * We also delete all reference to this post in vg_event_dates because we do not want this item to continue to be queried on the frontend
 *
*/
add_action( 'delete_post', 'vg_delete_post', 10 );

function vg_delete_post( $pid ) {

  global $wpdb;

  $sql = $wpdb->prepare('SELECT * FROM vg_listing_relationships WHERE listing_id = %d', $pid);

  $rows = $wpdb->get_results($sql);

  foreach ($rows as $row) {

    if ($row->form_entry_id) {

        $result = FrmEntry::destroy( $row->form_entry_id );

        if ($result === false || $result === 0) {
            /* no entry to delete, why? */
        } else {
            $wpdb->delete( 'vg_listing_relationships', array( 'id' => $row->id ) );
        }

    }

  }

  /* Delete Event Dates - so it cant be queried in the frontend */
  $wpdb->delete( 'vg_event_dates', array( 'post_id' => $pid ) );

}

/*
 *
 * In the below function we listen for when a post is untrashed. If it is untrashed, we then check if the post has a connection with the
 * vg_listing_relationships table. If it does we proceed to change the status from 2 (trashed) to 0 (static)
 * We do this so that the listing will start to show again in the users list of listings.
 *
*/
add_action('untrashed_post', 'vg_untrashed_post', 10, 1);

function vg_untrashed_post($pid) {

  global $wpdb;

  $sql = $wpdb->prepare('SELECT * FROM vg_listing_relationships WHERE listing_id = %d AND status = %d', $pid, 2);

  $rows = $wpdb->get_results($sql);

  foreach ($rows as $row) {

    $wpdb->update( 
      'vg_listing_relationships', 
      array( 
        'status' => 0
      ), 
      array( 'id' => $row->id ), 
      array( 
        '%d'
      ), 
      array( '%d' ) 
    );

  }


}


add_filter('frm_replace_shortcodes', 'vg_frm_map_html', 10, 3);

function vg_frm_map_html($html, $field, $args) {

    if ($field['id'] == 160 || $field['id'] == 161) {

      $html = array('<div id="frm_field_' . $field['id'] . '_container">');

        $html[] = '<label class="frm_primary_label">Location <span class="frm_required">*</span></label>';
        $html[] = '<div class="map-wrap" id="map"></div>';

        $html[] = '<script>';
          $html[] = 'function initMap() {';

            if ($field['id'] == 160) {
              $html[] = 'var field_latitude = "field_latitude"';
              $html[] = 'var field_longitude = "field_longitude"';
            }

            if ($field['id'] == 161) {
              $html[] = 'var field_latitude = "field_latitude2"';
              $html[] = 'var field_longitude = "field_longitude2"';
            }

            $html[] = 'var lat = parseFloat(jQuery("#" + field_latitude).val()) || 36.0382568;';
            $html[] = 'var lng = parseFloat(jQuery("#" + field_longitude).val()) || 14.2570478;';
            $html[] = 'var myLatLng = {lat: lat, lng : lng};';
            $html[] = 'var map = new google.maps.Map(document.getElementById("map"), {';
              $html[] = 'zoom: 17,';
              $html[] = 'center: myLatLng,';
              $html[] = 'scrollwheel: false,';
            $html[] = '});';
            $html[] = 'var marker = new google.maps.Marker({';
              $html[] = 'draggable: true,';
              $html[] = 'position: myLatLng,';
              $html[] = 'map: map,';
              $html[] = 'title : "Your Location",';
            $html[] = '});';
            $html[] = 'google.maps.event.addListener(marker, "click", function (event) {';
              $html[] = 'document.getElementById(field_latitude).value = event.latLng.lat();';
              $html[] = 'document.getElementById(field_longitude).value = event.latLng.lng();';
            $html[] = '});';
            $html[] = 'google.maps.event.addListener(marker, "dragend", function (event) {';
              $html[] = 'document.getElementById(field_latitude).value = this.getPosition().lat();';
              $html[] = 'document.getElementById(field_longitude).value = this.getPosition().lng();';
            $html[] = '});';
          $html[] = '}';
          
        $html[] = '</script>';
        $html[] = '<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhEob6nACitX-00PCy-CU_4iZdKfJB5zE&callback=initMap"></script>';

      $html[] = '</div>';

      $html = implode(PHP_EOL, $html);

    }

    return $html;

}

add_filter('frm_validate_field_entry', 'vg_validate_listing_field_entries', 10, 3);
function vg_validate_listing_field_entries($errors, $posted_field, $posted_value){

    /* event fields */
   if ($posted_field->id == 118 || $posted_field->id == 119) {

      if (empty($posted_value)) {
        $errors['field161'] = 'Please select a listing location.';
      }

   }

   /* listing fields */
   if ($posted_field->id == 95 || $posted_field->id == 96) {

      if (empty($posted_value)) {
        $errors['field160'] = 'Please select a listing location.';
      }
      
   }

  return $errors;
}