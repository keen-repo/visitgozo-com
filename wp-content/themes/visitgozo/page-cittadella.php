<?php
/**
 * Template Name: Cittadella Landing
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area cittadella-landing">
		<main id="main" class="site-main" role="main">

            <div class="cit-menu">
                <div class="relative">
                    <span class="toggle-menu active"></span>
                    <ul class="active">
                        <li><a href="#about">About</a></li>
                        <li><a href="#history">History</a></li>
                        <li><a href="#landmarks">Landmarks</a></li>
                        <li><a href="#panos">360&deg; Photos</a></li>
                        <li><a href="#webcams">Webcams</a></li>
                        <li><a href="#event-gallery">Event Gallery</a></li>
                        <li><a href="#social">Pinterest</a></li>
                        <li><a href="#social">Blog</a></li>
                        <li><a href="#contact">Contact Us</a></li>
                    </ul>
                    <img class="cit-logo" src="<?php bloginfo('template_url'); ?>/images/logo-cittadella.svg" alt="Cittadella" />
                </div>
            </div>
            <?php /*
            <a class="vote-button" href="http://ec.europa.eu/regional_policy/en/regio-stars-awards/finalists_2018/cat5_fin2" target="_blank">
                <img src="<?php bloginfo('template_url'); ?>/images/icon-vote.svg" alt="Vote Now!" />
                VOTE<br/>NOW
            </a>
            */ ?>

			<div class="video-intro">
                <div class="video-container">
                <script src="https://fast.wistia.com/embed/medias/mo9nl6bnah.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:47.29% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_mo9nl6bnah videoFoam=true" style="height:100%;position:relative;width:100%">&nbsp;</div></div></div>
                </div>
            </div>

			<div class="section section-about blue-bg" id="about">
				<div class="wrap row">
					<div class="col col-30">
						<h2 class="section-title"><?php echo rwmb_meta( $prefix.'about-section-title' ); ?></h2>
						<strong><?php echo rwmb_meta( $prefix.'about-section-subtitle' ); ?></strong>
					</div>
					<div class="col col-70">
                        <?php echo rwmb_meta( $prefix.'about-section-text' ); ?>
					</div>
				</div>
			</div>

			<div class="section section-about-2">
				<div class="wrap row">
					<div class="image col col-50">
                        <?php
                            $images = rwmb_meta( $prefix . 'about-section-image', array( 'size' => 'large' ) );
                            foreach ( $images as $image ) {
                                echo '<img src="', $image['url'], '">';
                            }
                        ?>
                    </div>
					<div class="text col col-50">
						<?php echo apply_filters('the_content', rwmb_meta( $prefix.'about-2-section-text' )); ?>
					</div>
				</div>
				<div class="wrap text-center section">
					<a href="<?php echo rwmb_meta( $prefix.'about-2-button-url' ); ?>" target="_blank" class="cit-button"><?php echo rwmb_meta( $prefix.'about-2-button-text' ); ?></a>
				</div>
			</div>

			<div class="section-background-image">
                <?php
                    $images = rwmb_meta( $prefix . 'section-bg-image', array( 'size' => 'full' ) );
                    foreach ( $images as $image ) {
                        echo '<img src="', $image['url'], '">';
                    }
                ?>
            </div>

			<div class="section section-project">
				<div class="wrap row">
					<div class="col col-50">
                        <?php echo apply_filters('the_content', rwmb_meta( $prefix.'project-text' )); ?>
					</div>
					<div class="col col-50 images">
                        <?php
                            $images = rwmb_meta( $prefix . 'project-images', array( 'size' => 'large' ) );
                            foreach ( $images as $image ) {
                                echo '<div><img src="', $image['url'], '"></div>';
                            }
                        ?>
					</div>
				</div>
			</div>

			<div class="section section-history blue-bg" id="history">
				<div class="wrap row">
					<div class="col col-30">
						<h2 class="section-title"><?php echo rwmb_meta( $prefix.'history-section-title' ); ?></h2>
						<strong><?php echo rwmb_meta( $prefix.'history-section-subtitle' ); ?></strong>
					</div>
					<div class="col col-70">
                        <?php echo rwmb_meta( $prefix.'history-section-text' ); ?>
					</div>
				</div>
			</div>

			<div class="section section-history-2">
				<div class="wrap row">
					<div class="image col col-50">
                        <?php
                            $images = rwmb_meta( $prefix . 'history-section-image', array( 'size' => 'large' ) );
                            foreach ( $images as $image ) {
                                echo '<img src="', $image['url'], '">';
                            }
                        ?>
                    </div>
					<div class="text col col-50">
                        <?php echo rwmb_meta( $prefix.'history-2-section-text' ); ?>
					</div>
				</div>
			</div>
            <?php /*
			<div class="section section-vote">
				<div class="wrap text-center">
                    <?php echo rwmb_meta( $prefix.'vote-text' ); ?>
                    <a href="<?php echo rwmb_meta( $prefix.'vote-button-url' ); ?>" target="_blank" class="cit-button"><?php echo rwmb_meta( $prefix.'vote-button-text' ); ?></a>
				</div>
            </div>
            */ ?>

			<div class="section-map" id="landmarks">
				<div class="container row">
					<div class="col col-40 map-image">
                        <?php
                            $images = rwmb_meta( $prefix . 'map-image', array( 'size' => 'large' ) );
                            foreach ( $images as $image ) {
                                echo '<img src="', $image['url'], '">';
                            }
                        ?>
                    </div>
                    <?php
                        $bgimage = rwmb_meta( $prefix . 'map-bg-image', array( 'size' => 'large' ) );
                        $image = reset($bgimage);
                    ?>
                    <div class="col col-60 map-text" style="background-image: url('<?php echo $image['url']; ?>');">
						<h2 class="section-title"><?php echo rwmb_meta( $prefix.'map-section-title' ); ?></h2>
						<a href="<?php echo rwmb_meta( $prefix.'map-button-url' ); ?>" target="_blank" class="cit-button dark"><?php echo rwmb_meta( $prefix.'map-button-text' ); ?></a>
					</div>
				</div>
			</div>

			<div class="section section-360" id="panos">
				<div class="wrap">
                    <h2 class="section-title text-center"><?php echo rwmb_meta( $prefix.'360-section-title' ); ?></h2>
                    <div class="pad-top-60"></div>
                    <?php $slides = array(
                        array(
                            'id' => 'battery',
                            'thumb' => 'https://www.visitgozo.com/wp-content/uploads/2018/08/battery.jpg',
                        ),
                        array(
                            'id' => 'cathedral',
                            'thumb' => 'https://www.visitgozo.com/wp-content/uploads/2018/08/cathedral.jpg',
                        ),
                        array(
                            'id' => 'ditch',
                            'thumb' => 'https://www.visitgozo.com/wp-content/uploads/2018/08/ditch.jpg',
                        ),
                        array(
                            'id' => 'sagristy',
                            'thumb' => 'https://www.visitgozo.com/wp-content/uploads/2018/08/sagristy.jpg',
                        ),
                        array(
                            'id' => 'stjohns',
                            'thumb' => 'https://www.visitgozo.com/wp-content/uploads/2018/08/stjohns.jpg',
                        )
                    );
                    ?>
                    <div class="slider-360">
                        <div class="page-gallery-slider">
                            <?php foreach( $slides as $slide ){ ?>
                                <a class="slide" href="#pano-<?php echo $slide['id']; ?>">
                                    <div class="relative">
                                        <div class="overlay">
                                            <img src="<?php bloginfo('template_url'); ?>/images/icon-360.png" alt="" />
                                        </div>
                                        <img src="<?php echo $slide['thumb']; ?>" alt="" />
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php foreach( $slides as $slide ){ ?>
                        <div class="remodal" data-remodal-id="pano-<?php echo $slide['id']; ?>">
                            <button data-remodal-action="close" class="remodal-close"></button>
                            <div class="pano-wrap">
                                <div class="pano-container">
                                    <iframe src="<?php bloginfo('template_url'); ?>/inc/panos/loader.php?pano=<?php echo $slide['id']; ?>" frameborder="0"></iframe>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
				</div>
			</div>

			<div class="section section-webcams blue-bg" id="webcams">
				<div class="wrap">
                    <h2 class="section-title text-center"><?php echo rwmb_meta( $prefix.'webcam-section-title' ); ?></h2>
                    <div class="cam-tabs">
                        <ul class="tabs">
                            <li data-cam="1" class="active">
                                <span>Camera 1</span>
                                Gozo Cathedral and Victoria
                            </li>
                            <li data-cam="2">
                                <span>Camera 2</span>
                                Gozo Ċittadella
                            </li>
                        </ul>
                    </div>
                    <div class="cam-embeds">
                        <div class="embed active" data-cam="1">
                            <script src="https://live.streamdays.com/hfd56wj2" type="text/javascript" language="javascript"></script>
                        </div>
                        <div class="embed active" data-cam="2">
                            <script src="https://live.streamdays.com/4j3i8pju" type="text/javascript" language="javascript"></script>
                        </div>
                    </div>
				</div>
			</div>

			<div class="section section-events-gallery" id="event-gallery">
				<div class="wrap">
                    <h2 class="section-title text-center"><?php echo rwmb_meta( $prefix.'gallery-section-title' ); ?></h2>
                    <div class="pad-top-60"></div>
                    <div class="slider-gallery">
                        <?php $galleries = rwmb_meta( 'cittadella-galleries' ); ?>
                        <div class="page-gallery-slider">
                            <?php foreach($galleries as $i=>$gallery){ ?>
                                <a class="slide" href="#gal-<?php echo $i; ?>" data-gallery="<?php echo $i; ?>">
                                    <div class="relative">
                                        <?php echo wp_get_attachment_image($gallery[$prefix.'gallery-images'][0], 'large', false); ?>
                                        <div class="overlay"><?php echo $gallery[$prefix.'gallery-title']; ?></div>
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php foreach( $galleries as $i=>$gallery ){ ?>
                        <div class="remodal gallery-modal" data-remodal-id="gal-<?php echo $i; ?>">
                            <button data-remodal-action="close" class="remodal-close"></button>
                            <div class="event-gallery-slides">
                                <?php foreach( $gallery[$prefix.'gallery-images'] as $image ){ ?>
                                    <div class="slide text-center">
                                        <?php echo wp_get_attachment_image( $image, 'large', false ); ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
				</div>
			</div>

			<div class="section section-book">
				<div class="wrap text-center">
                    <div class="text">
                        <?php echo rwmb_meta( $prefix.'book-section-text' ); ?>
					</div>
					<a href="<?php echo rwmb_meta( $prefix.'book-button-url' ); ?>" target="_blank" class="cit-button"><?php echo rwmb_meta( $prefix.'book-button-text' ); ?></a>
				</div>
			</div>

			<div class="social-blog" id="social">
				<div class="wrap row section">
					<div class="col col-50 social">
                        <h2 class="section-title">Pinterest</h2>
                        <div class="pad-top-60"></div>
						<div class="embed-wrap">
                            <a data-pin-do="embedBoard" data-pin-board-width="450" data-pin-scale-height="300" data-pin-scale-width="80" href="https://www.pinterest.com/visitgozo0636/cittadella-the-fortified-city-in-gozo/"></a>
                        </div>
					</div>
					<div class="col col-50 blog">
                        <h2 class="section-title">Blog</h2>
                        <div class="pad-top-60"></div>
                        <div class="articles">
                        <?php 
                            $blogPosts = get_posts(array(
                                'numberposts' => 2,
                                'category' => 431
                            ));

                            foreach($blogPosts as $item){ ?>
                                <div class="article">
                                    <div class="title"><?php echo $item->post_title; ?></div>
                                    <div class="text"><?php echo mb_substr( strip_tags($item->post_content), 0, 100); ?>...</div>
                                    <div class="link"><a href="<?php echo get_permalink($item->ID); ?>">READ MORE</a></div>
                                </div>
                            <?php }
                        ?>
                        </div>
                        <a href="/blog/category/cittadella/" class="cit-button dark">VIEW ALL</a>
					</div>
				</div>
			</div>

			<div class="section section-contact" id="contact">
				<div class="wrap">
                    <h2 class="section-title text-center">Contact</h2>
                    <div class="pad-top-60"></div>
                    <div class="row">
                        <div class="">
                            <?php echo nl2br( rwmb_meta( $prefix.'contact-address-text' ) ); ?>
                            <br/><br/>
                            <a href="https://www.facebook.com/cittadellagozo/" target="_blank">
                                <span class="icon-facebook"></span> <strong>Find us on Facebook</strong>
                            </a>
                        </div>
                        <div class="">
                            <?php echo nl2br( rwmb_meta( $prefix.'contact-phone-text' ) ); ?>
                        </div>
                        <div class="text-right">
                            <img src="<?php bloginfo('template_url'); ?>/images/logo-ministry.jpg" alt="">
                        </div>
                    </div>
                    <div class="pad-top-60"></div>
				</div>
			</div>

			<div class="section-ta">
				<div class="wrap">
                    <div class="ta-wrap none">
                        <div id="TA_selfserveprop106" class="TA_selfserveprop">
                        <ul id="B7OSLg1gK" class="TA_links Qj5n8KUXQ">
                        <li id="9U6nTJEll" class="5YvttpI">
                        <a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
                        </li>
                        </ul>
                        </div>
                        <script src="https://www.jscache.com/wejs?wtype=selfserveprop&uniq=106&locationId=1237784&lang=en_US&rating=true&nreviews=4&writereviewlink=true&popIdx=true&iswide=true&border=false&display_version=2"></script>
                    </div>
                </div>
                <div class="pad-top-60"></div>
			</div>

		</main><!-- #main -->
    </div><!-- #primary -->
    
    <script>
        jQuery('.cam-tabs li').click(function(){
            var cam = jQuery(this).data('cam');
            console.log(cam);

            jQuery('.cam-tabs li').removeClass('active');
            jQuery(this).addClass('active');

            jQuery('.cam-embeds .embed').removeClass('active');
            jQuery('.cam-embeds .embed[data-cam="'+cam+'"]').addClass('active');
        });

        // nasty hack to hide embed as cam provider 
        // was not loading embed if container was hidden
        setTimeout(function(){ jQuery('.cam-embeds .embed[data-cam="2"]').removeClass('active'); }, 3000);

        jQuery(document).on('opened', '.gallery-modal', function () {
            jQuery(this).find('.event-gallery-slides').slick();
        });

        // handle menu cancel button
        jQuery('.cit-menu .toggle-menu').click(function(){
            jQuery('.cit-menu ul').toggleClass('active');
            jQuery(this).toggleClass('active');
        });

    </script>

    <script async defer src="//assets.pinterest.com/js/pinit.js"></script>

<?php get_footer(); ?>
