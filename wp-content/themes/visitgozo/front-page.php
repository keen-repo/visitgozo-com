<?php
/**
 * The template for displaying homepage.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>

	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">
			<?php 
				/* UPCOMING EVENTS SECTION */
				vg_render_upcoming_events(6);
			?>
			<?php 
				/* FEATURED PAGES SECTION */
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

	<div class="section section-featured-pages">
		<div class="featured-nav txt-aligncenter">
			<span class="nav-item clickable active"><?php _e( 'What to do', 'vg-front' ); ?></span>
			<span class="nav-item clickable"><?php _e( 'Where to stay', 'vg-front' ); ?></span>
			<span class="nav-item clickable"><?php _e( 'Where to go', 'vg-front' ); ?></span>
		</div>
		<div class="featured-pages-wrap">
			<div class="leader-slides">
				<?php $sections = array( 'wtd', 'wts', 'wtg' );
				foreach( $sections as $section ){
					$sectionLeader = rwmb_meta( $prefix . $section . '-featured-leader', array(), icl_object_id(get_the_ID(), 'page', false, 'en') );

					$featuredID = get_post_thumbnail_id( $sectionLeader );
					if( !empty($featuredID) ){
						$featuredArr = wp_get_attachment_image_src( $featuredID, 'page-banner', true );
						$featuredImage = $featuredArr[0];
					} else {
						$featuredImage = '';
					} ?>
					<div class="leader bg-cover oflow" style="background-image: url('<?php echo $featuredImage; ?>')">
						<div class="wrap relative">
							<a class="leader-intro" href="<?php echo get_permalink(icl_object_id( $sectionLeader, 'page', false )); ?>">
								<h3><?php echo get_the_title( icl_object_id( $sectionLeader, 'page', false ) ); ?></h3>
								<p class="leader-text"><?php echo rwmb_meta( $prefix . 'page-intro-text', array(), icl_object_id( $sectionLeader, 'page', false ) ); ?></p>
								<span class="read-more icon-right-circle"></span>
							</a>
						</div>
					</div>
				<?php } ?>
			</div>
			<div class="featured-pages oflow wrap">
				<?php 
					foreach( $sections as $i=>$section ){ ?>
						<div class="featured-group slides-wrap oflow <?php if($i==0) echo 'active'; ?>">
							<div class="wrap oflow"><?php
								$sectionPages = [];

								for( $i=1; $i<=6; $i++ ){
									$sectionPages[] = rwmb_meta( $prefix . $section . '-featured-page-' . $i, array(), icl_object_id(get_the_ID(), 'page', false, 'en') );
								}

								$count = 0;
								foreach( $sectionPages as $sPageID ){
									$featuredID = get_post_thumbnail_id( $sPageID );
									if( !empty($featuredID) ){
										$featuredArr = wp_get_attachment_image_src( $featuredID, 'subpage-featured', true );
										$featuredImage = $featuredArr[0];
									} else {
										$featuredImage = '';
									} 

									if($count <= 1){
										$colClass = 'col-50';
									} else {
										$colClass = 'col-25';
									}

									?>
									<a class="col slide relative oflow fl relative <?php echo $colClass; ?>" href="<?php echo get_permalink(icl_object_id( $sPageID, 'page', false )); ?>">
										<div class="slide-image bg-cover fullwidth-fullheight" style="background-image: url('<?php echo $featuredImage; ?>')"></div>
										<div class="slide-title valign-middle"><?php echo get_the_title( icl_object_id( $sPageID, 'page', false ) ); ?></div>
										<?php if($colClass == 'col-50'){ ?>
											<div class="overlay fullwidth-fullheight">
												<h4><?php echo get_the_title( icl_object_id( $sPageID, 'page', false ) ); ?></h4>
												<?php echo rwmb_meta( $prefix . 'page-intro-text', array(), icl_object_id( $sPageID, 'page', false ) ); ?>
											</div>
										<?php } ?>
									</a>
								<?php $count++;
								} ?>
							</div>
							<div class="view-more oflow clear">
								<?php
									switch( $section ){
										case 'wtd': $href = 68; break;
										case 'wts': $href = 190; break;
										case 'wtg': $href = 212; break;
									}
								?>
								<a href="<?php echo get_permalink($href); ?>"><?php _e( 'View More', 'vg-front' ); ?></a>
							</div>
						</div>
					<?php }
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
