<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vg
 */

global $prefix;

get_header(); ?>
	
	<div id="primary" class="content-area wrap">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
	/* Child Pages */
	get_template_part('template-parts/content', 'child-pages');
	?>

	<?php

		$show_listing_widget = get_post_meta(icl_object_id(get_the_ID(), 'page', false, 'en'), $prefix . 'show_listing_widget', true);

		if ($show_listing_widget) { ?>
			<div class="section section-listings-filter section-padding-bottom wrap">
			<?php
				$shortcode_args = prepare_listing_widget_args(get_the_ID());
				echo do_shortcode($shortcode_args['html']);
			?>
			</div>
		<?php }
	?>

<?php get_footer(); ?>
