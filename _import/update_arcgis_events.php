<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );


$paged = 0;
if (isset($_GET['page'])) {
	$paged = $_GET['page'];
}

$posts = get_posts(array(
	'post_type' 		=> 'vg_events',
	'posts_per_page' 	=> 10,
  	'paged' 			=> $paged,
  	'suppress_filters' => false,
));

foreach ($posts as $post) {

	echo 'doing post: ' . $post->post_title . '<br />';
	

	delete_arcgis_event($post->ID);

	//update_arcgis_event($post);

}

$paged++;

echo 'refreshing...<meta http-equiv="refresh" content="3; url=https://www.visitgozo.com/_import/update_arcgis_events.php?page=' . $paged . '">';

?>