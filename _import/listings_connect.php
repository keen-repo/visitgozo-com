<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

global $wpdb;


if ( !function_exists('media_handle_upload') ) {
	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
	require_once(ABSPATH . "wp-admin" . '/includes/file.php');
	require_once(ABSPATH . "wp-admin" . '/includes/media.php');
}

function import_external_image($url, $post_id) {

	$url = str_replace(array('/large/','/Large/'), '/', $url);

	$tmp = download_url( $url );

	if( is_wp_error( $tmp ) ) {
		// download failed, handle error
		echo '<pre>';
		print_r($tmp);
		echo '</pre>';
	}

	$file_array = array();

	// Set variables for storage
	// fix file filename for query strings
	preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches);

	echo 'Matches:<pre>';
	print_r($matches);
	echo '</pre>';

	$file_array['name'] = basename($matches[0]);
	$file_array['tmp_name'] = $tmp;

	// If error storing temporarily, unlink
	if ( is_wp_error( $tmp ) ) {
		@unlink($file_array['tmp_name']);
		$file_array['tmp_name'] = '';
	}

	$id = media_handle_sideload($file_array, $post_id, '' );

	if ( is_wp_error($id) ) {
		@unlink($file_array['tmp_name']);
		return $id;
	}

	echo 'id:<pre>';
	print_r($id);
	echo '</pre>';

	return $id;

}


$listings_list = file_get_contents('listings.txt');

$listings = explode(PHP_EOL, $listings_list);

$counts = array(
	'many' => 0, 
	'single' => 0, 
	'none' => 0
);


$languages = array('en', 'es', 'de', 'ru', 'it', 'sv', 'fr');

function section_id_to_category_id($section_id) {

	$sections = array(
		202 => 47, //5star
		203 => 46, //4star
		204 => 45, //3star
		189 => 34, //health and wellbeing spas
		195 => 59, //towns and villages
		193 => 51, //archelogical sites
		198 => 55, //niches
		192 => 54, //museuems
		205 => 40, //aparthotels
		208 => 49, //villas
		206 => 43, //guesthouses
		210 => 48, //self catering apartments
		201 => 44, //hotels
		207 => 42, //farmhouses
		2178 => 41, //boutique accommodation
		185 => 31, //water sports
		179 => 30, //quad bike tours
		178 => 26, //eco tours
		546 => 24, //boat trips
		173 => 22, //abseiling /rock climbing
		1474 => 27, // gozo segway tours
		177 => 23, //adventure day tours
		132 => 176, // guided tours
		130 => 24, //boat trips
		1410 => 177,//trackless train
		111 => 176, // gozo 360
		120 => 182, //pharmacies
		200 => 56, // parks and gardens
		184 => 29, //kayaking
		172 => 178, //bike rentals
		171 => 178, // bike tours
		187 => 38, // wedding and honeymoons
		190 => 35, // learning english
		194 => 58, // siteseeing / places of interest
		174 => 28, // horse riding
		182 => 25, // diving centres
		183 => 25, // dive sites
		197 => 53, // churches and chapels
		196 => 52, // beaches
		199 => 57, // retreat houses
		127 => 180, // car hire
		5000 => 36, // local artisanal products
		191 => 37, // tastes of gozo
		140 => 15, // food and drink
		294 => 16, // bars
		292 => 17, // caffeteria
		291 => 19, // snack bars
		290 => 18, // restaurants
		126 => 181, // taxis
		188 => 33, // conferences and init.
	);

if (isset($sections[$section_id])) {
		return $sections[$section_id];
	}

	return '';

}

function google_locality_to_internal_id($locality) {

	$items = array(
		'Gharb' 			=> 167,
		'Ir-Rabat Għawdex' 	=> 163,
		'San Lawrenz' 		=> 169,
		'Il-Munxar' 		=> 160,
		'Sannat' 			=> 172,
		"Ta' Sannat" 		=> 172,
		'Għajnsielem' 		=> 158,
		'Ghajnsielem' 		=> 158,
		'Il-Qala' 			=> 161,
		'Qala' 				=> 161,
		'Nadur' 			=> 162,
		'Ix-Xagħra' 		=> 164,
		'Victoria' 			=> 163,
		'Munxar' 			=> 160,
		"Ta' Kerċem" 		=> 171,
		'Iż-Żebbuġ' 		=> 166,
		'Ix-Xewkija' 		=> 165,
		'L-Għarb' 			=> 167,
		'Il-Fontana' 		=> 159,
		'Kerċem' 			=> 171,
		'Xagħra' 			=> 164,
		'In-Nadur' 			=> 162,
		'L-Għasri' 			=> 168,
		'Żebbuġ' 			=> 166,
		'Mġarr' 			=> 173,
		'Għasri' 			=> 168,
		'Mgarr' 			=> 173,
	);

	if (isset($items[$locality])) {
		return $items[$locality];
	}

	return '';

}

if (isset($_GET['listing_index'])) {
	$listing_index = $_GET['listing_index'];
} else {
	$listing_index = 0;
}

if (isset($listings[$listing_index])) {

	$listing = $listings[$listing_index];

	$listing = json_decode($listing);

	echo '<pre>';
	print_r($listing);
	echo '</pre>';

	$title = trim($listing->title);

	if (empty($title)) { 

		$next_index = $listing_index+1;

		echo 'refreshing...<meta http-equiv="refresh" content="1; url=http://wordpress-27320-58852-157251.cloudwaysapps.com/_import/listings_connect.php?listing_index=' . $next_index . '">';

		exit('No TITLE!'); 

	}

	$matching_posts = get_posts(array(
		'post_type' => 'vg_listings',
		'post_status' => 'any',
		'meta_query' => array(
			array(
				'key' => '_imported_listing_id',
				'value' => $listing->id,
			)
		)
	));

	if (!empty($matching_posts)) {
		echo 'This item is already imported';

		$next_index = $listing_index+1;

		echo 'refreshing...<meta http-equiv="refresh" content="1; url=http://wordpress-27320-58852-157251.cloudwaysapps.com/_import/listings_connect.php?listing_index=' . $next_index . '">';
		exit;
	}


	/* member check */
	list($throwaway, $memberID) = explode('/', $listing->linked_member);

	$matchingUsers = get_users(array(
		'meta_query' => array(
			array(
				'key' => '_imported_user_id',
				'value' => $memberID,
			)
		)
	));

	echo 'matchingUsers:<pre>';
	print_r($matchingUsers);
	echo '</pre>';

	/*
	if (empty($matchingUsers)) {
		echo('No Connection to User!');
	} else {
	*/

		//$userID = $matchingUsers[0]->ID;
		$userID = 0;

		/* we need to first connect this listing with an actual member, if we are associated with a member we try to match the listing - 
		if a match is found we then can make the connection - if no match is found then probably the property does not exist - 
		we should make a call at the end to find out what properties do not have any connection from wordpress side and also the csv side. */

		$matching_posts = get_posts(array(
			'post_type' => 'vg_listings',
			'title' 	=> $title,
			'post_status' => 'publish',
		));

		echo 'Matching Posts:<pre>';
		print_r($matching_posts);
		echo '</pre>';

		$total = count($matching_posts);

		if (!$total) {
			echo '<strong>Not found!</strong><br />';
			$counts['none']++;

			/* so the item does not exist, lets just create a post for it and fake the rest of the proccess - so we are only setting the status and post type and title at this point in time. The next step will fill all the other required information like category and extra data. */

			$args = array(
				'post_title' => $title,
				'post_status' => 'publish',
				'post_type' => 'vg_listings',

			);

			$newPostID = wp_insert_post($args);

			if (!is_wp_error($newPostID)) {

				$p = get_post($newPostID);

				$matching_posts = array($p);

				$total = count($matching_posts);
				
			} else {
				$error_string = $result->get_error_message();
	   			echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';

			}

		}

		if ($total > 1) {

			echo '<strong>to many matching posts!</strong><br />';
			$counts['many']++;

		} else if ($total == 1) {

			echo '<strong>great there is a match!</strong><br />';
			$counts['single']++;

			$matchingID = $matching_posts[0]->ID;

			update_post_meta($matchingID, '_imported_listing_id', $listing->id);

			$attachments = array();

			$descriptions = array();

			$email = '';
			$mobile = '';
			$tel = '';
			$website = '';
			$approved = 'no';

			foreach ($languages as $lang) {

				$listingDataFile = file_get_contents('listings/' . $listing->id . '_' . $lang . '.txt');

				echo $listingDataFile . '<br />';

				$listingData = json_decode($listingDataFile);

				if (!empty($listingData)) {

					$descriptions[$lang] = strip_tags($listingData->desc);

					if ($lang == 'en') {

						$email = trim($listingData->email);
						$mobile = trim($listingData->mobile);
						$tel = trim($listingData->tel);
						$website = trim($listingData->website);
						$approved = $listingData->approved;

						foreach ($listingData->images as $image) {

							$aid = import_external_image($image, $matchingID);

							if (!is_wp_error($aid) ) 
								$attachments[] = $aid;

						}

					}

				}

			}

			$lat = '';
			$lon = '';
			$address = '';
			$locality = '';

			$geoFilePath = 'listings_geo/' . $listing->id . '.txt';

			if (file_exists($geoFilePath)) {

				$listingGeoDataFile = file_get_contents($geoFilePath);

				echo 'GEO Data:<pre>';
				print_r($listingGeoDataFile);
				echo '</pre>';

				if (!empty($listingGeoDataFile)) {

					$listingGeoDataFile = json_decode($listingGeoDataFile);

					$lat 		= $listingGeoDataFile->lat;
					$lon 		= $listingGeoDataFile->lon;
					$address 	= $listingGeoDataFile->address;
					$locality 	= google_locality_to_internal_id((isset($listingGeoDataFile->city)) ? $listingGeoDataFile->city : '');

				}

			}

			/* section check */
			list($throwaway, $sectionID) = explode('/', $listing->section);

			$categoryID = section_id_to_category_id($sectionID);

			echo 'Listing Extra Data:<pre>';
			print_r($listingData);
			echo '</pre>';

			$item_meta = array();

			$item_meta[75] = $matching_posts[0]->post_title;
			$item_meta[94] = $userID;

			/* Category */
			$item_meta[77] = $categoryID;

			/* Lang */
			$item_meta[86] = $descriptions['en'];
			$item_meta[125] = $descriptions['fr'];
			$item_meta[126] = $descriptions['de'];
			$item_meta[127] = $descriptions['it'];
			$item_meta[128] = $descriptions['ru'];
			$item_meta[129] = $descriptions['es'];
			$item_meta[130] = $descriptions['sv'];

			/* contact info */

			$contact_numbers = array();

			if (!empty($tel)) {
				$contact_numbers[] = $tel;
			}

			if (!empty($mobile)) {
				$contact_numbers[] = $mobile;
			}

			$item_meta[81] = implode(', ', $contact_numbers);

			$item_meta[82] = $email;
			$item_meta[83] = $website;

			if (!empty($attachments)) {

				$item_meta[91] = $attachments[0];
				$item_meta[92] = array();

				foreach ($attachments as $k=>$aid) {
					if ($k < 1) continue;
					$item_meta[92][] = $aid;
				}

			}

			/* location */
			$item_meta[95] = $lat;
			$item_meta[96] = $lon;
			$item_meta[78] = $address;
			$item_meta[80] = $locality;

			
			$frmArgs = array(
			  'form_id' 	=> 8,
			  'frm_user_id' => $userID,
			  'item_meta' 	=> $item_meta,
			);

			echo 'Form Args:<pre>';
			print_r($frmArgs);
			echo '</pre>';

			$entry_id = FrmEntry::create($frmArgs);

			echo 'Entry ID:<pre>';
			print_r($entry_id);
			echo '</pre>';

			$wpdb->insert( 
				'vg_listing_relationships', 
				array( 
					'listing_id' 	=> $matchingID, 
					'form_entry_id' => $entry_id,
					'user_id' 		=> $userID,
					'type'			=> 'listing',
					'status'		=> 1,
				), 
				array( 
					'%d', 
					'%d',
					'%d',
					'%s',
					'%d'
				) 
			);
			
		} /* create entry */

	//} /* matching user */

	echo '<br /><br /><br /><br /><br /><br />===================================================<br /><br /><br /><br /><br /><br />';

	$next_index = $listing_index+1;

	echo 'next: <a href="?listing_index=' . $next_index . '">Go Next</a><br /><br />'; 

	echo 'refreshing...<meta http-equiv="refresh" content="1; url=http://wordpress-27320-58852-157251.cloudwaysapps.com/_import/listings_connect.php?listing_index=' . $next_index . '">';

} else {
	exit('No matching index!');
}

?>