<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );


$paged = 0;
if (isset($_GET['page'])) {
	$paged = $_GET['page'];
}

$posts = get_posts(array(
	'post_type' => 'page',
	'post_per_page' => 10,
  	'paged' => $paged,
  	'suppress_filters' => false,
));

foreach ($posts as $post) {

	echo 'doing post: ' . $post->post_title . '<br />';
	
	vg_elastic_index_post($post->ID, $post);
	
	//
	//break;
}

$paged++;

echo 'refreshing...<meta http-equiv="refresh" content="3; url=https://www.visitgozo.com/_import/update_elastic_listings_page.php?page=' . $paged . '">';