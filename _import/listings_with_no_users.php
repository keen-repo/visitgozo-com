<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

$listings_list = file_get_contents('listings.txt');

$listings = explode(PHP_EOL, $listings_list);

$matches = 0;

foreach ($listings as $listing) {

	$listing = json_decode($listing);

	list($throwaway, $memberID) = explode('/', $listing->linked_member);

	$matchingUsers = get_users(array(
		'meta_query' => array(
			array(
				'key' => '_imported_user_id',
				'value' => $memberID,
			)
		)
	));

	if (empty($matchingUsers)) {
		$matches++;
		echo('No Connection to User!');

		echo '<pre>';
		print_r($listing);
		echo '</pre>';
		
	}

}

echo 'Matches: ' . $matches . '<br />';

?>