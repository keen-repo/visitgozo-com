<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

$posts = get_posts(array(
	'post_type' => 'any',
	'meta_query' => array(
		array(
			'key' 		=> '_vg_listing-website',
			'compare' 	=> '!=',
			'value' 	=> ''
		),
	)
));

foreach ($posts as $post) {

	echo '<pre>';
	print_r($post);
	echo '</pre>';

	$website_url = get_post_meta($post->ID, '_vg_listing-website', true);

	echo $website_url . '<br />';

}

?>