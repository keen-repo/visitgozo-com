<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

$pages = get_posts(array(
	'post_type' => 'page',
	'numberposts' => -1,
	'suppress_filters' => false,
));

$active_languages = $sitepress->get_active_languages();

echo '<pre>';
print_r($active_languages);
echo '</pre>';

foreach ($pages as $page) {

	$terms = wp_get_object_terms($page->ID, 'vg_listings_cats');

	echo 'Starting Page: ' . $page->ID . '<br />';

	foreach ($active_languages as $lang) {

		if ($lang['code'] == 'en') continue;

		$translated_page_id = icl_object_id($page->ID, 'page', true, $lang['code']);

		if ($translated_page_id) {

			echo 'doing lang: ' . $lang['code'] . ' for post: ' . $translated_page_id .  '<br />';

			$term_list = array();

			foreach ($terms as $term) {

				$term_id = (int)icl_object_id($term->term_id, 'vg_listings_cats', true, $lang['code']);

				if ($term_id) {

					$term_list[] = $term_id;

				}

			}

			if (!empty($term_list)) {

				echo 'adding term cats!';

				wp_set_object_terms($translated_page_id, $term_list, 'vg_listings_cats');

				echo 'final term list:';
				echo '<pre>';
				print_r($term_list);
				echo '</pre>';

			}

			

		}

	}
}

?>