<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );


$posts = get_posts(array(
	'post_type' => array('page'),
	'numberposts' => -1,
	'suppress_filters' => false,
	'meta_query' => array(
		'relation' => 'AND',
		array(
			'key' => '_vg_listing-tripadvisor-url',
			'compare' => '!=',
			'value' => '',
		)
	)
));

echo '<pre>';
print_r($posts);
echo '</pre>';

foreach ($posts as $post) {

	$trip_advisor_link = get_post_meta($post->ID, '_vg_listing-tripadvisor-url', true);

	preg_match_all("/d([0-9]*)-/", $trip_advisor_link, $out, PREG_PATTERN_ORDER);

	if (!empty($out) && isset($out[1][0])) {

		$listingID = $out[1][0];

		echo $post->post_title . '<br />';
		echo $trip_advisor_link . '<br />';
		echo $listingID . '<br />';
		echo '<br /><hr /><br />';

		update_post_meta($post->ID, '_vg_listing-tripadvisor-id', $listingID);

	}


}