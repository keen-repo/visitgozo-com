<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

$posts = get_posts(array(
	'post_type' => 'page',
	'numberposts' => -1,
	'suppress_filters' => false,
));

$csv[] = array();

$csv[] = array(
	'Page Title',
	'Listing Title',
	'Address 1',
	'Address 2',
	'Contact No',
	'Email',
	'Website',
	'Latitude',
	'Longitude'
);

foreach ($posts as $post) {

	if (empty($post->post_title)) continue;

	$listings = get_posts(array(
		'post_type' => 'vg_listings',
		'title' => $post->post_title,
	));

	echo 'Matching Post Title: ' . $post->post_title . '<br />';

	if (!empty($listings) && count($listings) === 1) {

		echo '<pre>';
		print_r($listings);
		echo '</pre>';

		echo '<hr />';

		$pm = get_post_meta($listings[0]->ID);

		echo '<pre>';
		print_r($pm);
		echo '</pre>';

		/*
		$keys = array('_vg_listing-address-1', '_vg_listing-address-2', '_vg_listing-contact-no', '_vg_listing-email', '_vg_listing-website', '_vg_listing-video', '_vg_listing-latitude', '_vg_listing-longitude');

		foreach ($keys as $key) {
			update_post_meta($post->ID, $key, $pm[$key][0]);
		}

		update_post_meta( $post->ID, '_wp_page_template', 'single-vg_listings.php' );

		$terms = wp_get_object_terms($listings[0]->ID, 'vg_locality_cats');

		echo '<pre>';
		print_r($terms);
		echo '</pre>';

		$locality = 0;
		foreach ($terms as $term) {
			$locality = $term->term_id;
			break;
		}

		wp_set_object_terms($post->ID, array((int)$locality), 'vg_locality_cats');
		*/

		$csv[] = array(
			$post->post_title,
			$listings[0]->post_title,
			get_post_meta($listings[0]->ID, '_vg_listing-address-1', true),
			get_post_meta($listings[0]->ID, '_vg_listing-address-2', true),
			get_post_meta($listings[0]->ID, '_vg_listing-contact-no', true),
			get_post_meta($listings[0]->ID, '_vg_listing-email', true),
			get_post_meta($listings[0]->ID, '_vg_listing-website', true),
			get_post_meta($listings[0]->ID, '_vg_listing-latitude', true),
			get_post_meta($listings[0]->ID, '_vg_listing-longitude', true),
		);

	}

}


$fp = fopen('matching_listings_and_pages.csv', 'w');

fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

foreach ($csv as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);