<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );

$paged = 0;
if (isset($_GET['page'])) {
	$paged = $_GET['page'];
}

$posts = get_posts(array(
	'post_type' => 'vg_listings',
	'posts_per_page' => 10,
  	'paged' => $paged,
  	'suppress_filters' => false,
  	'meta_query' => array(
  		array(
  			'key' => '_vg_map_object_id',
  			'value' => '',
  			'compare' => '!='
  		)
  	)
));

foreach ($posts as $post) {

	echo 'doing post: ' . $post->post_title . '<br />';

	update_arcgis_listing($post);

}

$paged++;

echo 'refreshing...<meta http-equiv="refresh" content="3; url=https://www.visitgozo.com/_import/update_arcgis_listings.php?page=' . $paged . '">';



?>