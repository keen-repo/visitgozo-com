<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );


$posts = get_posts(array(
	'post_type' => array('page'),
	'numberposts' => -1,
	'suppress_filters' => false,
	'meta_query' => array(
		array(
			'key' => '_wp_page_template',
			'value' => 'single-vg_listings.php',
		)
	)
));

$csv[] = array('Page Title', 'Has Gallery', 'Has Featured Image', 'Has Locality', 'Has Search Category', 'Has Lat & Lon', 'Has Tripadvisor', 'Has Page Intro');

function validate_row($data) {

	if (empty($data)) {
		return '';
	}

	return 'X';

}

$taken_ids = array();

foreach ($posts as $post) {

	$taken_ids[] = $post->ID;

	$gallery = get_post_meta($post->ID, '_vg_page-gallery', true);

	$thumbnail_id = get_post_thumbnail_id($post->ID);

	$category = wp_get_object_terms($post->ID, 'vg_listings_cats');

	$localities = wp_get_object_terms($post->ID, 'vg_locality_cats');

	$lat = get_post_meta($post->ID, '_vg_listing-latitude', true);
	$lon = get_post_meta($post->ID, '_vg_listing-longitude', true);
	$tripadvisor = get_post_meta($post->ID, '_vg_listing-tripadvisor-url', true);

	$page_intro = get_post_meta($post->ID, '_vg_page-intro-text', true);



	$csv[] = array(
		$post->post_title,
		validate_row($gallery),
		validate_row($thumbnail_id),
		validate_row($localities),
		validate_row($category),
		validate_row($lat),
		validate_row($tripadvisor),
		validate_row($page_intro)
	);

}


$posts = get_posts(array(
	'post_type' => array('page'),
	'numberposts' => -1,
	'suppress_filters' => false,
));

$csv[] = array();
$csv[] = array();
$csv[] = array();
$csv[] = array();
$csv[] = array();
$csv[] = array();
$csv[] = array();
$csv[] = array();

foreach ($posts as $post) {

	if (in_array($post->ID, $taken_ids)) continue;

	$gallery = get_post_meta($post->ID, '_vg_page-gallery', true);

	$thumbnail_id = get_post_thumbnail_id($post->ID);

	$category = wp_get_object_terms($post->ID, 'vg_listings_cats');

	$localities = wp_get_object_terms($post->ID, 'vg_locality_cats');

	$lat = get_post_meta($post->ID, '_vg_listing-latitude', true);
	$lon = get_post_meta($post->ID, '_vg_listing-longitude', true);
	$tripadvisor = get_post_meta($post->ID, '_vg_listing-tripadvisor-url', true);

	$page_intro = get_post_meta($post->ID, '_vg_page-intro-text', true);



	$csv[] = array(
		$post->post_title,
		validate_row($gallery),
		validate_row($thumbnail_id),
		validate_row($localities),
		validate_row($category),
		validate_row($lat),
		validate_row($tripadvisor),
		validate_row($page_intro)
	);

}


$fp = fopen('page_missing_data.csv', 'w');

fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

foreach ($csv as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);

?>