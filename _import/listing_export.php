<?php

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require('../wp-blog-header.php' );


$terms = vg_build_term_tree('vg_listings_cats', '_');

foreach ($terms as $term_id=>$term_name) {

	$term_info = get_term($term_id, 'vg_listings_cats');

	$posts = get_posts(array(
		'post_type' => array('page', 'vg_listings'),
		'numberposts' => -1,
		'suppress_filters' => false,
		'tax_query' => array(
			array(
				'taxonomy' => 'vg_listings_cats',
				'terms' => array((int)$term_id)
			)
		)
	));

	echo '<pre>';
	print_r($posts);
	echo '</pre>';

	$list = array();

	$list[] = array(
		'vg_web_id',
		'title',
		'en_description',
		'fr_description',
		'de_description',
		'it_description',
		'ru_description',
		'es_description',
		'sv_description',
		'cn_description',
		'category_id',
		'category_name', 
		'locality_id',
		'locality_name',
		'image',
		'latitude',
		'longitude',
		'address_1',
		'address_2',
		'contact_no',
		'email',
		'website',
		'vg_web_url',
	);

	foreach ($posts as $post) {

		$terms = wp_get_object_terms($post->ID, 'vg_locality_cats');

		$locality_id 	= 0;
		$locality_name 	= '';
		foreach ($terms as $term) {
			$locality_id = $term->term_id;
			$locality_name = $term->name;
			break;
		}

		$image = wp_get_attachment_image_src(get_post_thumbnail_id(), array(9999,9999));

		$lat = get_post_meta($post->ID, '_vg_listing-latitude', true);
		$lon = get_post_meta($post->ID, '_vg_listing-longitude', true);

		if (empty($lat)) $lat = 0;
		if (empty($lon)) $lon = 0;

		$address_1 	= get_post_meta($post->ID, '_vg_listing-address-1', true);
		$address_2 	= get_post_meta($post->ID, '_vg_listing-address-2', true);
		$contact_no = get_post_meta($post->ID, '_vg_listing-contact-no', true);
		$email 		= get_post_meta($post->ID, '_vg_listing-email', true);
		$website 	= get_post_meta($post->ID, '_vg_listing-website', true);

		$list[] = array(
			$post->ID,
			$post->post_title,
			apply_filters('the_content', $post->post_content),
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			$term_id,
			$term_info->name, 
			$locality_id,
			$locality_name,
			$image[0],
			$lat,
			$lon,
			$address_1,
			$address_2,
			$contact_no,
			$email,
			$website,
			get_permalink($post->ID),
		);

	}


	$fp = fopen('map/category_' . $term_name . '_' . $term_id . '.csv', 'w');

	foreach ($list as $fields) {
	    fputcsv($fp, $fields);
	}

	fclose($fp);

}

?>